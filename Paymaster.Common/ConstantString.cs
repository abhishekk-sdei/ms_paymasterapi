﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Common
{
    public class ConstantString
    {
        public static class StatusMessage
        {
           public const string ServerError = "Internal Server error";
            public const string FetchMessage = "Success";
            public const string NoFetchMessage = "No record found";
            public const string AddEmpSuccessMsg = "Employee saved successfully";
            public const string AddEmpBasicDetailsSuccessMsg = "Employee basic details saved successfully";
            public const string AddEmpSalaryDetailsSuccessMsg = "Employee salary details saved successfully";
            public const string AddEmpPersonalDetailsSuccessMsg = "Employee personal details saved successfully";
            public const string AddEmpPaymentDetailsSuccessMsg = "Employee payment details saved successfully";
            public const string UpdateEmpSuccessMsg = "Employee updated successfully";
            public const string UpdateCheckMsg = "Checks updated successfully";
            public const string DeleteEmpSuccessMsg = "Employee deleted successfully";
            public const string ProcessedSuccessMsg = "Processed successfully";
            public const string NoProcessRecord = "No need to process";
            public const string ProcessedErrorMsg = "Processed failed";
            public const string TimePunchesSaveSuccessMsg = "Time punches saved successfully";
            public const string TimePunchesUpdateSuccessMsg = "Time punches updated successfully";
            public const string OrgSaveSuccessMsg = "Organization saved successfully";
            public const string OrgUpdateSuccessMsg = "Organization updated successfully";
            public const string DomainExist = "Domain name already exists";
            public const string DomainNotExist = "Domain name not exists";
        }

        public static class OrganizationConnection
        {
            public const string Server = "";
            public const string  Database = "";
            public const string User = "";
            public const string Password = "";
        }
    }
}

﻿using Paymaster.DataContract.Authentication;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using static Paymaster.Common.Enum.CommonEnums;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System.IdentityModel.Tokens.Jwt;
using Paymaster.Common.Options;
using Paymaster.DataContract.Common;
using System.Linq;
using static Paymaster.Common.ConstantString;
using System.Net;

namespace Paymaster.Common
{
   public class CommonMethod
    {
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = EncryptDecryptKey.Key;
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Dispose();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }


        public static string GetFullExceptionDetails(Exception x)
        {
            var st = new StackTrace(x, true);
            var frames = st.GetFrames();
            var traceString = new StringBuilder();

            foreach (var frame in frames)
            {
                if (frame.GetFileLineNumber() < 1)
                    continue;

                traceString.Append("File: " + frame.GetFileName());
                traceString.Append(", Method:" + frame.GetMethod().Name);
                traceString.Append(", LineNumber: " + frame.GetFileLineNumber());
                traceString.Append("  -->  ");
            }

            return traceString.ToString();
        }

        public static TokenModel GetTokenDataModel(HttpContext request)
        {
            TokenModel token = new TokenModel();


            StringValues authorizationToken;
            StringValues timezone;
            StringValues ipAddress;
            StringValues locationID;
            StringValues offSet;
          //  JsonModel response = new JsonModel();
            var authHeader = request.Request.Headers.TryGetValue("Authorization", out authorizationToken);
            var authToken = authorizationToken.ToString().Replace("Bearer", "").Trim();
            //request.Request.Headers.TryGetValue("Timezone", out timezone);
            //request.Request.Headers.TryGetValue("IPAddress", out ipAddress);
            //request.Request.Headers.TryGetValue("LocationID", out locationID);
            //request.Request.Headers.TryGetValue("Offset", out offSet);
            try
            {
                if (authToken != null)
                {
                    var encryptData = GetDataFromToken(authToken);
                    
                    if (encryptData != null && encryptData.Claims != null)
                    {
                        string AccessToken = GetRefreshToken(encryptData);
                        token = new TokenModel()
                        {
                            UserID = Convert.ToInt32(encryptData.Claims[0].Value),
                            RoleID = Convert.ToInt32(encryptData.Claims[1].Value),
                            UserName = Convert.ToString(encryptData.Claims[2].Value),
                            OrganizationID = Convert.ToInt32(encryptData.Claims[3].Value),
                            StaffID = Convert.ToInt32(encryptData.Claims[4].Value),
                            LocationID = !string.IsNullOrEmpty(locationID) ? Convert.ToInt32(locationID) : Convert.ToInt32(encryptData.Claims[5].Value), //it should be from front end
                            DomainName = Convert.ToString(encryptData.Claims[6].Value),
                            Timezone = timezone,
                            IPAddress = ipAddress,
                            OffSet = Convert.ToInt32(offSet),
                            AccessToken = AccessToken
                            //Request = request,
                        };
                    }
                }
                token.Request = request;
            }
            catch (Exception ex)
            {
            }

            //// static token for LinkWorks Agency Admin
            //token = new TokenModel() { UserID = 5, OrganizationID = 16, Timezone = "India Standard Time", IPAddress = "203.129.220.76", LocationID = 5, RoleID = 9, DomainName ="paymaster", Request = request, OffSet = 330 };
            //// token = new TokenModel() { UserID = 3, OrganizationID = 3, Timezone = "India Standard Time", IPAddress = "203.129.220.76", LocationID = 3, RoleID = 9, DomainName = HCOrganizationConnectionStringEnum.Host, Request = request, OffSet = 330 };

            return token;
        }


        public static dynamic GetDataFromToken(string token)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                return handler.ReadToken(token);

            }
            catch (Exception)
            {
                return null;
            }

        }


        public static string GetRefreshToken(dynamic encryptData)
        {
            JwtIssuerOptions _jwtOptions = new JwtIssuerOptions();
            //create claim for login user
            var claims = encryptData.Claims;

            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.JwtAudience,
                claims: claims,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: _jwtOptions.SigningCredentials);

            //add login user's role in token
            jwt.Payload["roles"] = encryptData.Claims[1].Value; //array of user roles

            //token.LocationID = defaultLocation;
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }
        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = EncryptDecryptKey.Key;
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Dispose();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static JsonModel Pagination<T>(List<T> reponse, int page = 0, int pageSize = 10,int Totalrecords = 0) where T : class, new()
        {
            try
            {
                JsonModel obj = new JsonModel();
                obj.data = reponse;
                obj.Message = StatusMessage.FetchMessage;

                obj.StatusCode = (int)HttpStatusCodes.OK;//Success
                obj.meta = new Meta()
                {
                    TotalRecords = reponse != null && reponse.Count > 0 ? Totalrecords : 0,
                    CurrentPage = page,
                    PageSize = pageSize,
                    DefaultPageSize = pageSize,
                    TotalPages = Math.Ceiling(Convert.ToDecimal((reponse != null && reponse.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
                };

                return obj;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string GetIPAddress()
        {
            string hostName = Dns.GetHostName();
            return Dns.GetHostEntry(hostName).AddressList[1].ToString();
        }

    }
}

﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Admin.Login
{
    public class AdminUsers: CommonProp
    {
        [Key]
        public long UserId { get; set; }
        public Int16 RoleType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}

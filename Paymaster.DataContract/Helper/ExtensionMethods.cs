﻿using System;
using System.Collections.Generic;
using System.Text;
using Paymaster.DataContract;
using System.Linq;
using System.Collections;

namespace Paymaster.DataContract.Helper
{
    public static class ExtensionMethods
    {
        public static IEnumerable<User.Users> WithoutPasswords(this IEnumerable<User.Users> users)
        {
            return users.Select(x => x.WithoutPassword());
        }

        public static User.Users WithoutPassword(this User.Users user)
        {
            user.Password = null;
            return user;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class ErrorLog
    {
        [Key]
        public int ErrorLogId { get; set; }
        public Int64? ErrorNumber { get; set; }
        public string ErrorType { get; set; }
        public int ErrorLine { get; set; }
        public DateTime ErrorTime { get; set; }
        public string ErrorState { get; set; }
        public int ErrorSeverity { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorProcedure { get; set; }
    }
}

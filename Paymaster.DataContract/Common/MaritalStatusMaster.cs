﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class MaritalStatusMaster : CommonProp
    {
        [Key]
        public int MaritalStatusId { get; set; }
        public string MaritalStatusName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class EmployeeDetailModel
    {
        public List<EmployeeViewModel> EmployeeViewModel { get; set; }
        public EmployeePayPeriodModel EmployeePayPeriodModel { get; set; }
    }
    public class EmployeeViewModel
    {
        public int EmployeeID { get; set; }
        public string EnctypedEmployeeID { get; set; }
        public string Number { get; set; }
        public Int16? Active { get; set; }
        //[Required]
        public string SocSec { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string EmployeeName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string GenderName { get; set; }
        public string RaceName { get; set; }
        public string MaritalStatusName { get; set; }
        public string CityName { get; set; }
        public decimal? HourlyPayRate { get; set; }
        public string Hourly_PayRate { get; set; }
        public decimal? TotalTime { get; set; }
        public string BasicSalary { get; set; }
        
        public DateTime? CreatedDate { get; set; }
        
        public int IsActive { get; set; }
        public int TotalRecords { get; set; }
    }
    public class EmployeePayPeriodModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PeriodName { get; set; }
        public int PayPeriodId { get; set; }
    }

    public class EmployeeSocSecModel
    {
        public int EmployeeID { get; set; }
        public string Number { get; set; }
        public Int16? Active { get; set; }
        //[Required]
        public string SocSec { get; set; }

    }
}

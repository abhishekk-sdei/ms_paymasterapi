﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class PeriodMaster : CommonProp
    {
        [Key]
        public int PeriodId { get; set; }
        public string PeriodName { get; set; }
    }
}

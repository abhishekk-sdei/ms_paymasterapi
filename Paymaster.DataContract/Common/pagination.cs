﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class pagination
    {
        public object data { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public Meta meta { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
   public  class CountryMaster : CommonProp
    {
        [Key]
        public int CountryID { get; set; }
        public string  CountryName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class GenderMaster : CommonProp
    {
        [Key]
        public int GenderId { get; set; }
        public string GenderName { get; set; }
    }
}

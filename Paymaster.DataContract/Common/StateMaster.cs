﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class StateMaster : CommonProp
    {
        [Key]
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int? CountryID { get; set; }
    }
}

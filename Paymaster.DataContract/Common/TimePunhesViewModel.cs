﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class TimePunhesViewModel
    {
       
        public string EnctypedEmployeeID { get; set; }
        public string Number { get; set; }
        public Int16? Active { get; set; }
        //[Required]
        public string SocSec { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string EmployeeName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string GenderName { get; set; }
        public string RaceName { get; set; }
        public string MaritalStatusName { get; set; }
        public string CityName { get; set; }
        public decimal? HourlyPayRate { get; set; }
        public string Hourly_PayRate { get; set; }
        public decimal? TotalTime { get; set; }
        public string BasicSalary { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int IsActive { get; set; }
        public int TimePunchId { get; set; }
        public int? EmployeeId { get; set; }
        public int? CheckId { get; set; }
        public string InDateTime { get; set; }
        public string OutDateTime { get; set; }
        public char Type { get; set; }
        public decimal? Hours { get; set; }
        public DateTime? ReferenceDate { get; set; }
        public string HoursClock { get; set; }
      
        public decimal? OvertimeMultiplier { get; set; }
        public decimal? Amount { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TotalHrsclock { get; set; }
        public decimal? EmpHourlyPayRate { get; set; }
        public decimal? TotalHrs { get; set; }
        public int? Closed { get; set; }
        public long? CreatedBy { get; set; }
        public string GrossSalary { get; set; }
        public int TotalRecords { get; set; }
        public int CureentpayPeriodId { get; set; }
        public long UpdatedBy { get; set; }
    }


    public class TimePunhesDetailModel
    {
        public List<TimePunhesViewModel> TimePunhesViewModel { get; set; }
        public EmployeePayPeriodModel EmployeePayPeriodModel { get; set; }
    }

    public class EmployeeNumberModel
    {
        public int? EmployeeId { get; set; }
        public string Number { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class Processpayrollviewmodel
    {
        public int payperiodId { get; set; }
        public int applicationId { get; set; }
        public int userId { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int payFrequency{get;set;}
        public string createDate { get; set; }
        public long createdBy { get; set; }
        public string checkDate { get; set; }
    }
}

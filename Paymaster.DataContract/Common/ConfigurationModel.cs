﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Common
{
   public  class Configuration
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }
}

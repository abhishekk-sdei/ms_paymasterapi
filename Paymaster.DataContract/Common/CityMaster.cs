﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class CityMaster : CommonProp
    {
        [Key]
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int? StateId { get; set; }        
        public int? CountryID { get; set; }
    }
}
    
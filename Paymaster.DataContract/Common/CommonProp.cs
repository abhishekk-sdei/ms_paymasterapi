﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class CommonProp
    {
      //  [Required]
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public long? DeletedBy { get; set; }
        //[Required]
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
       // [Required]
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }

    public static class PayMasterConnectionStringEnum
    {

        //sa Local DB
        //public const string Server = "192.168.0.240";
        //public const string Database = "PaymasterMainDB";
        //public const string User = "paymaster";
        //public const string Password = "paymaster";
        //ea Local DB

        //sa Staging DB

        public const string Server = "75.126.168.31,7009";
        //sa Database of staging
        //public const string Database = "PaymasterMainDB";
        //ea Database of staging
        //sa Database of local

        public const string Database = "PaymasterMainDB_local";
        //ea Database of local
        public const string User = "paymaster";
        public const string Password = "paymaster";
        //ea Staging DB


        //public const string Host = "OHCDevelopment";
    }
}

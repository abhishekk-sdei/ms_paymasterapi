﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class RaceMaster : CommonProp
    {
        [Key]
        public int RaceId { get; set; }
        public string RaceName { get; set; }
    }
}

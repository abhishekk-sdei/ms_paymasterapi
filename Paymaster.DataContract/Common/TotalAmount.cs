﻿using Paymaster.DataContract.Organization.Employees;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class TotalAmount
    {
        public List<Employees> EmployeesFilter { get; set; }
        public List<EmployeeDeductions> EmployeesDeductionFilter { get; set; }
        public List<Checks> ChecksDeductionFilter { get; set; }
        public List<TimePunches> TimePunchesFilter { get; set; }
        public List<CheckDeductions> CheckDeductionsFilter { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class TypeMaster : CommonProp
    {
        [Key]
        public int TypeId { get; set; }
        public string TypeName { get; set; }

        public string TypeCode { get; set; }
    }
}

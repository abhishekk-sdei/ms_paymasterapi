﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Common
{
    public class TimePunchesHistoryModel
    {
        public string TypeCode { get; set; }
        public int? EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Lastname { get; set; }
        public string EmployeeName { get; set; }
        public string Number { get; set; }
        public string SocSec { get; set; }
        public string CheckNumber { get; set; }
        public string PayPeriodName { get; set; }
        public int TotalRecords { get; set; }
        public decimal Hours { get; set; }
        public decimal HoursClock { get; set; }
        public decimal Hourly_PayRate { get; set; }
        public decimal OvertimeMultiplier { get; set; }
        public decimal Amount { get; set; }
        public string InDateTime { get; set; }
        public string OutDateTime { get; set; }
        public string RefereceDay { get; set; }       
        public string ReferenceDate { get; set; }     
    }

    public class ViewTimePunchesHistoryModelOnChecks
    {
        public string TypeCode { get; set; }
        public int? EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Lastname { get; set; }
        public string EmployeeName { get; set; }
        public string Number { get; set; }
        public string SocSec { get; set; }
        public string CheckNumber { get; set; }
        public string PayPeriodName { get; set; }
        public int TotalRecords { get; set; }
        public string Hours { get; set; }
        public string HoursClock { get; set; }
        public string Hourly_PayRate { get; set; }
        public string OvertimeMultiplier { get; set; }
        public string Amount { get; set; }
        public string InDateTime { get; set; }
        public string OutDateTime { get; set; }
        public string RefereceDay { get; set; }
        public string ReferenceDate { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Authentication
{
    public class TokenModel
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public int RoleID { get; set; }
        public string Email { get; set; }
        public int OrganizationID { get; set; }
        public int LocationID { get; set; }
        public int StaffID { get; set; }
        public string Timezone { get; set; }
        public string IPAddress { get; set; }
        public string DomainName { get; set; }
        public string MacAddress { get; set; }
        //public int PateintID { get; set; }
        public int OffSet { get; set; }
        public string AccessToken { get; set; }
        public HttpContext Request { get; set; }
    }
    public class OrganizationDetails: CommonProp
    {
        [Key]
        public int ApplicationId { get; set; }
        public string DomainName { get; set; }
        public string ApplicationURL { get; set; }

        public string DatabaseServerName { get; set; }
        public string DatabaseName { get; set; }

        public string DatabaseUsername { get; set; }
        public string DatabasePassword { get; set; }
        public string ConnectionInfo { get; set; }
        public DateTime? StartFrom { get; set; }
        public DateTime? EndDate { get; set; }
        public string IPAddress { get; set; }
        public int PeriodId { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationUrl { get; set; }

        public string OrganizationEmail { get; set; }
        //public string OrganizationToken { get; set; }
        //public string OrganizationId { get; set; }
        ////public int Id { get; set; }
    }
}

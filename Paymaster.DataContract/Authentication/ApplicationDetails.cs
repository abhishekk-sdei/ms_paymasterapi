﻿using Paymaster.DataContract.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Authentication
{
    public class ApplicationDetails : Users
    {
        [Key]
        public int ApplicationId { get; set; }
        public string DomainName { get; set; }
        public string ApplicationURL { get; set; }

        public string DatabaseServerName { get; set; }
        public string DatabaseName { get; set; }
        public string DatabaseUsername { get; set; }
        public string DatabasePassword { get; set; }
        public string ConnectionInfo { get; set; }
        public DateTime? StartFrom { get; set; }
        public DateTime? EndDate { get; set; }
        public string IPAddress { get; set; }
        public int PeriodId { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationUrl { get; set; }

        public string OrganizationEmail { get; set; }
        public int OrganizationContactNumber { get; set; }
    }

    public class Organization_Details 
    {
        public string OrganizationName { get; set; }

        public string OrganizationEmail { get; set; }
        public string OrganizationContactNumber { get; set; }
        public string Password { get; set; }
        public int Country { get; set; }
        public string DomainName { get; set; }
    }
}

﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.User
{
    public class Users: CommonProp
    {
        [Key]
        public long UserId { get; set; }
        public Int16 RoleType { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        //public string ForgetPasswordToken { get; set; }
        //public string TokenId { get; set; }
    }
}

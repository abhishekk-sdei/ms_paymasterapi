﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.ViewModel
{
    public class FedWageBracketMethodTablesVM
    {
        public int FedWageBracketMethodId { get; set; }
        public Int16 Year { get; set; }
        public Int16 PayFrequency { get; set; }
        public Int16 MaritalStatus { get; set; }
        public Int16 Allowances { get; set; }
        public decimal FedWageBracketMethodOver { get; set; }
        public decimal Subtract { get; set; }
        public decimal Percentage { get; set; }

    }
}

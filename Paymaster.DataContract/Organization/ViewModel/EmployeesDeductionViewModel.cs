﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.ViewModel
{
    public class EmployeesDeductionViewModel
    {
        public int EmployeeDeductionId { get; set; }
        public int EmployeeId { get; set; }
        public Int16 PayFrequency { get; set; }
        public int DeductionTypeId { get; set; }
        public Int16 CalculateMethod { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}

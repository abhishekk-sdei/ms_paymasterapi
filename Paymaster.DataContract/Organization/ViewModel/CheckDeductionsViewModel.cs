﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.ViewModel
{
    public class CheckDeductionsViewModel
    {
        public int CheckDeductionId { get; set; }
        public int CheckId { get; set; }
        public int DeductionTypeId { get; set; }
        public int EmployeeDeductionId { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
    }
}

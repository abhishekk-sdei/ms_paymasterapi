﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.ViewModel
{
    public class PercentageTableRow
    {
       
        public int Id { get; set; }
        public Int16 maritalStatus { get; set; }
        public Decimal over { get; set; }
        public Decimal withold { get; set; }
        public Decimal plusPercentage { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.ViewModel
{
    public class ChecksViewModel
    {
        public int? CheckId { get; set; }
        //public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? EmployeeId { get; set; }
        public decimal? Gross { get; set; }
        public decimal? Amount { get; set; }
        public int? CheckNumber { get; set; }
        public DateTime? CheckDate { get; set; }
        public int? PayorBankNumber { get; set; }
        public string Notes { get; set; }

    }
}

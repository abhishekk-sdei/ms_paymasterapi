﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.ViewModel
{
    public class FedPercentageMethodTablesViewModel
    {
        public int FedPercentageMethodId { get; set; }
        public Int16 Year { get; set; }
        public Int16 PayFrequency { get; set; }
        public Int16 MartialStatus { get; set; }
        public decimal FedPercentageMeathodOver { get; set; }
        public decimal Withhold { get; set; }
        public decimal PlusPercentage { get; set; }
        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}

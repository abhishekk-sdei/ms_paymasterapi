﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.ViewModel
{
    public class WageBracketTableRow
    {
        public int Id { get; set; }
        public Int16 maritalStatus { get; set; }
        public Int16 allowances { get; set; }
        public Decimal over { get; set; }
        public Decimal subtract { get; set; }
        public Decimal percentage { get; set; }
    }
}

﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.ViewModel.TemplatesViewModel
{
    public class PrintChecksDetails
    {
        public decimal Regular { get; set; }
        public decimal Holiday { get; set; }
        public decimal Vacation { get; set; }
        public decimal Bonus { get; set; }
        public string PaidDate { get; set; }
        public decimal RegularHour { get; set; }
        public decimal HolidayHour { get; set; }
        public decimal VacationHour { get; set; }
        public decimal BonusHour { get; set; }
        public decimal RegularPayRate { get; set; }
        public decimal HolidayPayRate { get; set; }
        public decimal VacationPayRate { get; set; }
        public decimal BonusPayRate { get; set; }
        public decimal RegularAssignedHours { get; set; }
        public decimal HolidayAssignedHours { get; set; }
        public decimal VacationAssignedHours { get; set; }
        public decimal BonusAssignedHours { get; set; }
        public long CheckNumber { get; set; }
        public string CheckDate { get; set; }
        public long PayorBankNumber { get; set; }
        public decimal Amount { get; set; }
        public decimal Gross { get; set; }
        public string Firstname { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Number { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public decimal HourlyPayRate { get; set; }
        public decimal FederalTaxAmount { get; set; }
        public decimal SocialSecurityTaxAmount { get; set; }
        public decimal MedicareTaxAmount { get; set; }
        public decimal StateTaxAmount { get; set; }
        public decimal AdditionalFederalTaxAmount { get; set; }
        public decimal PFML_MedicalAmount { get; set; }
        public decimal PFML_LeaveAmount { get; set; }
        public decimal EmployeeDeductionAmount { get; set; }
        public Int16 Allowances { get; set; }
        public Int16 Dependents { get; set; }
        public Int16 Exemptions { get; set; }
        public decimal TotalEarning { get; set; }
        public decimal TotalDeduction { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationUrl { get; set; }
        public string OrganizationEmail { get; set; }

        public int EmployeeId { get; set; }

        public List<TimePunchesHistoryModel> TimePunchList { get; set; }

    }


    public class ViewPrintChecksDetails
    {
        public string Regular { get; set; }
        public string Holiday { get; set; }
        public string Vacation { get; set; }
        public string Bonus { get; set; }
        public string PaidDate { get; set; }
        public string RegularHour { get; set; }
        public string HolidayHour { get; set; }
        public string VacationHour { get; set; }
        public string BonusHour { get; set; }
        public string RegularPayRate { get; set; }
        public string HolidayPayRate { get; set; }
        public string VacationPayRate { get; set; }
        public string BonusPayRate { get; set; }
        public string RegularAssignedHours { get; set; }
        public string HolidayAssignedHours { get; set; }
        public string VacationAssignedHours { get; set; }
        public string BonusAssignedHours { get; set; }
        public long CheckNumber { get; set; }
        public string CheckDate { get; set; }
        public long PayorBankNumber { get; set; }
        public string Amount { get; set; }
        public string Gross { get; set; }
        public string Firstname { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Number { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string HourlyPayRate { get; set; }
        public string FederalTaxAmount { get; set; }
        public string SocialSecurityTaxAmount { get; set; }
        public string MedicareTaxAmount { get; set; }
        public string StateTaxAmount { get; set; }
        public string AdditionalFederalTaxAmount { get; set; }
        public string PFML_MedicalAmount { get; set; }
        public string PFML_LeaveAmount { get; set; }
        public string EmployeeDeductionAmount { get; set; }
        public Int16 Allowances { get; set; }
        public Int16 Dependents { get; set; }
        public Int16 Exemptions { get; set; }
        public string TotalEarning { get; set; }
        public string TotalDeduction { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationUrl { get; set; }
        public string OrganizationEmail { get; set; }

        public int EmployeeId { get; set; }
        public string AmountInWords { get; set; }
        public List<ViewTimePunchesHistoryModelOnChecks> TimePunchList { get; set; }

    }
}

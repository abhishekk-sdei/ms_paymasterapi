﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class EmployeeDeductions : CommonProp
    {
        [Key]
        public int EmployeeDeductionId { get; set; }
        public int EmployeeId { get; set; }
        public Int16 PayFrequency { get; set; }
        public int DeductionTypeId { get; set; }
        public Int16 CalculateMethod { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
    }
}

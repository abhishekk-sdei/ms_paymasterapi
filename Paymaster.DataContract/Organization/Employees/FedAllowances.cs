﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class FedAllowances : CommonProp
    {
        [Key]
        public int FedAllowancesId { get; set; }
        public Int16 Year { get; set; }
        public Int16 PayFrequency { get; set; }
        public decimal EachAllowanceAmount { get; set; }
    }
}

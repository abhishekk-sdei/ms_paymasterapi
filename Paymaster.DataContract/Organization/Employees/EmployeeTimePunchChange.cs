﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class EmployeeTimePunchChange : CommonProp
    {
        [Key]
        public int EmpTimePunchChangeId { get; set; }
        public int PayPeriodId { get; set; }
        public int? EmployeeId { get; set; }
    }
}

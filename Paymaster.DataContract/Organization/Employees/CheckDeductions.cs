﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class CheckDeductions : CommonProp
    {
        [Key]
        public int CheckDeductionId { get; set; }
        public int CheckId { get; set; }
        public int DeductionTypeId { get; set; }
        public int EmployeeDeductionId { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
        public int? EmployeeId { get; set; }
        public int Payperiod { get; set; }
    }
}

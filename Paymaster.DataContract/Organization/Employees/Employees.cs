﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
   public class Employees : CommonProp
    {
        [Key]
        public int EmployeeID { get; set; }
        //public string EnyEmployeeID { get; set; }
        public string Number { get; set; }
        public Int16? Active { get; set; }
        //[Required]
        public string SocSec { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Int16? PayFrequency { get; set; }
        public decimal? HourlyPayRate { get; set; }
        public Int16? DailyRate { get; set; }
        public DateTime? Hiredate { get; set; }
        public Int16? EarnedIncomeCredit { get; set; }
        public Int16? EarnedIncomeCreditChildren { get; set; }
        public Int16? MaritalStatus { get; set; }
        public Int16? Allowances { get; set; }
        public Int16? Dependents { get; set; }
        public Int16? Exemptions { get; set; }
        public decimal? FlatFit { get; set; }
        public Int16? AdditionalWitholdingFlag { get; set; }
        public decimal? AdditionalWitholdingAmount { get; set; }
        public DateTime? BirthDate { get; set; }
        public Int16? Race { get; set; }
        public Int16? Sex { get; set; }
        public Int16? PartTime { get; set; }
       //[Required]
        public Int16? FilingStatus { get; set; }
       // [Required]        
        public Int16? UsCitizen { get; set; }
        
        public int? TimeCalcFlag { get; set; }
        //[Required]
        public string TimeCalcModifier { get; set; }
        
        public bool? HeadOfHousehold { get; set; }
        public string Blind { get; set; }
        public bool? Student { get; set; }
        //public int RoleId { get; set; }
        //[Required]
        public string CityId { get; set; }
        //public int? CityId { get; set; }
        //  [Required]
        public int? StateId { get; set; }
       // [Required]
        public int? CountryId { get; set; }

        public long? UserId { get; set; }
        public decimal? Hours { get; set; }
    }
    public class GetEmployeeIdModel
    {
        public int EmployeeID { get; set; }
    }

    public class EmployeeSalaryDetailsModel
    {
        public string EnyEmployeeID { get; set; }
        public int EmployeeID { get; set; }
        public Int16? PayFrequency { get; set; }
        public decimal? HourlyPayRate { get; set; }
        public Int16? EarnedIncomeCredit { get; set; }
        public Int16? EarnedIncomeCreditChildren { get; set; }
        public Int16? Allowances { get; set; }
        public Int16? Dependents { get; set; }
        public Int16? Exemptions { get; set; }
        public decimal? FlatFit { get; set; }
        public decimal? AdditionalWitholdingAmount { get; set; }
        public Int16? PartTime { get; set; }
        public decimal? Hours { get; set; }
        //public long? UserId { get; set; }
    }

    public class EmployeePersonalDetailsModel
    {
        public string EnyEmployeeID { get; set; }
        public int EmployeeID { get; set; }
        public string CityId { get; set; }
        public int? StateId { get; set; }
        public int? CountryId { get; set; }
        public bool? HeadOfHousehold { get; set; }
        public string Blind { get; set; }
        public bool? Student { get; set; }
    }

    public class EmployeesDetails : CommonProp
    {
        [Key]
        public int EmployeeID { get; set; }
        public string EnyEmployeeID { get; set; }
        public string Number { get; set; }
        public Int16? Active { get; set; }
        //[Required]
        public string SocSec { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string EmployeeName { get; set; }
        public Int16? PayFrequency { get; set; }
        public decimal? HourlyPayRate { get; set; }
        public string Hourly_PayRate { get; set; }
        public Int16? DailyRate { get; set; }
        public string  JoiningDate { get; set; }
        public DateTime? Hiredate { get; set; }
        public Int16? EarnedIncomeCredit { get; set; }
        public Int16? EarnedIncomeCreditChildren { get; set; }
        public Int16? Allowances { get; set; }
        public Int16? Dependents { get; set; }
        public Int16? Exemptions { get; set; }
        public decimal? FlatFit { get; set; }
        public Int16? AdditionalWitholdingFlag { get; set; }
        public decimal? AdditionalWitholdingAmount { get; set; }

        public DateTime? BirthDate { get; set; }
        public Int16? Race { get; set; }
        public Int16? Sex { get; set; }

        public string Birth_Date { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string GenderName { get; set; }
        public string RaceName { get; set; }

        public Int16? MaritalStatus { get; set; }
        public string MaritalStatusName { get; set; }
        public string CityName { get; set; }
        public Int16? PartTime { get; set; }
        //[Required]
        public Int16? FilingStatus { get; set; }
        // [Required]        
        public Int16? UsCitizen { get; set; }

        public int? TimeCalcFlag { get; set; }
        //[Required]
        public string TimeCalcModifier { get; set; }

        public bool? HeadOfHousehold { get; set; }
        public string Blind { get; set; }
        public bool? Student { get; set; }
        //public int RoleId { get; set; }
        public string BasicSalary { get; set; }
        public long? UserId { get; set; }

        public string CityId { get; set; }
        //public int? CityId { get; set; }
        //  [Required]
        public int? StateId { get; set; }
        // [Required]
        public int? CountryId { get; set; }
        public decimal? Hours { get; set; }
        public decimal? TotalTime { get; set; }

        public string GrossSalary { get; set; }
        public int? PayorBankNumber { get; set; }
        public string PeriodName { get; set; }

    }
    public class ActiveEmployees
    {
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
    }
}

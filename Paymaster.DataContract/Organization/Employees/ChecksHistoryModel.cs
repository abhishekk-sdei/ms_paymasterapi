﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class ChecksHistoryModel
    {
        public int? CheckId { get; set; }
        public int? PayperiodId { get; set; }
        //public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int? EmployeeId { get; set; }
        public decimal? Gross { get; set; }
        public decimal? Amount { get; set; }
        public int? CheckNumber { get; set; }
        public DateTime? CheckDate { get; set; }
        public int? PayorBankNumber { get; set; }
        public string Notes { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public decimal? FederalTaxAmount { get; set; }
        public decimal? SocialSecurityTaxAmount { get; set; }
        public decimal? MedicareTaxAmount { get; set; }
        public decimal? StateTaxAmount { get; set; }
        public decimal? AdditionalFederalTaxAmount { get; set; }
        public decimal? PFML_MedicalAmount { get; set; }
        public decimal? PFML_LeaveAmount { get; set; }
        public decimal? TotalDeductionAmount { get; set; }
        public int TotalRecords { get; set; }
    }
}

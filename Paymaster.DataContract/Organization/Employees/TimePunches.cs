﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class TimePunches : CommonProp
    {
        [Key]
        public int TimePunchId { get; set; }
        public int? EmployeeId { get; set; }
        public int? CheckId { get; set; }
        public DateTime? InDateTime { get; set; }
        public DateTime? OutDateTime { get; set; }
        public string Type { get; set; }
        public DateTime? ReferenceDate { get; set; }
        public decimal? Hours { get; set; }
        public decimal? HoursClock { get; set; }
        public decimal? HourlyPayRate { get; set; }
        public decimal? OvertimeMultiplier { get; set; }
        public decimal? Amount { get; set; }
        public int? Closed { get; set; }
        public long? PayPeriodId { get; set; }
        public bool? timepuncheschangeChk { get; set; }
    }
}

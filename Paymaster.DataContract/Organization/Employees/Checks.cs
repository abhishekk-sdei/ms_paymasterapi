﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class Checks : CommonProp
    {
        [Key]
        public int? CheckId { get; set; }
        //public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? EmployeeId { get; set; }
        public decimal? Gross { get; set; }
        public decimal? Amount { get; set; }
        public int? CheckNumber { get; set; }
        public DateTime? CheckDate { get; set; }
        public int? PayorBankNumber { get; set; }
        public string Notes { get; set; }
        public int PayPeriodId { get; set; }

        public decimal? FederalTaxAmount { get; set; }
        public decimal? SocialSecurityTaxAmount { get; set; }
        public decimal? MedicareTaxAmount { get; set; }
        public decimal? StateTaxAmount { get; set; }
        public decimal? AdditionalFederalTaxAmount { get; set; }
        public decimal? PFML_MedicalAmount { get; set; }
        public decimal? PFML_LeaveAmount { get; set; }
        public decimal? EmployeeDeductionAmount { get; set; }

        public decimal? TotalDeductionAmount { get; set; }
    }

    public class ViewChecksModel
    {
        public int? CheckId { get; set; }
        public int? PayperiodId { get; set; }
        //public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? EmployeeId { get; set; }
        public decimal? Gross { get; set; }
        public decimal? Amount { get; set; }
        public int? CheckNumber { get; set; }
        public DateTime? CheckDate { get; set; }
        public int? PayorBankNumber { get; set; }
        public string Notes { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        //public decimal? FederalTaxAmount { get; set; }
        //public decimal? SocialSecurityTaxAmount { get; set; }
        //public decimal? MedicareTaxAmount { get; set; }
        //public decimal? StateTaxAmount { get; set; }
        //public decimal? AdditionalFederalTaxAmount { get; set; }
        //public decimal? PFML_MedicalAmount { get; set; }
        //public decimal? PFML_LeaveAmount { get; set; }
        public decimal? TotalDeductionAmount { get; set; }
        public int TotalRecords { get; set; }
    }

    public class ViewChecksDetails
    {
        public decimal? Regular { get; set; } //
        public decimal? Holiday { get; set; }//
        public decimal? Vacation { get; set; }//
        public decimal? Bonus { get; set; }//

        public decimal? RegularHour { get; set; }//
        public decimal? HolidayHour { get; set; }//
        public decimal? VacationHour { get; set; }//
        public decimal? BonusHour { get; set; }//

        public decimal? RegularPayRate { get; set; }//
        public decimal? HolidayPayRate { get; set; }//
        public decimal? VacationPayRate { get; set; }//
        public decimal? BonusPayRate { get; set; }//

        public decimal? RegularAssignedHours { get; set; }//
        public decimal? HolidayAssignedHours { get; set; }//
        public decimal? VacationAssignedHours { get; set; }//
        public decimal? BonusAssignedHours { get; set; }//

        public string PaidDate { get; set; }

        public decimal? IncomeTax { get; set; }
        public decimal? FederalTax { get; set; }
        public decimal? Medical { get; set; }
        public decimal? LifeInsurance { get; set; }
        public decimal? ProvidentFund { get; set; }
        public decimal? TotalEarning { get; set; }
        public decimal? TotalDeduction { get; set; }

        public int? CheckNumber { get; set; }
        public int? PayorBankNumber { get; set; }
        public string CheckDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Gross { get; set; }
        public decimal? HourlyPayRate { get; set; }
        public string Firstname { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Number { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationUrl { get; set; }
        public string OrganizationEmail { get; set; }
        public Int16? Allowances { get; set; }
        public Int16? Dependents { get; set; }
        public Int16? Exemptions { get; set; }

        public decimal? FederalTaxAmount { get; set; }
        public decimal? SocialSecurityTaxAmount { get; set; }
        public decimal? MedicareTaxAmount { get; set; }
        public decimal? StateTaxAmount { get; set; }
        public decimal? AdditionalFederalTaxAmount { get; set; }
        public decimal? PFML_MedicalAmount { get; set; }
        public decimal? PFML_LeaveAmount { get; set; }
        public decimal? EmployeeDeductionAmount { get; set; }
    }

    public class DropdownBindCheckList
    {
        public int? CheckId { get; set; }
        public int? CheckNumber { get; set; }
    }
}

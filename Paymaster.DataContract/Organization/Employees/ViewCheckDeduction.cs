﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class ViewCheckDeduction
    {
        public string DeductionTypeName { get; set; }
        public decimal Amount { get; set; }
    }
}

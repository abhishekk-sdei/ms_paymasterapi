﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class PayPeriods : CommonProp
    {
        [Key]
      public int PayPeriodId { get; set; }
      public Int16 PayFrequency { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CheckDate { get; set; }
        public DateTime TransferDate { get; set; }

        public Int16 Status { get; set; }
        public Int16 RunCount { get; set; }
        public Int16 InProgress { get; set; }
    }
    public class ViewPayPeriods
    {
        [Key]
        public int PayPeriodId { get; set; }
        public string PeriodName { get; set; }
        public string StartEmployee { get; set; }
        public string LastEmployee { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CheckDate { get; set; }
        public string TransferDate { get; set; }
        public Int16 Status { get; set; }
        public string StatusName { get; set; }
        public int ProcessStatus { get; set; }
        public int UserId { get; set; }
        public Int16 PayFrequency { get; set; }
        public int ProcessCHK { get; set; }
    }

    public class BindPayPeriodList
    {
        [Key]
        public int PayPeriodId { get; set; }
        public string PayPeriodName { get; set; }
    }
}

﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class TimePunchChanges : CommonProp
    {
        [Key]
       public int TimePunchChangeID {get;set;}
       public int TimePunchID {get;set;}
       public string FieldName {get;set;}
       public string TimePunchChangeFrom {get;set;}
       public string TimePunchChangeTo {get;set;}
       public string Notes {get;set;}
    }
}

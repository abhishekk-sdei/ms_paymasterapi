﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class StateExemptions : CommonProp
    {
        [Key]
        public int StateExemptionId { get; set; }
        public string State { get; set; }
        public Int16 Year { get; set; }
        public string Type { get; set; }
        public Int16 PayFrequency { get; set; }
        public decimal PerExemptionAmount { get; set; }
        public decimal Plus { get; set; }
        public decimal HeadOfHousehold { get; set; }
        public decimal Blind { get; set; }
        public decimal StudentLimit { get; set; }
    }
}

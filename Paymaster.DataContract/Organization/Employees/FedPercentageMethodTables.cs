﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public class FedPercentageMethodTables : CommonProp
    {
        [Key]
        public int FedPercentageMethodId { get; set; }
        public Int16 Year { get; set; }
        public Int16 PayFrequency { get; set; }
        public Int16 MaritalStatus { get; set; }
        public decimal FedPercentageMeathodOver { get; set; }
        public decimal Withhold { get; set; }
        public decimal PlusPercentage { get; set; }

    }
}

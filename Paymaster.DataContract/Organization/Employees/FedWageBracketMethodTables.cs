﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Paymaster.DataContract.Organization.Employees
{
    public  class FedWageBracketMethodTables : CommonProp
    {
        [Key]
        public int FedWageBracketMethodId { get; set; }
        public Int16 Year { get; set; }
        public Int16 PayFrequency { get; set; }
        public Int16 MaritalStatus { get; set; }
        public Int16 Allowances { get; set; }
        public decimal FedWageBracketMethodOver { get; set; }
        public decimal Subtract { get; set; }
        public decimal Percentage { get; set; }
    }
}

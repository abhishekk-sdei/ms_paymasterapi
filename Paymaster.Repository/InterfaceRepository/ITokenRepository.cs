﻿using Paymaster.DataContract.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Repository.InterfaceRepository
{
    public interface ITokenRepository
    {
        TokenModel GetDomain(OrganizationDetails domainToken);
    }
}

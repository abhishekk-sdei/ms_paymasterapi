﻿using Paymaster.DataContract.Organization.Employees;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Repository.InterfaceRepository.ICommonRepository
{
    public interface IStateTaxCalculation
    {
        public StateExemptions StateTaxCalulate(string year, Int32 payFrequency);
        public Decimal TaxPercentageMethod(Decimal? gross, Int16 exemptions, bool headOfHousehold, char blind, bool student);
    }
}

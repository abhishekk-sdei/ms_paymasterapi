﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Repository.InterfaceRepository.ICommonRepository
{
    public interface IFedTaxCalculation
    {
        public decimal FedTaxCalculate(string createDate, int payFrequency);
        public decimal TaxPercentage(decimal? gross, int martialStatus, int allowances);
        public decimal TaxBracket(decimal? gross, int martialStatus, int allowances);
    }
}

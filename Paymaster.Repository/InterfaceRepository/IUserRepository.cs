﻿using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Repository.InterfaceRepository
{
    public interface IUserRepository
    {
        TokenModel Authenticate(string username, string password);

        TokenModel AdminAuthenticate(string username, string password);
    }
}

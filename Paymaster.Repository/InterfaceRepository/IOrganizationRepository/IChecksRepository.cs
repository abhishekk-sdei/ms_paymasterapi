﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paymaster.Repository.InterfaceRepository.IOrganizationRepository
{
    public interface IChecksRepository
    {
        public JsonModel UpdateCheckDetail(Checks checks);
        public IQueryable<T> GetCheksList<T>(int page, int pageSize) where T : class, new();

        public IQueryable<T> GetDropdownBindCheckslist<T>(int Emp_ID, int PayPeriodId = 0) where T : class, new();

        public IQueryable<T> ViewChecksDetails<T>(int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0) where T : class, new();
    }
}

﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.DataContract.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paymaster.Repository.InterfaceRepository.IOrganizationRepository
{
    public interface IOrganizationRepository
    {
        //public List<Employees> GetEmployeeList();
        public IQueryable<T> GetEmployeeList<T>(int TimePunchCheck = 0, int page = 0, int pageSize = 10) where T : class, new();
        public IQueryable<T> GetAllEmployeeList<T>() where T : class, new();
        //public EmployeeDetailModel GetEmployeeList(int TimePunchCheck = 0);

        public Users CheckCredentials(Users users);

        //public List<Employees> GetEmployeeListByName(string Seaching);
        public IQueryable<T> GetEmployeeListByName<T>(string Seaching, int page = 0, int pageSize = 10) where T : class, new();
        public JsonModel DeleteEmployee(int EmployeeId);

        public IQueryable<T> GetEmployeeListById<T>(int EmployeeId) where T : class, new();
        public IQueryable<T> GetActiveEmployee<T>() where T : class, new();
        public IQueryable<T> GetPreviousEmployee<T>() where T : class, new();
    }
}

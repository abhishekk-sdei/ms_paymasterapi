﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paymaster.Repository.InterfaceRepository.IOrganizationRepository
{
    public interface IProcessPayrollRepository
    {
        // public List<PayPeriods> GetPayPeriodList();
        public IQueryable<T> GetPayPeriodList<T>(string seaching) where T : class, new();

        public IQueryable<T> GetAllPayPeriodList<T>() where T : class, new();
        public JsonModel payrollProcess(ViewPayPeriods payPeriods);

        public JsonModel CalculateAmount(Processpayrollviewmodel Payroll);
        public JsonModel GetIsChangesOnTimePuches();
        //public Task<MemoryStream> PrintEmployeeChecksAsync(int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0);
        public Task<string> PrintEmployeeChecksAsync( string FromEmployeeNO, string ToEmployeeNO,int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0);

        public JsonModel AddCheckIdInTaxDeduction(Processpayrollviewmodel Payroll);
        public JsonModel FromEmployeeList(string EmployeeNo,int EmployeeId = 0);
        public JsonModel ToEmployeeList(string EmployeeNo,int EmployeeId = 0);
    }
}

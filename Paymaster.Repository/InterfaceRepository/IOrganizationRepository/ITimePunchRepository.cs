﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paymaster.Repository.InterfaceRepository.IOrganizationRepository
{
    public interface ITimePunchRepository
    {
        // public List<TimePunches> GetTimePunchList(int EmployeeID);
        public IQueryable<T> GetTimePunchList<T>(int EmployeeID) where T : class, new();
        public IQueryable<T> GetTimePunchHistoryList<T>(int page = 0, int pageSize = 10, int Emp_ID = 0, int PayPeriodId = 0, string SearchText = "") where T : class, new();

        public IQueryable<T> GetEmployeecheckListHistory<T>(int page = 0, int pageSize = 10, int Emp_ID = 0, int PayPeriodId = 0, string SearchText = "") where T : class, new();
        public IQueryable<T> ViewTimePunchList<T>(int EmployeeID) where T : class, new();
        public TimePunhesDetailModel TimePunchList(int page, int pageSize);
        public TimePunhesDetailModel ViewEmployeeTimelist(int page, int pageSize);

        //public TimePunhesDetailModel GetEmployeeCheckDeductionList(int CheckId);

        public IQueryable<T> GetEmployeeCheckDeductionList<T>(int CheckId) where T : class, new();

        public TimePunhesDetailModel ViewEmployeeFilteredTimelist(int page, int pageSize, int EmployeeId = 0, int PayPeriodId = 0);

        public JsonModel AddTimePunch(TimePunches timePunches);

        public JsonModel UpdateTimePunch(TimePunches timePunches);
    }
}

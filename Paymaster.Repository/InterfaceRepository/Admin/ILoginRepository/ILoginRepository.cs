﻿using Paymaster.DataContract.Admin.Login;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paymaster.Repository.InterfaceRepository.Admin.ILoginRepository
{
    public interface ILoginRepository
    {
        public AdminUsers CheckAdminCredentials(AdminUsers users);

        public List<OrganizationDetails> GetOrganizationList();
        public OrganizationDetails GetOrganizationListById(int OrganizationId);

        public JsonModel SaveOrganization(ApplicationDetails ApplicationDetails);

        public JsonModel UpdateOrganization(OrganizationDetails organizationDetails);

        public JsonModel CheckDomainName(string DomainName);

        public IQueryable<T> CreateDynamicDB<T>(ApplicationDetails ApplicationDetails) where T : class, new();
        public JsonModel SetUpOrganization(Organization_Details ApplicationDetails);
    }
}

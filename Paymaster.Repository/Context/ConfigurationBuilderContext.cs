﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Paymaster.DataContract.Authentication;
//using Paymaster.RepositoryContract.User;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using static Paymaster.Common.Enum.CommonEnums;
using Paymaster.Common;
using Paymaster.DataContract.Common;
using Microsoft.Extensions.Primitives;
using static Paymaster.Common.ConstantString;

namespace Paymaster.Repository.Context
{
    public class ConfigurationBuilderContext
    {


        public string GetNewConnection(HttpContext httpContext,string conString,string hostName)
        {
            ConstantString constantString = new ConstantString();
            string host = string.Empty;
            StringValues authorizationToken;
            var tokenExist = httpContext.Request.Headers.TryGetValue("domainToken", out authorizationToken);// get host name from token
            if (tokenExist)
            {
                //get the host name from request
                string domainname = CommonMethod.Decrypt(httpContext.Request.Headers["domainToken"].ToString());
                host = domainname;
            }
            //else if (httpContext.Request.QueryString.Value.Contains("Business")) // before login get verified host name from query string
            //{
            //    host = httpContext.Request.QueryString.Value.Split("=")[1];
            //}
            //else
            //{
            //    httpContext.Request.Headers.TryGetValue("BusinessToken", out authorizationToken); //get host name from header
            //    host = CommonMethod.Decrypt(authorizationToken);
            //}

            //its only for debug mode
            else
            {
                //host = httpContext.Request.Host.ToString() == "localhost:55440" || httpContext.Request.Host.ToString() == "52.25.96.244:7003" ? "paymaster" : httpContext.Request.Host.ToString(); //its Merging db
                host = hostName; //its Merging db

            }
            host = hostName; //its Merging db

            //return new connetion string which made from request host
            //  host = "paymaster";
            return GetDomain(host, conString);

        }

        public string GetDomain(string host,string conString)
        {
            //TO DO Master connection string should be from app-setings
            var optionsBuilder = new DbContextOptionsBuilder<PaymasterDBContext>();
            optionsBuilder.UseSqlServer(CreateMasterConnectionString(conString));

            PaymasterDBContext _masterContext = new PaymasterDBContext(optionsBuilder.Options);

            string con = string.Empty;

            //get the organization from Business-Name

            OrganizationDetails org = _masterContext.OrganizationDetails.Where(a => a.DomainName == host && a.IsDeleted == false).FirstOrDefault();
            if (org != null)
            {
                con = ConnectionString(org);
            }
            //return new connection string
            return con;
        }

        public string ConnectionString(OrganizationDetails domainToken)
        {
            string conn = @"Server=" + domainToken.DatabaseServerName + ";Database=" + domainToken.DatabaseName + ";Trusted_Connection=True;MultipleActiveResultSets=true;Integrated Security=false;User ID=" + domainToken.DatabaseUsername + ";Password=" + domainToken.DatabasePassword + ";";
            return conn;
        }
        public string CreateOrganizationConnectionString(string conString)
        {

            OrganizationDetails domainToken = new OrganizationDetails();
            domainToken.DatabaseServerName = conString.Split(';').Where(r => r.Substring(0, r.LastIndexOf("=")) == "Server").Select(r => r.Substring(r.LastIndexOf("=") + 1)).FirstOrDefault();
            domainToken.DatabaseName = conString.Split(';').Where(r => r.Substring(0, r.LastIndexOf("=")) == "Database").Select(r => r.Substring(r.LastIndexOf("=") + 1)).FirstOrDefault();
            domainToken.DatabaseUsername = conString.Split(';').Where(r => r.Substring(0, r.LastIndexOf("=")) == "User ID").Select(r => r.Substring(r.LastIndexOf("=") + 1)).FirstOrDefault();
            domainToken.DatabasePassword = conString.Split(';').Where(r => r.Substring(0, r.LastIndexOf("=")) == "Password").Select(r => r.Substring(r.LastIndexOf("=") + 1)).FirstOrDefault();
            return ConnectionString(domainToken);

        }

        public string CreateMasterConnectionString(string conString)
        {
            OrganizationDetails domainToken = new OrganizationDetails();
            domainToken.DatabaseServerName = conString.Split(';').Where(r => r.Substring(0, r.LastIndexOf("=")) == "Server").Select(r => r.Substring(r.LastIndexOf("=") + 1)).FirstOrDefault();
            domainToken.DatabaseName = conString.Split(';').Where(r => r.Substring(0, r.LastIndexOf("=")) == "Database").Select(r => r.Substring(r.LastIndexOf("=") + 1)).FirstOrDefault();
            domainToken.DatabaseUsername = conString.Split(';').Where(r => r.Substring(0, r.LastIndexOf("=")) == "User ID").Select(r => r.Substring(r.LastIndexOf("=") + 1)).FirstOrDefault();
            domainToken.DatabasePassword = conString.Split(';').Where(r => r.Substring(0, r.LastIndexOf("=")) == "Password").Select(r => r.Substring(r.LastIndexOf("=") + 1)).FirstOrDefault();

            return ConnectionString(domainToken);
        }

    }
}

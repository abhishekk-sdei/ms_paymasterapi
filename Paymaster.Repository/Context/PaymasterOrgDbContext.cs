﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.DataContract.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace Paymaster.Repository.Context
{
    public class PaymasterOrgDbContext :DbContext
    {
        private IConfiguration _configuration;
        private readonly IHttpContextAccessor _contextAccessor;
        private ConfigurationBuilderContext _configurationBuilderContext = new ConfigurationBuilderContext();
       
        public PaymasterOrgDbContext(DbContextOptions<PaymasterOrgDbContext> options, IHttpContextAccessor contextAccessor, IConfiguration configuration) : base(options) { _contextAccessor = contextAccessor; _configuration = configuration; }

        /// <summary>
        /// override the context from http request
        /// </summary>
        /// <param name="optionsBuilder"></param>

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)

        {
            base.OnConfiguring(optionsBuilder);
              var NewDbConnection = new ConfigurationBuilderContext();

            //check http request is not null
            if (_contextAccessor.HttpContext != null)
            {
                //create new connection string from the http context
                string con = NewDbConnection.GetNewConnection(_contextAccessor.HttpContext, _configuration["ConnectionStrings:PMMasterDB"],_configuration["hostName"]);
                if (!string.IsNullOrEmpty(con))
                {
                    //create new option builder with new connection string
                    optionsBuilder.UseSqlServer(con);
                }
            }
            else
            {
                //first time when application load the "HttpContext" is null so we bind database wirth static one
                //if http context null then bind the context with default connection string
                optionsBuilder.UseSqlServer(_configurationBuilderContext.CreateOrganizationConnectionString(_configuration["ConnectionStrings:PMORGMasterDB"]));
            }
        }


        public DbSet<Employees> Employees { get; set; }
        public DbSet<PayPeriods> PayPeriods { get; set; }

        public DbSet<Users> Users { get; set; }

        public DbSet<TimePunches> TimePunches { get; set; }
        public DbSet<TimePunchChanges> TimePunchChanges { get; set; }
        public DbSet<EmployeeTimePunchChange> EmployeeTimePunchChange { get; set; }
        public DbSet<Checks> Checks { get; set; }

        public DbSet<CheckDeductions> CheckDeductions { get; set; }
        public DbSet<FedPercentageMethodTables> FedPercentageMethodTables { get; set; }
        public DbSet<FedAllowances> FedAllowances { get; set; }
        public DbSet<FedWageBracketMethodTables> FedWageBracketMethodTables { get; set; }
        public DbSet<StateExemptions> StateExemptions { get; set; }
        public DbSet<EmployeeDeductions> EmployeeDeductions { get; set; }
        public DbSet<ErrorLog> ErrorHandlerModel { get; set; }
        public IList<TEntity> ExecStoredProcedureWithOutOutput<TEntity>(string commandText, int totalOutputParams, params object[] parameters) where TEntity : class, new()
        {
            var connection = this.Database.GetDbConnection();
            IList<TEntity> result = new List<TEntity>();
            try
            {
                totalOutputParams = totalOutputParams == 0 ? 1 : totalOutputParams;
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                using (var cmd = connection.CreateCommand())
                {
                    AddParametersToDbCommand(commandText, parameters, cmd);
                    //using (var reader = cmd.ExecuteReader())
                    //{
                    //    result = DataReaderMapToList<TEntity>(reader);
                    //    reader.NextResult();
                    //}
                    cmd.ExecuteReader();
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }
        }

        public IList<TEntity> ExecStoredProcedureWithOutput<TEntity>(string commandText, int totalOutputParams, params object[] parameters) where TEntity : class, new()
        {
            var connection = this.Database.GetDbConnection();
            IList<TEntity> result = new List<TEntity>();
            try
            {
                totalOutputParams = totalOutputParams == 0 ? 1 : totalOutputParams;
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                using (var cmd = connection.CreateCommand())
                {
                    AddParametersToDbCommand(commandText, parameters, cmd);
                    using (var reader = cmd.ExecuteReader())
                    {
                        result = DataReaderMapToList<TEntity>(reader);
                        reader.NextResult();
                    }
                    // cmd.ExecuteReader();
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }
        }

        public EmployeeDetailModel ExecStoredProcedureWithEmployeeDetailOutput(string commandText, int totalOutputParams, params object[] parameters)
        {
            var connection = this.Database.GetDbConnection();
            EmployeeDetailModel result = new EmployeeDetailModel();
            try
            {
                totalOutputParams = totalOutputParams == 0 ? 1 : totalOutputParams;
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                using (var cmd = connection.CreateCommand())
                {
                    AddParametersToDbCommand(commandText, parameters, cmd);
                    using (var reader = cmd.ExecuteReader())
                    {
                        result.EmployeeViewModel = DataReaderMapToList<EmployeeViewModel>(reader).ToList();
                        reader.NextResult();
                        result.EmployeePayPeriodModel = DataReaderMapToList<EmployeePayPeriodModel>(reader).FirstOrDefault();
                        reader.NextResult();
                    }
                    // cmd.ExecuteReader();
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }
        }

        private void AddParametersToDbCommand(string commandText, object[] parameters, System.Data.Common.DbCommand cmd)
        {
            cmd.CommandText = commandText;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 1000;

            if (parameters != null)
            {
                foreach (var p in parameters)
                {
                    if (p != null)
                    {
                        cmd.Parameters.Add(p);
                    }
                }
            }
        }

        public static IList<T> DataReaderMapToList<T>(IDataReader dr)
        {
            IList<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())               //Solution - Check if property is there in the reader and then try to remove try catch code
                {
                    try
                    {
                        if (!object.Equals(dr[prop.Name], DBNull.Value))
                        {
                            prop.SetValue(obj, dr[prop.Name], null);
                        }
                    }
                    catch { continue; }
                }
                list.Add(obj);
            }
            return list;
        }




        public TimePunhesDetailModel ExecStoredProcedureWithTimepunchesDetailOutput(string commandText, int totalOutputParams, params object[] parameters)
        {
            var connection = this.Database.GetDbConnection();
            TimePunhesDetailModel result = new TimePunhesDetailModel();
            try
            {
                totalOutputParams = totalOutputParams == 0 ? 1 : totalOutputParams;
                EmployeePayPeriodModel _emppayPriod = new EmployeePayPeriodModel();
                List<TimePunhesViewModel> TimePunhesViewModel =new List<TimePunhesViewModel>();
                TimePunhesViewModel TimePunhesViewModel1 = null;
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                using (var cmd = connection.CreateCommand())
                {
                    AddParametersToDbCommand(commandText, parameters, cmd);
                    using (var reader = cmd.ExecuteReader())
                    {
                        // result.EmployeePayPeriodModel = DataReaderMapToList<EmployeePayPeriodModel>(reader).FirstOrDefault();

                        while (reader.Read())
                        {
                            _emppayPriod.PeriodName = reader["PeriodName"].ToString();
                            _emppayPriod.PayPeriodId = Convert.ToInt32(reader["PayPeriodId"]);
                            _emppayPriod.StartDate =Convert.ToDateTime(reader["StartDate"].ToString());
                            _emppayPriod.EndDate = Convert.ToDateTime(reader["EndDate"].ToString());
                        }
                        result.EmployeePayPeriodModel = _emppayPriod;
                        reader.NextResult();
                        
                        while (reader.Read())
                        {
                            TimePunhesViewModel1 = GetReaderforTimepunches(TimePunhesViewModel, reader);

                        }
                        result.TimePunhesViewModel = TimePunhesViewModel;
                        //reader.NextResult();
                    }
                    // cmd.ExecuteReader();
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }
        }

        private static TimePunhesViewModel GetReaderforTimepunches(List<TimePunhesViewModel> TimePunhesViewModel, System.Data.Common.DbDataReader reader)
        {
            TimePunhesViewModel TimePunhesViewModel1 = new TimePunhesViewModel();
            TimePunhesViewModel1.EmployeeId = (int)reader["EmployeeId"];
            TimePunhesViewModel1.Number = reader["Number"].ToString();
            TimePunhesViewModel1.Active = (short)reader["Active"];
            TimePunhesViewModel1.SocSec = reader["SocSec"].ToString();
            TimePunhesViewModel1.FirstName = reader["FirstName"].ToString();
            TimePunhesViewModel1.LastName = reader["Lastname"].ToString();
            TimePunhesViewModel1.MiddleName = reader["MiddleName"].ToString();
            TimePunhesViewModel1.EmployeeName = reader["EmployeeName"].ToString();
            TimePunhesViewModel1.BirthDate = Convert.ToDateTime(reader["BirthDate"]);
            TimePunhesViewModel1.CityName = reader["CityName"].ToString();
            TimePunhesViewModel1.CityName = reader["CityName"].ToString();
            TimePunhesViewModel1.StateName = reader["StateName"].ToString();
            TimePunhesViewModel1.CountryName = reader["CountryName"].ToString();
            TimePunhesViewModel1.GenderName = reader["GenderName"].ToString();
            TimePunhesViewModel1.RaceName = reader["RaceName"].ToString();
            TimePunhesViewModel1.RaceName = reader["RaceName"].ToString();
            TimePunhesViewModel1.MaritalStatusName = reader["MaritalStatusName"].ToString();
            TimePunhesViewModel1.Hourly_PayRate = reader["Hourly_PayRate"].ToString();
            TimePunhesViewModel1.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
            TimePunhesViewModel1.BasicSalary = reader["BasicSalary"].ToString();
            //TimePunhesViewModel1.GrossSalary = reader["GrossSalary"].ToString();
            TimePunhesViewModel1.TotalTime = Convert.ToDecimal(reader["TotalTime"]);
            TimePunhesViewModel1.TotalRecords = Convert.ToInt32(reader["TotalRecords"]);
            TimePunhesViewModel1.IsActive = Convert.ToInt32(reader["IsActive"]);

            TimePunhesViewModel.Add(TimePunhesViewModel1);
            return TimePunhesViewModel1;
        }
    }
}

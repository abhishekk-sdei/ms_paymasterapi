﻿using Microsoft.EntityFrameworkCore;
using Paymaster.DataContract.Admin.Login;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;

namespace Paymaster.Repository.Context
{
    public class PaymasterDBContext: DbContext
    {
        public PaymasterDBContext(DbContextOptions<PaymasterDBContext> options) : base(options) { }
        public DbSet<Users> Users { get; set; }

        public DbSet<OrganizationDetails> OrganizationDetails { get; set; }

        public DbSet<CountryMaster> CountryMaster { get; set; }

        public DbSet<StateMaster> StateMaster { get; set; }
        public DbSet<CityMaster> CityMaster { get; set; }

        public DbSet<MaritalStatusMaster> MaritalStatusMaster { get; set; }
        public DbSet<RaceMaster> RaceMaster { get; set; }
        public DbSet<GenderMaster> GenderMaster { get; set; }

        public DbSet<AdminUsers> AdminUsers { get; set; }
        public DbSet<PeriodMaster> PeriodMaster { get; set; }

        public DbSet<TypeMaster> TypeMaster { get; set; }
        public DbSet<ErrorLog> ErrorLog { get; set; }
        

        public IList<TEntity> ExecStoredProcedureWithOutOutput<TEntity>(string commandText, int totalOutputParams, params object[] parameters) where TEntity : class, new()
        {
            var connection = this.Database.GetDbConnection();
            IList<TEntity> result = new List<TEntity>();
            try
            {
                totalOutputParams = totalOutputParams == 0 ? 1 : totalOutputParams;
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                using (var cmd = connection.CreateCommand())
                {
                    AddParametersToDbCommand(commandText, parameters, cmd);
                    //using (var reader = cmd.ExecuteReader())
                    //{
                    //    result = DataReaderMapToList<TEntity>(reader);
                    //    reader.NextResult();
                    //}
                    cmd.ExecuteReader();
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }
        }

        public IList<TEntity> ExecStoredProcedureWithOutput<TEntity>(string commandText, int totalOutputParams, params object[] parameters) where TEntity : class, new()
        {
            var connection = this.Database.GetDbConnection();
            IList<TEntity> result = new List<TEntity>();
            try
            {
                totalOutputParams = totalOutputParams == 0 ? 1 : totalOutputParams;
                if (connection.State == ConnectionState.Closed) { connection.Open(); }
                using (var cmd = connection.CreateCommand())
                {
                    AddParametersToDbCommand(commandText, parameters, cmd);
                    using (var reader = cmd.ExecuteReader())
                    {
                        result = DataReaderMapToList<TEntity>(reader);
                        reader.NextResult();
                    }
                    //cmd.ExecuteReader();
                }
                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Close();
            }
        }

        private void AddParametersToDbCommand(string commandText, object[] parameters, System.Data.Common.DbCommand cmd)
        {
            cmd.CommandText = commandText;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 1000;

            if (parameters != null)
            {
                foreach (var p in parameters)
                {
                    if (p != null)
                    {
                        cmd.Parameters.Add(p);
                    }
                }
            }
        }

        public static IList<T> DataReaderMapToList<T>(IDataReader dr)
        {
            IList<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())               //Solution - Check if property is there in the reader and then try to remove try catch code
                {
                    try
                    {
                        if (!object.Equals(dr[prop.Name], DBNull.Value))
                        {
                            prop.SetValue(obj, dr[prop.Name], null);
                        }
                    }
                    catch { continue; }
                }
                list.Add(obj);
            }
            return list;
        }

    }
}

﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Paymaster.Common;
using Paymaster.DataContract.Admin.Login;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.User;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.Admin.ILoginRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using static Paymaster.Common.ConstantString;
using static Paymaster.Common.Enum.CommonEnums;

namespace Paymaster.Repository.Repository.Admin.LoginRepository
{
    public class LoginRepository: ILoginRepository
    {
        private readonly PaymasterDBContext _dbContext;
        //private readonly CommonRepository _commonRepository;
        
        public LoginRepository(PaymasterDBContext dbContext)
        {
            _dbContext = dbContext;            
        }
        public AdminUsers CheckAdminCredentials(AdminUsers users)
        {            
            return users = _dbContext.AdminUsers.Where(r => r.Email == users.Email && r.Password == users.Password && r.IsActive == true && r.IsDeleted == false).FirstOrDefault();          
        }

        public List<OrganizationDetails> GetOrganizationList()
        {

            List<OrganizationDetails> OrganizationDetailsList = new List<OrganizationDetails>();
            return OrganizationDetailsList = _dbContext.OrganizationDetails.ToList();
            //.Where(a => a.DomainName == domainToken.DomainName && a.IsActive == true && a.IsDeleted == false).FirstOrDefault();

        }

        public OrganizationDetails GetOrganizationListById(int OrganizationId)
        {

            OrganizationDetails OrganizationDetailsById = new OrganizationDetails();
            return OrganizationDetailsById = _dbContext.OrganizationDetails.Where(r => r.ApplicationId == OrganizationId).FirstOrDefault();
            //.Where(a => a.DomainName == domainToken.DomainName && a.IsActive == true && a.IsDeleted == false).FirstOrDefault();

        }


        public JsonModel SetUpOrganization(Organization_Details ApplicationDetails)
        {

            try
            {                
                Random generator = new Random();
                ApplicationDetails.DomainName = "PM_" + generator.Next(0, 999999).ToString("D6");
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@OrganizationName", ApplicationDetails.OrganizationName),
                    new SqlParameter("@Email", ApplicationDetails.OrganizationEmail),
                     new SqlParameter("@OrganizationContactNumber", ApplicationDetails.OrganizationContactNumber),
                     new SqlParameter("@Password", ApplicationDetails.Password),
                     new SqlParameter("@Country", ApplicationDetails.Country),
                     new SqlParameter("@DomainName", ApplicationDetails.DomainName),
                     new SqlParameter("@DatabaseName", ApplicationDetails.DomainName+ "DB"),
                     new SqlParameter("@ApplicationURL", ApplicationDetails.DomainName+ ".paymaster.com"),
                     new SqlParameter("@DatabaseServerName",  PayMasterConnectionStringEnum.Server),
                     new SqlParameter("@DatabaseUsername", PayMasterConnectionStringEnum.User),
                     new SqlParameter("@DatabasePassword", PayMasterConnectionStringEnum.Password),
                     new SqlParameter("@UserName", ApplicationDetails.OrganizationName),
                     new SqlParameter("@IPAddress", CommonMethod.GetIPAddress())
                     //new SqlParameter("@IPAddress", Dns.GetHostEntry(Dns.GetHostName()).AddressList)
                     // new SqlParameter("@DomainName", 1),
                };
                 _dbContext.ExecStoredProcedureWithOutOutput<Organization_Details>("SaveOrganization", parameters.Length, parameters).AsQueryable();
                return new JsonModel()
                {
                    data = new object(),
                    Message = ConstantString.StatusMessage.OrgSaveSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                SqlParameter[] parameters1 =
                  {
                   //new SqlParameter("@GetType",ex.GetType().ToString()),
                    new SqlParameter("@StackTrace",ex.StackTrace),
                     new SqlParameter("@Message", ex.Message),
                     //new SqlParameter("@Password", ex.Data.Count),
                };

                _dbContext.ExecStoredProcedureWithOutOutput<Organization_Details>("sp_errorHandling", parameters1.Length, parameters1).AsQueryable();
                //ErrorLog objErrorHandler = new ErrorLog();
                //objErrorHandler.ErrorNumber = 1;
                //objErrorHandler.ErrorType = ex.GetType().ToString();
                //objErrorHandler.ErrorLine = 1;
                //objErrorHandler.ErrorTime = DateTime.UtcNow;
                //objErrorHandler.ErrorState = ex.StackTrace;
                //objErrorHandler.ErrorSeverity = ex.Data.Count;
                //objErrorHandler.ErrorMessage = ex.Message; //+ " Inner exception " + ex.InnerException.Message;
                //objErrorHandler.ErrorProcedure = "";
                //_dbContext.ErrorLog.Add(objErrorHandler);
                //_dbContext.SaveChanges();
                return new JsonModel()
                {
                    data = ex,
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
               // CommonRepository _commonRepository = new CommonRepository();
            }
        }

        public JsonModel SaveOrganization(ApplicationDetails ApplicationDetails)
        {
            try
            {
                //sa Abhishek 
                OrganizationDetails organizationDetails = new OrganizationDetails();
                organizationDetails.OrganizationEmail = ApplicationDetails.OrganizationEmail;
                organizationDetails.OrganizationName = ApplicationDetails.OrganizationName;
                organizationDetails.OrganizationUrl = ApplicationDetails.OrganizationUrl;
                organizationDetails.DomainName = ApplicationDetails.DomainName;
                organizationDetails.StartFrom = ApplicationDetails.StartFrom;
                organizationDetails.EndDate = ApplicationDetails.EndDate;
                //string HostName = Dns.GetHostName();
                organizationDetails.IPAddress = Dns.GetHostByName(Dns.GetHostName()).AddressList[1].ToString();
                organizationDetails.DatabaseServerName = PayMasterConnectionStringEnum.Server; 
                organizationDetails.DatabaseUsername = PayMasterConnectionStringEnum.User;
                organizationDetails.DatabasePassword = PayMasterConnectionStringEnum.Password;
                organizationDetails.ApplicationURL = organizationDetails.DomainName + ".paymaster.com";
                organizationDetails.DatabaseName = organizationDetails.DomainName + "DB";

                //ea Abhishek 
                organizationDetails.IsActive = true;
                organizationDetails.CreatedBy = (int)CommonEnumField.OrgUserID;
                organizationDetails.PeriodId = ApplicationDetails.PeriodId;
                organizationDetails.CreatedDate = DateTime.Now;
                organizationDetails.IsDeleted = false;
                var emp = _dbContext.OrganizationDetails.Add(organizationDetails);
                _dbContext.SaveChanges();

            
                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.OrgSaveSuccessMsg,
                    StatusCode = (int)HttpStatusCodes.OK
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message + " "+ex.StackTrace.ToString()+" "+ ex.InnerException.ToString(),
                    StatusCode = (int)HttpStatusCodes.BadRequest
                };
            }
        }

        public IQueryable<T> CreateDynamicDB<T>(ApplicationDetails ApplicationDetails) where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                    {
                    new SqlParameter("@DomainName", ApplicationDetails.DomainName),
                    new SqlParameter("@UserName", ApplicationDetails.UserName),
                     new SqlParameter("@Password", ApplicationDetails.Password),
                     // new SqlParameter("@DomainName", 1),
                      new SqlParameter("@Email", ApplicationDetails.Email)
                };
                return _dbContext.ExecStoredProcedureWithOutOutput<T>("CreateDynamicDB", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public JsonModel UpdateOrganization(OrganizationDetails organizationDetails)
        {
            try
            {
                var entity = _dbContext.OrganizationDetails.FirstOrDefault(item => item.ApplicationId == organizationDetails.ApplicationId);
                if (entity != null)
                {
                    // Answer for question #2

                    // Make changes on entity
                    entity.OrganizationName = organizationDetails.OrganizationName;
                    entity.ApplicationURL = organizationDetails.ApplicationURL;
                    //entity.DatabaseUsername = organizationDetails.DatabaseUsername;
                    //entity.DatabasePassword = organizationDetails.DatabasePassword;
                    entity.StartFrom = organizationDetails.StartFrom;
                    entity.EndDate = organizationDetails.EndDate;
                    entity.PeriodId = organizationDetails.PeriodId;
                   // entity.DatabaseServerName = organizationDetails.DatabaseServerName;
                    entity.OrganizationEmail = organizationDetails.OrganizationEmail;
                    entity.UpdatedDate = DateTime.Now;
                    entity.UpdatedBy = organizationDetails.UpdatedBy;


                    entity.DatabaseServerName = PayMasterConnectionStringEnum.Server;
                    entity.DatabaseUsername = PayMasterConnectionStringEnum.User;
                    entity.DatabasePassword = PayMasterConnectionStringEnum.Password;


                    // Save changes in database
                    _dbContext.SaveChanges();
                }

                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.OrgUpdateSuccessMsg,
                    StatusCode = (int)HttpStatusCodes.OK
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = (int)HttpStatusCodes.BadRequest
                };
            }
        }

        public JsonModel CheckDomainName(string DomainName)
        {
            //return null;
            try
            {
                List<OrganizationDetails> orglist = new List<OrganizationDetails>();
                orglist = _dbContext.OrganizationDetails.Where(r => r.DomainName == DomainName && r.IsDeleted == false && r.IsActive == true).ToList();
                if (orglist.Count > 0)
                {
                    return new JsonModel()
                    {
                        data = new object(),
                        Message = StatusMessage.DomainExist,
                        StatusCode = (int)HttpStatusCodes.Conflict
                    };
                }
                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.DomainNotExist,
                    StatusCode = (int)HttpStatusCodes.OK
                };
            }

            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = (int)HttpStatusCodes.BadRequest
                };
            }
        }
    }
}



﻿using Paymaster.DataContract.Authentication;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Paymaster.Repository.Context;
using Paymaster.Common;
using Paymaster.Repository.InterfaceRepository;

namespace Paymaster.Repository.Repository
{
    public class TokenRepository: ITokenRepository
    {
        private readonly PaymasterDBContext _dbcontext;

        public TokenRepository(PaymasterDBContext dbcontext)
        {
            _dbcontext = dbcontext;
        }
        public TokenModel GetDomain(OrganizationDetails domainToken)
        {
            try
            {
                //OrganizationDetails dbDomainData = new OrganizationDetails();
                OrganizationDetails masterOrg = _dbcontext.OrganizationDetails.Where(a => a.DomainName.ToLower() == domainToken.DomainName.ToLower() && a.IsActive == true && a.IsDeleted == false).FirstOrDefault();


                if (masterOrg != null)
                {
                    TokenModel torg = new TokenModel();
                    //OrganizationDatabaseDetail orgDatabaseDetails = _masterContext.OrganizationDatabaseDetail.Where(a => a.Id == masterOrg.DatabaseDetailId).FirstOrDefault();
                    torg.AccessToken = CommonMethod.Encrypt(masterOrg.DomainName);
                    torg.OrganizationID = masterOrg.ApplicationId;
                   
                    return torg;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}

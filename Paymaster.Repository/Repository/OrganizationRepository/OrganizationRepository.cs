﻿using Microsoft.Data.SqlClient;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.DataContract.User;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Paymaster.Common.ConstantString;
using static Paymaster.Common.Enum.CommonEnums;

namespace Paymaster.Repository.Repository.OrganizationRepository
{
    public class OrganizationRepository : IOrganizationRepository
    {
        private readonly PaymasterOrgDbContext _OrgdbContext;
        public OrganizationRepository(PaymasterOrgDbContext OrgdbContext)
        {
            _OrgdbContext = OrgdbContext;
        }
        //public List<Employees> GetEmployeeList()
        public IQueryable<T> GetEmployeeList<T>(int TimePunchCheck = 0, int page = 0, int pageSize = 10) where T : class, new()
        {
            try
            {

                SqlParameter[] parameters =
                  {
                    //new SqlParameter("@TimePunchCheck", TimePunchCheck)
                    new SqlParameter("@PageNumber", page),
                    new SqlParameter("@RowspPage", pageSize)
                };
                //var a = _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_GetEmployeeDetail(new)", parameters.Length, parameters).AsQueryable();
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetEmployees", parameters.Length, parameters).AsQueryable();
                
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> GetAllEmployeeList<T>() where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                  {

                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetAllEmployees", parameters.Length, parameters).AsQueryable();

            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public EmployeeDetailModel GetEmployeeList(int TimePunchCheck = 0)
        {
            try
            {
                //List<EmployeesDetails> EmployeesList = new List<EmployeesDetails>();
                //  return EmployeesList = _OrgdbContext.Employees.Where(r => r.IsActive == true && r.IsDeleted == false).OrderByDescending(r => r.EmployeeID).ToList();
                SqlParameter[] parameters =
                  {
                    new SqlParameter("@TimePunchCheck", TimePunchCheck)
                };
                //return _OrgdbContext.ExecStoredProcedureWithEmployeeDetailOutput("sp_GetEmployeeDetail(new)", parameters.Length, parameters);
                return _OrgdbContext.ExecStoredProcedureWithEmployeeDetailOutput("GetEmployees", parameters.Length, parameters);

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> GetEmployeeListById<T>(int EmployeeId) where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                  {
                    new SqlParameter("@EmployeeId", EmployeeId)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_GetEmployeeDetailById", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> GetActiveEmployee<T>() where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                  {
                    //new SqlParameter("@EmployeeId", EmployeeId)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetActiveEmployees", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> GetPreviousEmployee<T>() where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                  {
                    //new SqlParameter("@EmployeeId", EmployeeId)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetPerviousEmployees", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public Users CheckCredentials(Users users)
        {
            try
            { 
            return users = _OrgdbContext.Users.Where(r => r.Email == users.Email && r.Password == users.Password && r.IsActive == true && r.IsDeleted == false).FirstOrDefault();
            }
            catch(Exception ex)
            {
                throw;
            }

        }

        //public List<Employees> GetEmployeeListByName(string Seaching)
        //{
        //    try
        //    { 
        //    List<Employees> EmployeesList = new List<Employees>();
        //    return EmployeesList = _OrgdbContext.Employees.Where(r => (r.FirstName.Contains(Seaching) 
        //    || r.MiddleName.Contains(Seaching) ||r.LastName.Contains(Seaching) || r.Number.Contains(Seaching)
        //    || r.SocSec.Contains(Seaching)) && r.IsActive == true && r.IsDeleted == false
        //    ).OrderByDescending(r => r.EmployeeID).ToList();

        //    }
        //    catch (Exception ex) {
        //        throw;
        //    }


        //}
        public IQueryable<T> GetEmployeeListByName<T>(string Seaching, int page = 0, int pageSize = 10) where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                  {
                    new SqlParameter("@Seaching", Seaching),
                    new SqlParameter("@PageNumber", page),
                    new SqlParameter("@RowspPage", pageSize)
                };
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetFilteringEmployeesList", parameters.Length, parameters).AsQueryable();
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetEmployees", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public JsonModel DeleteEmployee(int EmployeeId)
        {
            try
            { 
            var entity = _OrgdbContext.Employees.Where(item => item.EmployeeID == EmployeeId && item.IsActive == true && item.IsDeleted == false).FirstOrDefault();
            if(entity != null)
            {
                entity.IsActive = false;
                entity.IsDeleted = true;
                entity.DeletedDate = DateTime.UtcNow;
                    entity.DeletedBy = 3;

                _OrgdbContext.SaveChanges();

                    return new JsonModel()
                    {
                        StatusCode = Convert.ToInt32(HttpStatusCodes.OK),
                        Message = StatusMessage.DeleteEmpSuccessMsg
                    };
            }
            else
                {
                    return new JsonModel()
                    {
                        StatusCode = Convert.ToInt32(HttpStatusCodes.NotFound),
                        Message = StatusMessage.NoFetchMessage
                    };
                }
            }
            catch(Exception ex) 
            {
                return new JsonModel()
                {
                    StatusCode = Convert.ToInt32(HttpStatusCodes.InternalServerError),
                    Message = ex.Message
                };
            }
        }
    }
}

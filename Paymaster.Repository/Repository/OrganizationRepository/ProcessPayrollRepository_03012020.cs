﻿using Microsoft.Data.SqlClient;
using Paymaster.Common.TaxCalculate;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.DataContract.Organization.ViewModel;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paymaster.Repository.InterfaceRepository.ICommonRepository;
using System.Transactions;
using static Paymaster.Common.ConstantString;
using static Paymaster.Common.Enum.CommonEnums;

namespace Paymaster.Repository.Repository.OrganizationRepository
{
    public class ProcessPayrollRepository : IProcessPayrollRepository
    {
        private readonly PaymasterOrgDbContext _OrgdbContext;
        private readonly PaymasterDBContext _dbContext;
        private readonly IFedTaxCalculation _fedTaxCal;
        private readonly IStateTaxCalculation _stateTaxCal;
        public ProcessPayrollRepository(PaymasterOrgDbContext OrgdbContext, PaymasterDBContext dbContext, IFedTaxCalculation fedTaxCal, IStateTaxCalculation stateTaxCal)
        {
            _OrgdbContext = OrgdbContext;
            _dbContext = dbContext;
            _fedTaxCal = fedTaxCal;
            _stateTaxCal = stateTaxCal;
        }
        //sa old code of pay period list
        //public List<PayPeriods> GetPayPeriodList()
        //{
        //    try
        //    {
        //        List<ViewPayPeriods> ViewPayPeriods = new List<ViewPayPeriods>();
        //        List<PayPeriods> PeriodMasterList = new List<PayPeriods>();

        //       return  PeriodMasterList = _OrgdbContext.PayPeriods.Where(r =>  r.IsActive == true && r.IsDeleted == false
        //        ).OrderByDescending(r => r.PayPeriodId).ToList();

        //        //foreach(var lst in PeriodMasterList)
        //        //{
        //        //    ViewPayPeriods[lst.PayPeriodId].PayPeriodId = lst.PayPeriodId;
        //        //    ViewPayPeriods[lst.PayPeriodId].PayPeriodName = dbContext
        //        //    ViewPayPeriods[lst.PayPeriodId].StartDate = lst.StartDate;
        //        //    ViewPayPeriods[lst.PayPeriodId].EndDate = lst.EndDate;
        //        //    ViewPayPeriods[lst.PayPeriodId].Status = lst.Status;
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }

        //}
        //ea old code of pay period list


        public IQueryable<T> GetPayPeriodList<T>(string Seaching = "") where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                {
                     new SqlParameter("@Seaching", Seaching)
                };
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_GetPayPeriodlist", parameters.Length, parameters).AsQueryable();
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetPayPeriodlist", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> GetAllPayPeriodList<T>() where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                {
                     //new SqlParameter("@Seaching", Seaching)
                };
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_GetPayPeriodlist", parameters.Length, parameters).AsQueryable();
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("ViewPayPeriodList", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public JsonModel payrollProcess(ViewPayPeriods payPeriods)
        {
            try
            {

                int _applicationID = Convert.ToInt32(CommonEnumField.ApplicationID);
                SqlParameter[] parameters =
                 {
                    new SqlParameter("@applicationId",  _applicationID),
                    new SqlParameter("@payperiodId",  payPeriods.PayPeriodId),
                    new SqlParameter("@userId",  payPeriods.UserId)


                };
                _OrgdbContext.ExecStoredProcedureWithOutOutput<PayPeriods>("sp_ChangePayrollRrocessStatus", parameters.Length, parameters).AsQueryable();
                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.ProcessedSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)

                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)

                };
            }

        }


        public JsonModel CalculateAmount(Processpayrollviewmodel Payroll)
        {
            try
            {
                if(Payroll.payperiodId <= 0)
                {
                    return new JsonModel()
                    {
                        data = new object(),
                        Message = StatusMessage.ProcessedErrorMsg,
                        StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                    };
                }
                else
                {
                    //var result = _OrgdbContext.ExecStoredProcedureWithOutOutput<TotalAmount>("sp_FinalAmount", Payroll.payperiodId,  Payroll.payFrequency, Payroll.startDate, Payroll.endDate);

                    //sa Original code
                    //var query = (from timePuches in _OrgdbContext.TimePunches.Where()
                    //             group timePuches by timePuches.EmployeeId into grp
                    //             select new
                    //             {
                    //                 employeeID = grp.Key,
                    //                 gross = grp.Sum(x=>x.Amount),
                    //             });
                    //ea Original code

                    //sa only for debugging
                    var query = (from timePuches in _OrgdbContext.TimePunches.Where(r => r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false)
                                 group timePuches by timePuches.EmployeeId into grp
                                 select new
                                 {
                                     employeeID = grp.Key,
                                     gross = grp.Sum(x => x.Amount),
                                 });
                  //  var ChkEmpChangeTimePunches = _OrgdbContext.EmployeeTimePunchChange.Where()
                        var ChkEmpChangeTimePunches = query.Join(// outer sequence 
                      _OrgdbContext.EmployeeTimePunchChange.Where(r=>r.IsActive == true && r.IsDeleted == false && r.PayPeriodId == Payroll.payperiodId),  // inner sequence 
                      r => r.employeeID,    // outerKeySelector
                      x => x.EmployeeId,  // innerKeySelector
                      (r, x) => new  // result selector
                      {
                          employeeID = r.employeeID,
                          gross = r.gross
                      });
                    //ea only for debugging

                    //foreach (var check in _OrgdbContext.Checks)
                    //{
                    //    check.Gross = 0;
                    //    check.Amount = 0;
                    //}

                    foreach(var checkDeduct in _OrgdbContext.CheckDeductions)
                    {
                        checkDeduct.Amount = 0;
                    }

                    var fedTax = _fedTaxCal.FedTaxCalculate(Payroll.createDate, Payroll.payFrequency);
                     var stateTax = _stateTaxCal.StateTaxCalulate(Payroll.createDate, Payroll.payFrequency);

                    //  foreach (var item in query)
                    foreach (var item in ChkEmpChangeTimePunches)
                    {
                        int checkId = 0;
                        decimal gross = item.gross ?? default(decimal);
                        decimal totalTax = 0, totalDeductions = 0;

                        var EmpTimePunchChnages = _OrgdbContext.EmployeeTimePunchChange.Where(r => r.EmployeeId == item.employeeID).SingleOrDefault();
                        if (EmpTimePunchChnages != null)
                        {
                            EmpTimePunchChnages.IsActive = false;
                            EmpTimePunchChnages.IsDeleted = true;
                            _OrgdbContext.EmployeeTimePunchChange.Update(EmpTimePunchChnages);
                        }

                        foreach (var check in _OrgdbContext.Checks.Where(r=>r.EmployeeId == item.employeeID && r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false))
                        {
                            check.Gross = 0;
                            check.Amount = 0;
                        }

                        List<Employees> _empRow = (from employee in _OrgdbContext.Employees
                                          where employee.EmployeeID == item.employeeID
                                          select employee
                                          ).ToList();
                        List<EmployeeDeductions> _empDeduction = (from deduction in _OrgdbContext.EmployeeDeductions.Where(r=> r.IsActive == true && r.IsDeleted == false)
                                                                  where deduction.EmployeeId == item.employeeID
                                                            select deduction
                                                            ).ToList();
                        if (_empRow.Count() != 1)
                            continue;
                        //sa old code
                        //List<Checks> _checkRow = (from check in _OrgdbContext.Checks
                        //                          where check.EmployeeId == item.employeeID
                        //                          select check
                        //                          ).ToList();
                        // sa old code
                        List<Checks> _checkRow = (from check in _OrgdbContext.Checks.Where(r => r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false)
                                                  where check.EmployeeId == item.employeeID                                                   
                                                  select check
                                                  ).ToList();

                        if(_checkRow.Count() == 1)
                        {
                            _checkRow[0].Gross = gross;
                            _checkRow[0].Amount = 0;
                            _checkRow[0].FirstName = _empRow[0].FirstName;
                            _checkRow[0].LastName = _empRow[0].LastName;

                            checkId = Convert.ToInt32(_checkRow[0].CheckId);
                        }
                        else if (_checkRow.Count() == 0)
                        {
                            Checks _newCheckRow = new Checks();
                            _newCheckRow.EmployeeId = item.employeeID;
                            _newCheckRow.PayPeriodId = Payroll.payperiodId;
                            _newCheckRow.Gross = gross;
                            _newCheckRow.Amount = 0;
                            _newCheckRow.CheckNumber = 0;
                            _newCheckRow.CheckDate = Convert.ToDateTime(Payroll.checkDate.Split('/')[1] + "/" + Payroll.checkDate.Split('/')[0] + "/" + Payroll.checkDate.Split('/')[2]);
                            _newCheckRow.FirstName = _empRow[0].FirstName;
                            _newCheckRow.LastName = _empRow[0].LastName;
                            _newCheckRow.Notes = "";
                            _newCheckRow.CreatedDate = DateTime.UtcNow;
                            _newCheckRow.CreatedBy = Payroll.createdBy;
                            _newCheckRow.IsActive = true;
                            _newCheckRow.IsDeleted = false;
                            _OrgdbContext.Checks.Add(_newCheckRow);
                            checkId = Convert.ToInt32(_newCheckRow.CheckId);
                            _checkRow = (from check in _OrgdbContext.Checks
                                         where check.EmployeeId == item.employeeID
                                         select check
                                                  ).ToList();
                        }
                        else
                        {
                            //will throw error
                        }

                        if(checkId != 0)
                        {
                            //sa old code
                            //foreach (var res in _OrgdbContext.TimePunches
                            //{
                            //    if (Convert.ToInt32(res.EmployeeId) == item.employeeID)
                            //        res.CheckId = checkId;
                            //}
                            //ea old code
                            foreach (var res in _OrgdbContext.TimePunches.Where(r => r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false))
                            {
                                if (Convert.ToInt32(res.EmployeeId) == item.employeeID)
                                    res.CheckId = checkId;
                            }
                            decimal taxMedicare = 0M;
                            decimal taxSocialSecurity = 0M;
                            decimal taxFed = 0M;
                            decimal taxState = 0M;
                            decimal employeeDeductionsTotal = 0M;

                            //sa new code
                            decimal _empDeductionAmt = 0M;
                            //ea new code

                            //Calculate Medicare Witholding
                            taxMedicare = (decimal)item.gross * .0145M;
                            taxMedicare = Math.Round(taxMedicare, 2);

                            //sa old code for medicare tax
                            //List<CheckDeductions> medicareRow = _OrgdbContext.CheckDeductions.Where(x=>x.CheckId == checkId && x.DeductionTypeId == 3 && x.IsActive == true && x.IsDeleted == false).ToList();
                            //if (medicareRow.Count() == 1)
                            //{

                            //    medicareRow[0].Amount = taxMedicare;
                            //    medicareRow[0].Notes = "";

                            //}
                            //else if (medicareRow.Count() == 0)
                            //{

                            //    CheckDeductions _checkDeductionNewRow = new CheckDeductions();
                            //   _checkDeductionNewRow.CheckId = checkId;
                            //   _checkDeductionNewRow.DeductionTypeId = 3;
                            //   _checkDeductionNewRow.EmployeeDeductionId = 0;
                            //   _checkDeductionNewRow.Amount = taxMedicare;
                            //   _checkDeductionNewRow.Notes = "";
                            //    _checkDeductionNewRow.CreatedBy = Payroll.createdBy;
                            //    _checkDeductionNewRow.CreatedDate = DateTime.UtcNow;
                            //    _checkDeductionNewRow.IsActive = true;
                            //    _checkDeductionNewRow.IsDeleted = false;
                            //    _OrgdbContext.CheckDeductions.Add(_checkDeductionNewRow);

                            //}
                            //else
                            //{
                            //    //throw error;
                            //}
                            //ea old code for medicare tax

                            //Calculate Social Security Witholding
                            taxSocialSecurity = (decimal)item.gross * .062M;
                            taxSocialSecurity = Math.Round(taxSocialSecurity, 2);

                            //sa old code for social security
                            //List<CheckDeductions> socialSecurityTax = (from checkDeduct in _OrgdbContext.CheckDeductions.Where(r=> r.IsActive == true && r.IsDeleted == false)
                            //                               where checkDeduct.CheckId == checkId && checkDeduct.DeductionTypeId == 2
                            //                               select checkDeduct
                            //                               ).ToList();

                            //if (socialSecurityTax.Count() == 1)
                            //{ 
                            //    socialSecurityTax[0].Amount = taxSocialSecurity;
                            //    socialSecurityTax[0].Notes = "";
                            //}
                            //else if (socialSecurityTax.Count() == 0)
                            //{

                            //    CheckDeductions newDeductioRow = new CheckDeductions();
                            //    newDeductioRow.CheckId = checkId;
                            //    newDeductioRow.DeductionTypeId = 2;
                            //    newDeductioRow.EmployeeDeductionId = 0;
                            //    newDeductioRow.Amount = taxSocialSecurity;
                            //    newDeductioRow.Notes = "";
                            //    newDeductioRow.CreatedBy = Payroll.createdBy;
                            //    newDeductioRow.CreatedDate = DateTime.UtcNow;
                            //    newDeductioRow.IsActive = true;
                            //    newDeductioRow.IsDeleted = false;
                            //    _OrgdbContext.CheckDeductions.Add(newDeductioRow);
                            //}
                            //else
                            //{
                            //    // not possible  SHOULD THROW ERROR HERE!!!
                            //}
                            //ea old code for social security
                            //Calculate Federal Witholding
                            taxFed = _fedTaxCal.TaxPercentage(item.gross, Convert.ToInt16(_empRow[0].MaritalStatus), Convert.ToInt16(_empRow[0].Allowances));

                            //sa old code for fed tax
                            //var fedTaxRow = (from fedtax in _OrgdbContext.CheckDeductions.Where(r =>  r.IsActive == true && r.IsDeleted == false)
                            //                 where fedtax.CheckId == checkId && fedtax.DeductionTypeId == 1
                            //                 select fedtax
                            //                ).ToList();
                            //if (fedTaxRow.Count() == 1)
                            //{ 

                            //    fedTaxRow[0].Amount = taxFed;
                            //    fedTaxRow[0].Notes = "";

                            //}
                            //else if (fedTaxRow.Count() == 0)
                            //{
                            //    CheckDeductions fedTaxx = new CheckDeductions();
                            //    fedTaxx.CheckId = checkId;
                            //    fedTaxx.DeductionTypeId = 1;
                            //    fedTaxx.EmployeeDeductionId = 0;
                            //    fedTaxx.Amount = taxFed;
                            //    fedTaxx.Notes = "";
                            //    fedTaxx.CreatedBy = Payroll.createdBy;
                            //    fedTaxx.CreatedDate = DateTime.UtcNow;
                            //    fedTaxx.IsActive = true;
                            //    fedTaxx.IsDeleted = false;
                            //    _OrgdbContext.CheckDeductions.Add(fedTaxx);
                            //}
                            //else
                            //{
                            //    // THROW ERROR HERE!!!
                            //}
                            //ea old code for fed tax

                            //Calculate Deductions from employeeDeductionsTable
                            employeeDeductionsTotal = 0;

                            

                            foreach (var employeeDeduction in _empDeduction)
                            {
                                Int32 deductionTypeID = Convert.ToInt32(employeeDeduction.DeductionTypeId);
                                Int32 employeeDeductionID = Convert.ToInt32(employeeDeduction.DeductionTypeId);
                                decimal amount = Convert.ToDecimal(employeeDeduction.Amount);

                                //sa new code for Employee deduction
                                _empDeductionAmt += amount;
                                //sa new code for Employee deduction

                                //sa old code for Employee deduction
                                //List<CheckDeductions> additionalDeduct = (from cDeduct in _OrgdbContext.CheckDeductions.Where(r =>  r.IsActive == true && r.IsDeleted == false)
                                //                                          where cDeduct.CheckId == checkId && cDeduct.DeductionTypeId == deductionTypeID && cDeduct.EmployeeDeductionId == employeeDeductionID
                                //                                          select cDeduct).ToList();
                                //if (additionalDeduct.Count() == 1)
                                //{

                                //    additionalDeduct[0].Amount = amount;
                                //    additionalDeduct[0].Notes = "";
                                //    employeeDeductionsTotal = amount;

                                //}
                                //else if (additionalDeduct.Count() == 0)
                                //{
                                //    CheckDeductions cdeduct = new CheckDeductions();
                                //    cdeduct.CheckId = checkId;
                                //    cdeduct.DeductionTypeId = deductionTypeID;
                                //    cdeduct.EmployeeDeductionId = employeeDeductionID;
                                //    cdeduct.Amount = amount;
                                //    cdeduct.Notes = "";
                                //    cdeduct.CreatedBy = Payroll.createdBy;
                                //    cdeduct.CreatedDate = DateTime.UtcNow;
                                //    cdeduct.IsActive = true;
                                //    cdeduct.IsDeleted = false;
                                //    _OrgdbContext.CheckDeductions.Add(cdeduct);
                                //    employeeDeductionsTotal = amount;

                                //}
                                //else
                                //{
                                //    //  THROW ERROR 
                                //}
                                //ea old code for Employee deduction


                            }

                            //#region  Calculate State Witholding

                            decimal tempGross = gross - (taxMedicare + taxSocialSecurity);

                            taxState = _stateTaxCal.TaxPercentageMethod(tempGross, Convert.ToInt16(_empRow[0].Exemptions), Convert.ToBoolean(_empRow[0].HeadOfHousehold), Convert.ToChar(_empRow[0].Blind), Convert.ToBoolean(_empRow[0].Student));

                            //sa old code for state tax
                            //List<CheckDeductions> stateTaxRow = (from deduct in _OrgdbContext.CheckDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
                            //                            where deduct.CheckId == checkId && deduct.DeductionTypeId == 4
                            //                            select deduct
                            //                         ).ToList();

                            //if (stateTaxRow.Count() == 1)
                            //{  

                            //    stateTaxRow[0].Amount = taxState;
                            //    stateTaxRow[0].Notes = "MA";

                            //}
                            //else if (stateTaxRow.Count() == 0)
                            //{
                            //    CheckDeductions obj = new CheckDeductions();
                            //    obj.CheckId = checkId;
                            //    obj.DeductionTypeId = 4;
                            //    obj.EmployeeDeductionId = 0;
                            //    obj.Amount = taxState;
                            //    obj.Notes = "MA";
                            //    obj.CreatedBy = Payroll.createdBy;
                            //    obj.CreatedDate = DateTime.UtcNow;
                            //    obj.IsActive = true;
                            //    obj.IsDeleted = false;
                            //    _OrgdbContext.CheckDeductions.Add(obj);

                            //}
                            //else
                            //{

                            //}
                            //ea old code for state tax

                            _checkRow[0].Amount = (gross - taxMedicare - taxSocialSecurity - taxFed - taxState - employeeDeductionsTotal);

                            //sa New Code
                            _checkRow[0].StateTaxAmount = taxState;
                            _checkRow[0].EmployeeDeductionAmount = _empDeductionAmt;
                            _checkRow[0].FederalTaxAmount = taxFed;
                            _checkRow[0].SocialSecurityTaxAmount = taxSocialSecurity;
                            _checkRow[0].MedicareTaxAmount = taxMedicare;
                            //ea new Code

                            _OrgdbContext.Checks.Update(_checkRow[0]);

                            //EmployeeTimePunchChange employeeTimePunchChange = new EmployeeTimePunchChange();


                        
                           


                        }
                    }
                    using (TransactionScope transaction = new TransactionScope()) 
                    {
                        _OrgdbContext.SaveChanges();
                        transaction.Complete();
                    }


                }
                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.ProcessedSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch(Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }




    }
}

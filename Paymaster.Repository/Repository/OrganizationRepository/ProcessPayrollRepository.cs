﻿using Microsoft.Data.SqlClient;
using Paymaster.Common.TaxCalculate;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.DataContract.Organization.ViewModel;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Paymaster.Repository.InterfaceRepository.ICommonRepository;
using System.Transactions;
using static Paymaster.Common.ConstantString;
using static Paymaster.Common.Enum.CommonEnums;
using Paymaster.DataContract.Organization.ViewModel.TemplatesViewModel;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using iText.Kernel.Pdf;
using iText.Html2pdf;
using iTextSharp.text;
using iText.Kernel.Geom;
using Microsoft.AspNetCore.Hosting;

namespace Paymaster.Repository.Repository.OrganizationRepository
{
    public class ProcessPayrollRepository : IProcessPayrollRepository
    {
        private readonly PaymasterOrgDbContext _OrgdbContext;
        private readonly PaymasterDBContext _dbContext;
        private readonly IFedTaxCalculation _fedTaxCal;
        private readonly IStateTaxCalculation _stateTaxCal;

        private readonly IRazorViewEngine _razorViewEngine;
        private readonly IServiceProvider _serviceProvider;
        private readonly ITempDataProvider _tempDataProvider;
        private readonly IHostingEnvironment _HostingEnvironment;

        public ProcessPayrollRepository(IHostingEnvironment HostingEnvironment, IRazorViewEngine razorViewEngine, IServiceProvider serviceProvider, ITempDataProvider tempDataProvider, PaymasterOrgDbContext OrgdbContext, PaymasterDBContext dbContext, IFedTaxCalculation fedTaxCal, IStateTaxCalculation stateTaxCal)
        {

            _OrgdbContext = OrgdbContext;
            _dbContext = dbContext;
            _fedTaxCal = fedTaxCal;
            _stateTaxCal = stateTaxCal;
            _razorViewEngine = razorViewEngine;
            _serviceProvider = serviceProvider;
            _tempDataProvider = tempDataProvider;
            _HostingEnvironment = HostingEnvironment;
        }
        //sa old code of pay period list
        //public List<PayPeriods> GetPayPeriodList()
        //{
        //    try
        //    {
        //        List<ViewPayPeriods> ViewPayPeriods = new List<ViewPayPeriods>();
        //        List<PayPeriods> PeriodMasterList = new List<PayPeriods>();

        //       return  PeriodMasterList = _OrgdbContext.PayPeriods.Where(r =>  r.IsActive == true && r.IsDeleted == false
        //        ).OrderByDescending(r => r.PayPeriodId).ToList();

        //        //foreach(var lst in PeriodMasterList)
        //        //{
        //        //    ViewPayPeriods[lst.PayPeriodId].PayPeriodId = lst.PayPeriodId;
        //        //    ViewPayPeriods[lst.PayPeriodId].PayPeriodName = dbContext
        //        //    ViewPayPeriods[lst.PayPeriodId].StartDate = lst.StartDate;
        //        //    ViewPayPeriods[lst.PayPeriodId].EndDate = lst.EndDate;
        //        //    ViewPayPeriods[lst.PayPeriodId].Status = lst.Status;
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }

        //}
        //ea old code of pay period list


        public IQueryable<T> GetPayPeriodList<T>(string Seaching = "") where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                {
                     new SqlParameter("@Seaching", Seaching)
                };
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_GetPayPeriodlist", parameters.Length, parameters).AsQueryable();
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetPayPeriodlist", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> GetAllPayPeriodList<T>() where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                {
                     //new SqlParameter("@Seaching", Seaching)
                };
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_GetPayPeriodlist", parameters.Length, parameters).AsQueryable();
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("ViewPayPeriodList", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public JsonModel payrollProcess(ViewPayPeriods payPeriods)
        {
            try
            {

                int _applicationID = Convert.ToInt32(CommonEnumField.ApplicationID);
                SqlParameter[] parameters =
                 {
                    new SqlParameter("@applicationId",  _applicationID),
                    new SqlParameter("@payperiodId",  payPeriods.PayPeriodId),
                    new SqlParameter("@userId",  payPeriods.UserId)


                };
                _OrgdbContext.ExecStoredProcedureWithOutOutput<PayPeriods>("sp_ChangePayrollprocessStatus", parameters.Length, parameters).AsQueryable();
                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.ProcessedSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)

                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)

                };
            }

        }

        public string ConvertToDDMMYYYY(string _date) 
        {
            
            string output;
            if(_date.Split("/").Length  == 1) 
            {
                string[] input = _date.Split("-");
                output = input[2].ToString() + "/" + input[1].ToString() + "/" + input[0].ToString();
            }
            else
            {
                string[] input = _date.Split("/");
                output = input[1].ToString() + "/" + input[0].ToString() + "/" + input[2].ToString();
            }
            return output;
        }

        public JsonModel CalculateAmount(Processpayrollviewmodel Payroll)
        {
           
            try
            {
                string _message = "";
                int _statusCode = 0;
                if (Payroll.payperiodId <= 0)
                {
                    _message = StatusMessage.ProcessedErrorMsg;
                    _statusCode = Convert.ToInt32(HttpStatusCodes.BadRequest);
                    //return new JsonModel()
                    //{
                    //    data = new object(),
                    //    Message = StatusMessage.ProcessedErrorMsg,
                    //    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                    //};
                }
                else
                {
                    var query = (from timePuches in _OrgdbContext.TimePunches.Where(r => r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false)
                                 group timePuches by timePuches.EmployeeId into grp
                                 select new
                                 {
                                     employeeID = grp.Key,
                                     gross = grp.Sum(x => x.Amount),
                                 });
                    //  var ChkEmpChangeTimePunches = _OrgdbContext.EmployeeTimePunchChange.Where()
                    var ChkEmpChangeTimePunches = _OrgdbContext.EmployeeTimePunchChange.Where(r => r.IsActive == true && r.IsDeleted == false && r.PayPeriodId == Payroll.payperiodId).Select(x => x.EmployeeId).ToList();  // inner sequence 
                    var changeInTimePunchForEmployee = query.Where(x => ChkEmpChangeTimePunches.Contains(x.employeeID)).ToList();

                    Payroll.createDate = ConvertToDDMMYYYY(Payroll.createDate);
                    var fedTax = _fedTaxCal.FedTaxCalculate(Payroll.createDate, Payroll.payFrequency);
                    var stateTax = _stateTaxCal.StateTaxCalulate(Payroll.createDate, Payroll.payFrequency);
                    int _processStatus = 0;
                    foreach (var item in changeInTimePunchForEmployee)
                    {
                        _processStatus = 1;
                        int checkId = 0;
                        decimal gross = item.gross ?? default(decimal);
                        decimal totalTax = 0, totalDeductions = 0;
                        Checks _newCheckRow = new Checks();
                        var EmpTimePunchChnages = _OrgdbContext.EmployeeTimePunchChange.Where(r => r.EmployeeId == item.employeeID).SingleOrDefault();
                        if (EmpTimePunchChnages != null)
                        {
                            EmpTimePunchChnages.IsActive = false;
                            EmpTimePunchChnages.IsDeleted = true;
                            _OrgdbContext.EmployeeTimePunchChange.Update(EmpTimePunchChnages);
                        }

                        foreach (var check in _OrgdbContext.Checks.Where(r => r.EmployeeId == item.employeeID && r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false))
                        {
                            check.Gross = 0;
                            check.Amount = 0;
                        }



                        List<Employees> _empRow = (from employee in _OrgdbContext.Employees
                                                   where employee.EmployeeID == item.employeeID
                                                   select employee
                                          ).ToList();
                        List<EmployeeDeductions> _empDeduction = (from deduction in _OrgdbContext.EmployeeDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
                                                                  where deduction.EmployeeId == item.employeeID
                                                                  select deduction
                                                            ).ToList();
                        if (_empRow.Count() != 1)
                            continue;
                        List<Checks> _checkRow = (from check in _OrgdbContext.Checks.Where(r => r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false)
                                                  where check.EmployeeId == item.employeeID
                                                  select check
                                                  ).ToList();

                        if (_checkRow.Count() == 1)
                        {
                            checkId = Convert.ToInt32(_checkRow[0].CheckId);
                        }
                        else if (_checkRow.Count() == 0)
                        {

                            checkId = Convert.ToInt32(_newCheckRow.CheckId);

                        }
                        else
                        {
                            //will throw error
                        }

                        foreach (var checkDeduct in _OrgdbContext.CheckDeductions.Where(r => r.CheckId == checkId && r.IsActive == true && r.IsDeleted == false))
                        {
                            checkDeduct.Amount = 0;
                        }

                        //if (checkId != 0)
                        //{
                        var _timePunchesVal = _OrgdbContext.TimePunches.Where(r => r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false);
                        foreach (var res in _timePunchesVal)
                        {
                            if (Convert.ToInt32(res.EmployeeId) == item.employeeID)
                                res.CheckId = checkId;
                        }

                        decimal taxMedicare = 0M;
                        decimal taxSocialSecurity = 0M;
                        decimal taxFed = 0M;
                        decimal taxState = 0M;
                        decimal employeeDeductionsTotal = 0M;

                        //sa new code
                        decimal _empDeductionAmt = 0M;
                        //ea new code

                        //Calculate Medicare Witholding
                        taxMedicare = (decimal)item.gross * .0145M;
                        taxMedicare = Math.Round(taxMedicare, 2);

                        List<CheckDeductions> medicareRow = _OrgdbContext.CheckDeductions.Where(x => x.CheckId == checkId && x.DeductionTypeId == 3 && x.IsActive == true && x.IsDeleted == false).ToList();
                        if (medicareRow.Count() == 1)
                        {
                            //medicareRow[0].Amount = taxMedicare;
                            //medicareRow[0].Notes = "";
                            //List<CheckDeductions> medicareRow = _OrgdbContext.CheckDeductions.Where(x => x.CheckId == checkId && x.DeductionTypeId == 3 && x.IsActive == true && x.IsDeleted == false).ToList();
                            if (medicareRow.Count() == 1)
                            {
                                medicareRow[0].Amount = taxMedicare;
                                medicareRow[0].Notes = "";
                                medicareRow[0].UpdatedBy = Payroll.createdBy;
                                medicareRow[0].UpdatedDate = DateTime.UtcNow;
                                _OrgdbContext.CheckDeductions.Update(medicareRow[0]);

                            }
                            else if (medicareRow.Count() == 0)
                            {

                                CheckDeductions _checkDeductionNewRow = new CheckDeductions();
                                _checkDeductionNewRow.CheckId = checkId;
                                _checkDeductionNewRow.DeductionTypeId = 3;
                                _checkDeductionNewRow.EmployeeDeductionId = 0;
                                _checkDeductionNewRow.Amount = taxMedicare;
                                _checkDeductionNewRow.Notes = "";
                                _checkDeductionNewRow.CreatedBy = Payroll.createdBy;
                                _checkDeductionNewRow.CreatedDate = DateTime.UtcNow;
                                _checkDeductionNewRow.IsActive = true;
                                _checkDeductionNewRow.IsDeleted = false;
                                _checkDeductionNewRow.EmployeeId = item.employeeID;
                                _checkDeductionNewRow.Payperiod = Payroll.payperiodId;


                                _OrgdbContext.CheckDeductions.Add(_checkDeductionNewRow);

                            }
                            else
                            {
                                //throw error;
                            }

                            //Calculate Social Security Witholding
                            taxSocialSecurity = (decimal)item.gross * .062M;
                            taxSocialSecurity = Math.Round(taxSocialSecurity, 2);

                            List<CheckDeductions> socialSecurityTax = (from checkDeduct in _OrgdbContext.CheckDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
                                                                       where checkDeduct.CheckId == checkId && checkDeduct.DeductionTypeId == 2
                                                                       select checkDeduct
                                                           ).ToList();

                            if (socialSecurityTax.Count() == 1)
                            {
                                socialSecurityTax[0].Amount = taxSocialSecurity;
                                socialSecurityTax[0].Notes = "";
                                socialSecurityTax[0].UpdatedBy = Payroll.createdBy;
                                socialSecurityTax[0].UpdatedDate = DateTime.UtcNow;
                                _OrgdbContext.CheckDeductions.Update(socialSecurityTax[0]);
                            }
                            else if (socialSecurityTax.Count() == 0)
                            {

                                CheckDeductions newDeductioRow = new CheckDeductions();
                                newDeductioRow.CheckId = checkId;
                                newDeductioRow.DeductionTypeId = 2;
                                newDeductioRow.EmployeeDeductionId = 0;
                                newDeductioRow.Amount = taxSocialSecurity;
                                newDeductioRow.Notes = "";
                                newDeductioRow.CreatedBy = Payroll.createdBy;
                                newDeductioRow.CreatedDate = DateTime.UtcNow;
                                newDeductioRow.IsActive = true;
                                newDeductioRow.IsDeleted = false;
                                newDeductioRow.EmployeeId = item.employeeID;
                                newDeductioRow.Payperiod = Payroll.payperiodId;
                                _OrgdbContext.CheckDeductions.Add(newDeductioRow);
                            }
                            else
                            {
                                // not possible  SHOULD THROW ERROR HERE!!!
                            }
                            //Calculate Federal Witholding
                            taxFed = _fedTaxCal.TaxPercentage(item.gross, Convert.ToInt16(_empRow[0].MaritalStatus), Convert.ToInt16(_empRow[0].Allowances));

                            var fedtaxrow = (from fedtax in _OrgdbContext.CheckDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
                                             where fedtax.CheckId == checkId && fedtax.DeductionTypeId == 1
                                             select fedtax
                                            ).ToList();
                            if (fedtaxrow.Count() == 1)
                            {

                                fedtaxrow[0].Amount = taxFed;
                                fedtaxrow[0].Notes = "";
                                fedtaxrow[0].UpdatedBy = Payroll.createdBy;
                                fedtaxrow[0].UpdatedDate = DateTime.UtcNow;
                                _OrgdbContext.CheckDeductions.Update(fedtaxrow[0]);

                            }
                            else if (fedtaxrow.Count() == 0)
                            {
                                CheckDeductions fedtaxx = new CheckDeductions();
                                fedtaxx.CheckId = checkId;
                                fedtaxx.DeductionTypeId = 1;
                                fedtaxx.EmployeeDeductionId = 0;
                                fedtaxx.Amount = taxFed;
                                fedtaxx.Notes = "";
                                fedtaxx.CreatedBy = Payroll.createdBy;
                                fedtaxx.CreatedDate = DateTime.UtcNow;
                                fedtaxx.IsActive = true;
                                fedtaxx.IsDeleted = false;
                                fedtaxx.EmployeeId = item.employeeID;
                                fedtaxx.Payperiod = Payroll.payperiodId;
                                _OrgdbContext.CheckDeductions.Add(fedtaxx);
                            }
                            else
                            {
                                // throw error here!!!
                            }

                            //Calculate Deductions from employeeDeductionsTable
                            employeeDeductionsTotal = 0;



                            foreach (var employeeDeduction in _empDeduction)
                            {
                                Int32 deductionTypeID = Convert.ToInt32(employeeDeduction.DeductionTypeId);
                                Int32 employeeDeductionID = Convert.ToInt32(employeeDeduction.DeductionTypeId);
                                decimal amount = Convert.ToDecimal(employeeDeduction.Amount);



                                List<CheckDeductions> additionalDeduct = (from cDeduct in _OrgdbContext.CheckDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
                                                                          where cDeduct.CheckId == checkId && cDeduct.DeductionTypeId == deductionTypeID && cDeduct.EmployeeDeductionId == employeeDeductionID
                                                                          select cDeduct).ToList();
                                if (additionalDeduct.Count() == 1)
                                {

                                    additionalDeduct[0].Amount = amount;
                                    additionalDeduct[0].Notes = "";
                                    employeeDeductionsTotal = amount;
                                    additionalDeduct[0].UpdatedBy = Payroll.createdBy;
                                    additionalDeduct[0].UpdatedDate = DateTime.UtcNow;
                                    _OrgdbContext.CheckDeductions.Update(additionalDeduct[0]);

                                }
                                else if (additionalDeduct.Count() == 0)
                                {
                                    CheckDeductions cdeduct = new CheckDeductions();
                                    cdeduct.CheckId = checkId;
                                    cdeduct.DeductionTypeId = deductionTypeID;
                                    cdeduct.EmployeeDeductionId = employeeDeductionID;
                                    cdeduct.Amount = amount;
                                    cdeduct.Notes = "";
                                    cdeduct.CreatedBy = Payroll.createdBy;
                                    cdeduct.CreatedDate = DateTime.UtcNow;
                                    cdeduct.IsActive = true;
                                    cdeduct.IsDeleted = false;
                                    cdeduct.EmployeeId = item.employeeID;
                                    cdeduct.Payperiod = Payroll.payperiodId;
                                    _OrgdbContext.CheckDeductions.Add(cdeduct);
                                    employeeDeductionsTotal = amount;

                                }
                                else
                                {
                                    //  THROW ERROR 
                                }


                            }

                            //#region  Calculate State Witholding

                            decimal tempGross = gross - (taxMedicare + taxSocialSecurity);

                            taxState = _stateTaxCal.TaxPercentageMethod(tempGross, Convert.ToInt16(_empRow[0].Exemptions), Convert.ToBoolean(_empRow[0].HeadOfHousehold), Convert.ToChar(_empRow[0].Blind), Convert.ToBoolean(_empRow[0].Student));

                            List<CheckDeductions> stateTaxRow = (from deduct in _OrgdbContext.CheckDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
                                                                 where deduct.CheckId == checkId && deduct.DeductionTypeId == 4
                                                                 select deduct
                                                     ).ToList();

                            if (stateTaxRow.Count() == 1)
                            {

                                stateTaxRow[0].Amount = taxState;
                                stateTaxRow[0].Notes = "MA";
                                stateTaxRow[0].UpdatedBy = Payroll.createdBy;
                                stateTaxRow[0].UpdatedDate = DateTime.UtcNow;
                                _OrgdbContext.CheckDeductions.Update(stateTaxRow[0]);

                            }
                            else if (stateTaxRow.Count() == 0)
                            {
                                CheckDeductions obj = new CheckDeductions();
                                obj.CheckId = checkId;
                                obj.DeductionTypeId = 4;
                                obj.EmployeeDeductionId = 0;
                                obj.Amount = taxState;
                                obj.Notes = "MA";
                                obj.CreatedBy = Payroll.createdBy;
                                obj.CreatedDate = DateTime.UtcNow;
                                obj.IsActive = true;
                                obj.IsDeleted = false;
                                obj.EmployeeId = item.employeeID;
                                obj.Payperiod = Payroll.payperiodId;
                                _OrgdbContext.CheckDeductions.Add(obj);

                            }
                            else
                            {

                            }

                            if (_checkRow.Count() == 1)
                            {
                                _checkRow[0].Gross = gross;
                                _checkRow[0].Amount = (gross - taxMedicare - taxSocialSecurity - taxFed - taxState - employeeDeductionsTotal);
                                _checkRow[0].FirstName = _empRow[0].FirstName;
                                _checkRow[0].LastName = _empRow[0].LastName;
                                _checkRow[0].TotalDeductionAmount = taxMedicare + taxSocialSecurity + taxFed + taxState + employeeDeductionsTotal;

                                //_checkRow[0].StateTaxAmount = taxState;
                                //_checkRow[0].EmployeeDeductionAmount = _empDeductionAmt;
                                //_checkRow[0].FederalTaxAmount = taxFed;
                                //_checkRow[0].SocialSecurityTaxAmount = taxSocialSecurity;
                                //_checkRow[0].MedicareTaxAmount = taxMedicare;

                                // checkId = Convert.ToInt32(_checkRow[0].CheckId);
                                _OrgdbContext.Checks.Update(_checkRow[0]);
                            }
                            else if (_checkRow.Count() == 0)
                            {
                                // Checks _newCheckRow = new Checks();
                                _newCheckRow.EmployeeId = item.employeeID;
                                _newCheckRow.PayPeriodId = Payroll.payperiodId;
                                _newCheckRow.Gross = gross;
                                _newCheckRow.Amount = (gross - taxMedicare - taxSocialSecurity - taxFed - taxState - employeeDeductionsTotal);
                                _newCheckRow.TotalDeductionAmount = taxMedicare + taxSocialSecurity + taxFed + taxState + employeeDeductionsTotal;
                                _newCheckRow.CheckNumber = 0;
                                _newCheckRow.CheckDate = Convert.ToDateTime(Payroll.checkDate.Split('/')[1] + "/" + Payroll.checkDate.Split('/')[0] + "/" + Payroll.checkDate.Split('/')[2]);
                                _newCheckRow.FirstName = _empRow[0].FirstName;
                                _newCheckRow.LastName = _empRow[0].LastName;
                                _newCheckRow.Notes = "";
                                _newCheckRow.CreatedDate = DateTime.UtcNow;
                                _newCheckRow.CreatedBy = Payroll.createdBy;
                                _newCheckRow.IsActive = true;
                                _newCheckRow.IsDeleted = false;

                                //_newCheckRow.StateTaxAmount = taxState;
                                //_newCheckRow.EmployeeDeductionAmount = _empDeductionAmt;
                                //_newCheckRow.FederalTaxAmount = taxFed;
                                //_newCheckRow.SocialSecurityTaxAmount = taxSocialSecurity;
                                //_newCheckRow.MedicareTaxAmount = taxMedicare;


                                _OrgdbContext.Checks.Add(_newCheckRow);
                                //checkId = Convert.ToInt32(_newCheckRow.CheckId);
                                //_checkRow = (from check in _OrgdbContext.Checks
                                //             where check.EmployeeId == item.employeeID
                                //             select check
                                //                      ).ToList();
                            }
                            else
                            {
                                //will throw error
                            }

                            //EmployeeTimePunchChange employeeTimePunchChange = new EmployeeTimePunchChange();





                            //}
                        }

                   
                      

                    }
                    if (_processStatus == 1)
                    {
                        _message = StatusMessage.ProcessedSuccessMsg;
                        _statusCode = Convert.ToInt32(HttpStatusCodes.OK);
                    }
                    else
                    {
                        _message = StatusMessage.NoProcessRecord;
                        _statusCode = Convert.ToInt32(HttpStatusCodes.NotFound);
                    }


                    using (TransactionScope transaction = new TransactionScope())
                    {
                        _OrgdbContext.SaveChanges();
                        transaction.Complete();
                    }

                }
                return new JsonModel()
                {
                    data = new object(),
                    Message = _message,
                    StatusCode = _statusCode
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }



        #region Calculate Amount and add in check table

        //public JsonModel CalculateAmount(Processpayrollviewmodel Payroll)
        //{
        //    try
        //    {
        //        string _message;
        //        int _statusCode;
        //        if (Payroll.payperiodId <= 0)
        //        {
        //            _message = StatusMessage.ProcessedErrorMsg;
        //            _statusCode = Convert.ToInt32(HttpStatusCodes.BadRequest);
        //        }
        //        else
        //        {
        //            //var result = _OrgdbContext.ExecStoredProcedureWithOutOutput<TotalAmount>("sp_FinalAmount", Payroll.payperiodId,  Payroll.payFrequency, Payroll.startDate, Payroll.endDate);



        //            //sa only for debugging
        //            var query = (from timePuches in _OrgdbContext.TimePunches.Where(r => r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false)
        //                         group timePuches by timePuches.EmployeeId into grp
        //                         select new
        //                         {
        //                             employeeID = grp.Key,
        //                             gross = grp.Sum(x => x.Amount),
        //                         });
        //            //  var ChkEmpChangeTimePunches = _OrgdbContext.EmployeeTimePunchChange.Where()
        //            var ChkEmpChangeTimePunches = _OrgdbContext.EmployeeTimePunchChange.Where(r => r.IsActive == true && r.IsDeleted == false && r.PayPeriodId == Payroll.payperiodId).Select(x => x.EmployeeId).ToList();  // inner sequence 
        //            var changeInTimePunchForEmployee = query.Where(x => ChkEmpChangeTimePunches.Contains(x.employeeID)).ToList();


        //            //ea only for debugging

        //            //foreach (var check in _OrgdbContext.Checks)
        //            //{
        //            //    check.Gross = 0;
        //            //    check.Amount = 0;
        //            //}

        //            foreach (var checkDeduct in _OrgdbContext.CheckDeductions)
        //            {
        //                checkDeduct.Amount = 0;
        //            }

        //            var fedTax = _fedTaxCal.FedTaxCalculate(Payroll.createDate, Payroll.payFrequency);
        //            var stateTax = _stateTaxCal.StateTaxCalulate(Payroll.createDate, Payroll.payFrequency);




        //            //  foreach (var item in query)
        //            int _processStatus = 0;
        //            foreach (var item in changeInTimePunchForEmployee)
        //            {
        //                _processStatus = 1;
        //                int checkId = 0;
        //                decimal gross = item.gross ?? default(decimal);
        //                decimal totalTax = 0, totalDeductions = 0;

        //                var EmpTimePunchChnages = _OrgdbContext.EmployeeTimePunchChange.Where(r => r.EmployeeId == item.employeeID).SingleOrDefault();
        //                if (EmpTimePunchChnages != null)
        //                {
        //                    EmpTimePunchChnages.IsActive = false;
        //                    EmpTimePunchChnages.IsDeleted = true;
        //                    _OrgdbContext.EmployeeTimePunchChange.Update(EmpTimePunchChnages);
        //                }

        //                foreach (var check in _OrgdbContext.Checks.Where(r => r.EmployeeId == item.employeeID && r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false))
        //                {
        //                    check.Gross = 0;
        //                    check.Amount = 0;
        //                }

        //                List<Employees> _empRow = (from employee in _OrgdbContext.Employees
        //                                           where employee.EmployeeID == item.employeeID
        //                                           select employee
        //                                  ).ToList();
        //                List<EmployeeDeductions> _empDeduction = (from deduction in _OrgdbContext.EmployeeDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
        //                                                          where deduction.EmployeeId == item.employeeID
        //                                                          select deduction
        //                                                    ).ToList();
        //                if (_empRow.Count() != 1)
        //                    continue;

        //                List<Checks> _checkRow = (from check in _OrgdbContext.Checks.Where(r => r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false)
        //                                          where check.EmployeeId == item.employeeID
        //                                          select check
        //                                          ).ToList();



        //                var _timePunchesVal = _OrgdbContext.TimePunches.Where(r => r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false);


        //                foreach (var res in _timePunchesVal)
        //                {
        //                    if (Convert.ToInt32(res.EmployeeId) == item.employeeID)
        //                        res.CheckId = checkId;
        //                }
        //                decimal taxMedicare = 0M;
        //                decimal taxSocialSecurity = 0M;
        //                decimal taxFed = 0M;
        //                decimal taxState = 0M;
        //                decimal employeeDeductionsTotal = 0M;

        //                //sa new code
        //                decimal _empDeductionAmt = 0M;
        //                //ea new code

        //                //Calculate Medicare Witholding
        //                taxMedicare = (decimal)item.gross * .0145M;
        //                taxMedicare = Math.Round(taxMedicare, 2);



        //                //Calculate Social Security Witholding
        //                taxSocialSecurity = (decimal)item.gross * .062M;
        //                taxSocialSecurity = Math.Round(taxSocialSecurity, 2);


        //                //Calculate Federal Witholding
        //                taxFed = _fedTaxCal.TaxPercentage(item.gross, Convert.ToInt16(_empRow[0].MaritalStatus), Convert.ToInt16(_empRow[0].Allowances));



        //                //Calculate Deductions from employeeDeductionsTable
        //                employeeDeductionsTotal = 0;



        //                foreach (var employeeDeduction in _empDeduction)
        //                {
        //                    Int32 deductionTypeID = Convert.ToInt32(employeeDeduction.DeductionTypeId);
        //                    Int32 employeeDeductionID = Convert.ToInt32(employeeDeduction.DeductionTypeId);
        //                    decimal amount = Convert.ToDecimal(employeeDeduction.Amount);

        //                    //sa new code for Employee deduction
        //                    _empDeductionAmt += amount;
        //                    //sa new code for Employee deduction

        //                }

        //                //#region  Calculate State Witholding

        //                decimal tempGross = gross - (taxMedicare + taxSocialSecurity);

        //                taxState = _stateTaxCal.TaxPercentageMethod(tempGross, Convert.ToInt16(_empRow[0].Exemptions), Convert.ToBoolean(_empRow[0].HeadOfHousehold), Convert.ToChar(_empRow[0].Blind), Convert.ToBoolean(_empRow[0].Student));

        //                if (_checkRow.Count() == 1)
        //                {
        //                    _checkRow[0].Gross = gross;
        //                    _checkRow[0].Amount = (gross - taxMedicare - taxSocialSecurity - taxFed - taxState - employeeDeductionsTotal);
        //                    _checkRow[0].FirstName = _empRow[0].FirstName;
        //                    _checkRow[0].LastName = _empRow[0].LastName;

        //                    _checkRow[0].StateTaxAmount = taxState;
        //                    _checkRow[0].EmployeeDeductionAmount = _empDeductionAmt;
        //                    _checkRow[0].FederalTaxAmount = taxFed;
        //                    _checkRow[0].SocialSecurityTaxAmount = taxSocialSecurity;
        //                    _checkRow[0].MedicareTaxAmount = taxMedicare;

        //                   // checkId = Convert.ToInt32(_checkRow[0].CheckId);
        //                    _OrgdbContext.Checks.Update(_checkRow[0]);
        //                }
        //                else if (_checkRow.Count() == 0)
        //                {
        //                    Checks _newCheckRow = new Checks();
        //                    _newCheckRow.EmployeeId = item.employeeID;
        //                    _newCheckRow.PayPeriodId = Payroll.payperiodId;
        //                    _newCheckRow.Gross = gross;
        //                    _newCheckRow.Amount = (gross - taxMedicare - taxSocialSecurity - taxFed - taxState - employeeDeductionsTotal);
        //                    _newCheckRow.CheckNumber = 0;
        //                    _newCheckRow.CheckDate = Convert.ToDateTime(Payroll.checkDate.Split('/')[1] + "/" + Payroll.checkDate.Split('/')[0] + "/" + Payroll.checkDate.Split('/')[2]);
        //                    _newCheckRow.FirstName = _empRow[0].FirstName;
        //                    _newCheckRow.LastName = _empRow[0].LastName;
        //                    _newCheckRow.Notes = "";
        //                    _newCheckRow.CreatedDate = DateTime.UtcNow;
        //                    _newCheckRow.CreatedBy = Payroll.createdBy;
        //                    _newCheckRow.IsActive = true;
        //                    _newCheckRow.IsDeleted = false;

        //                    _newCheckRow.StateTaxAmount = taxState;
        //                    _newCheckRow.EmployeeDeductionAmount = _empDeductionAmt;
        //                    _newCheckRow.FederalTaxAmount = taxFed;
        //                    _newCheckRow.SocialSecurityTaxAmount = taxSocialSecurity;
        //                    _newCheckRow.MedicareTaxAmount = taxMedicare;


        //                    _OrgdbContext.Checks.Add(_newCheckRow);
        //                    //checkId = Convert.ToInt32(_newCheckRow.CheckId);
        //                    _checkRow = (from check in _OrgdbContext.Checks
        //                                 where check.EmployeeId == item.employeeID
        //                                 select check
        //                                          ).ToList();
        //                }
        //                else
        //                {
        //                    //will throw error
        //                }



        //                //sa New Code

        //                //ea new Code



        //            }

        //            if(_processStatus == 1) 
        //            {
        //                _message = StatusMessage.ProcessedSuccessMsg;
        //                _statusCode = Convert.ToInt32(HttpStatusCodes.OK);
        //            }
        //            else
        //            {
        //                _message = StatusMessage.NoProcessRecord;
        //                _statusCode = Convert.ToInt32(HttpStatusCodes.NotFound);
        //            }

        //            using (TransactionScope transaction = new TransactionScope())
        //            {
        //                _OrgdbContext.SaveChanges();
        //                transaction.Complete();
        //            }


        //        }
        //        return new JsonModel()
        //        {
        //            data = new object(),
        //            Message = _message,
        //            StatusCode = _statusCode
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new JsonModel()
        //        {
        //            data = new object(),
        //            Message = ex.Message,
        //            StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
        //        };
        //    }
        //}
        #endregion Calculate Amount and add in check table


        #region Old Code for Calculate Tax Amount
        //public JsonModel CalculateAmount(Processpayrollviewmodel Payroll)
        //{
        //    try
        //    {
        //        if (Payroll.payperiodId <= 0)
        //        {
        //            return new JsonModel()
        //            {
        //                data = new object(),
        //                Message = StatusMessage.ProcessedErrorMsg,
        //                StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
        //            };
        //        }
        //        else
        //        {
        //            //var result = _OrgdbContext.ExecStoredProcedureWithOutOutput<TotalAmount>("sp_FinalAmount", Payroll.payperiodId,  Payroll.payFrequency, Payroll.startDate, Payroll.endDate);

        //            //sa Original code
        //            var query = (from timePuches in _OrgdbContext.TimePunches
        //                         group timePuches by timePuches.EmployeeId into grp
        //                         select new
        //                         {
        //                             employeeID = grp.Key,
        //                             gross = grp.Sum(x => x.Amount),
        //                         });
        //            //ea Original code



        //            foreach (var check in _OrgdbContext.Checks)
        //            {
        //                check.Gross = 0;
        //                check.Amount = 0;
        //            }

        //            foreach (var checkDeduct in _OrgdbContext.CheckDeductions)
        //            {
        //                checkDeduct.Amount = 0;
        //            }

        //            var fedTax = _fedTaxCal.FedTaxCalculate(Payroll.createDate, Payroll.payFrequency);
        //            var stateTax = _stateTaxCal.StateTaxCalulate(Payroll.createDate, Payroll.payFrequency);

        //            foreach (var item in query)
        //            {
        //                int checkId = 0;
        //                decimal gross = item.gross ?? default(decimal);
        //                decimal totalTax = 0, totalDeductions = 0;

        //                var EmpTimePunchChnages = _OrgdbContext.EmployeeTimePunchChange.Where(r => r.EmployeeId == item.employeeID).SingleOrDefault();
        //                if (EmpTimePunchChnages != null)
        //                {
        //                    EmpTimePunchChnages.IsActive = false;
        //                    EmpTimePunchChnages.IsDeleted = true;
        //                    _OrgdbContext.EmployeeTimePunchChange.Update(EmpTimePunchChnages);
        //                }

        //                foreach (var check in _OrgdbContext.Checks.Where(r => r.EmployeeId == item.employeeID && r.PayPeriodId == Payroll.payperiodId && r.IsActive == true && r.IsDeleted == false))
        //                {
        //                    check.Gross = 0;
        //                    check.Amount = 0;
        //                }

        //                List<Employees> _empRow = (from employee in _OrgdbContext.Employees
        //                                           where employee.EmployeeID == item.employeeID
        //                                           select employee
        //                                  ).ToList();
        //                List<EmployeeDeductions> _empDeduction = (from deduction in _OrgdbContext.EmployeeDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
        //                                                          where deduction.EmployeeId == item.employeeID
        //                                                          select deduction
        //                                                    ).ToList();
        //                if (_empRow.Count() != 1)
        //                    continue;
        //                List<Checks> _checkRow = (from check in _OrgdbContext.Checks
        //                                          where check.EmployeeId == item.employeeID
        //                                          select check
        //                                          ).ToList();


        //                if (_checkRow.Count() == 1)
        //                {
        //                    _checkRow[0].Gross = gross;
        //                    _checkRow[0].Amount = 0;
        //                    _checkRow[0].FirstName = _empRow[0].FirstName;
        //                    _checkRow[0].LastName = _empRow[0].LastName;

        //                    checkId = Convert.ToInt32(_checkRow[0].CheckId);
        //                }
        //                else if (_checkRow.Count() == 0)
        //                {
        //                    Checks _newCheckRow = new Checks();
        //                    _newCheckRow.EmployeeId = item.employeeID;
        //                    _newCheckRow.PayPeriodId = Payroll.payperiodId;
        //                    _newCheckRow.Gross = gross;
        //                    _newCheckRow.Amount = 0;
        //                    _newCheckRow.CheckNumber = 0;
        //                    _newCheckRow.CheckDate = Convert.ToDateTime(Payroll.checkDate.Split('/')[1] + "/" + Payroll.checkDate.Split('/')[0] + "/" + Payroll.checkDate.Split('/')[2]);
        //                    _newCheckRow.FirstName = _empRow[0].FirstName;
        //                    _newCheckRow.LastName = _empRow[0].LastName;
        //                    _newCheckRow.Notes = "";
        //                    _newCheckRow.CreatedDate = DateTime.UtcNow;
        //                    _newCheckRow.CreatedBy = Payroll.createdBy;
        //                    _newCheckRow.IsActive = true;
        //                    _newCheckRow.IsDeleted = false;
        //                    _OrgdbContext.Checks.Add(_newCheckRow);
        //                    checkId = Convert.ToInt32(_newCheckRow.CheckId);
        //                    _checkRow = (from check in _OrgdbContext.Checks
        //                                 where check.EmployeeId == item.employeeID
        //                                 select check
        //                                          ).ToList();
        //                }
        //                else
        //                {
        //                    //will throw error
        //                }

        //                if (checkId != 0)
        //                {
        //                    foreach (var res in _OrgdbContext.TimePunches)
        //                    {
        //                        if (Convert.ToInt32(res.EmployeeId) == item.employeeID)
        //                            res.CheckId = checkId;
        //                    }

        //                    decimal taxMedicare = 0M;
        //                    decimal taxSocialSecurity = 0M;
        //                    decimal taxFed = 0M;
        //                    decimal taxState = 0M;
        //                    decimal employeeDeductionsTotal = 0M;

        //                    //sa new code
        //                    decimal _empDeductionAmt = 0M;
        //                    //ea new code

        //                    //Calculate Medicare Witholding
        //                    taxMedicare = (decimal)item.gross * .0145M;
        //                    taxMedicare = Math.Round(taxMedicare, 2);

        //                    List<CheckDeductions> medicareRow = _OrgdbContext.CheckDeductions.Where(x => x.CheckId == checkId && x.DeductionTypeId == 3 && x.IsActive == true && x.IsDeleted == false).ToList();
        //                    if (medicareRow.Count() == 1)
        //                    {

        //                        medicareRow[0].Amount = taxMedicare;
        //                        medicareRow[0].Notes = "";

        //                    }
        //                    else if (medicareRow.Count() == 0)
        //                    {

        //                        CheckDeductions _checkDeductionNewRow = new CheckDeductions();
        //                        _checkDeductionNewRow.CheckId = checkId;
        //                        _checkDeductionNewRow.DeductionTypeId = 3;
        //                        _checkDeductionNewRow.EmployeeDeductionId = 0;
        //                        _checkDeductionNewRow.Amount = taxMedicare;
        //                        _checkDeductionNewRow.Notes = "";
        //                        _checkDeductionNewRow.CreatedBy = Payroll.createdBy;
        //                        _checkDeductionNewRow.CreatedDate = DateTime.UtcNow;
        //                        _checkDeductionNewRow.IsActive = true;
        //                        _checkDeductionNewRow.IsDeleted = false;
        //                        _OrgdbContext.CheckDeductions.Add(_checkDeductionNewRow);

        //                    }
        //                    else
        //                    {
        //                        //throw error;
        //                    }

        //                    //Calculate Social Security Witholding
        //                    taxSocialSecurity = (decimal)item.gross * .062M;
        //                    taxSocialSecurity = Math.Round(taxSocialSecurity, 2);

        //                    List<CheckDeductions> socialSecurityTax = (from checkDeduct in _OrgdbContext.CheckDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
        //                                                               where checkDeduct.CheckId == checkId && checkDeduct.DeductionTypeId == 2
        //                                                               select checkDeduct
        //                                                   ).ToList();

        //                    if (socialSecurityTax.Count() == 1)
        //                    {
        //                        socialSecurityTax[0].Amount = taxSocialSecurity;
        //                        socialSecurityTax[0].Notes = "";
        //                    }
        //                    else if (socialSecurityTax.Count() == 0)
        //                    {

        //                        CheckDeductions newDeductioRow = new CheckDeductions();
        //                        newDeductioRow.CheckId = checkId;
        //                        newDeductioRow.DeductionTypeId = 2;
        //                        newDeductioRow.EmployeeDeductionId = 0;
        //                        newDeductioRow.Amount = taxSocialSecurity;
        //                        newDeductioRow.Notes = "";
        //                        newDeductioRow.CreatedBy = Payroll.createdBy;
        //                        newDeductioRow.CreatedDate = DateTime.UtcNow;
        //                        newDeductioRow.IsActive = true;
        //                        newDeductioRow.IsDeleted = false;
        //                        _OrgdbContext.CheckDeductions.Add(newDeductioRow);
        //                    }
        //                    else
        //                    {
        //                        // not possible  SHOULD THROW ERROR HERE!!!
        //                    }
        //                    //Calculate Federal Witholding
        //                    taxFed = _fedTaxCal.TaxPercentage(item.gross, Convert.ToInt16(_empRow[0].MaritalStatus), Convert.ToInt16(_empRow[0].Allowances));

        //                    var fedtaxrow = (from fedtax in _OrgdbContext.CheckDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
        //                                     where fedtax.CheckId == checkId && fedtax.DeductionTypeId == 1
        //                                     select fedtax
        //                                    ).ToList();
        //                    if (fedtaxrow.Count() == 1)
        //                    {

        //                        fedtaxrow[0].Amount = taxFed;
        //                        fedtaxrow[0].Notes = "";

        //                    }
        //                    else if (fedtaxrow.Count() == 0)
        //                    {
        //                        CheckDeductions fedtaxx = new CheckDeductions();
        //                        fedtaxx.CheckId = checkId;
        //                        fedtaxx.DeductionTypeId = 1;
        //                        fedtaxx.EmployeeDeductionId = 0;
        //                        fedtaxx.Amount = taxFed;
        //                        fedtaxx.Notes = "";
        //                        fedtaxx.CreatedBy = Payroll.createdBy;
        //                        fedtaxx.CreatedDate = DateTime.UtcNow;
        //                        fedtaxx.IsActive = true;
        //                        fedtaxx.IsDeleted = false;
        //                        _OrgdbContext.CheckDeductions.Add(fedtaxx);
        //                    }
        //                    else
        //                    {
        //                        // throw error here!!!
        //                    }

        //                    //Calculate Deductions from employeeDeductionsTable
        //                    employeeDeductionsTotal = 0;



        //                    foreach (var employeeDeduction in _empDeduction)
        //                    {
        //                        Int32 deductionTypeID = Convert.ToInt32(employeeDeduction.DeductionTypeId);
        //                        Int32 employeeDeductionID = Convert.ToInt32(employeeDeduction.DeductionTypeId);
        //                        decimal amount = Convert.ToDecimal(employeeDeduction.Amount);



        //                        List<CheckDeductions> additionalDeduct = (from cDeduct in _OrgdbContext.CheckDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
        //                                                                  where cDeduct.CheckId == checkId && cDeduct.DeductionTypeId == deductionTypeID && cDeduct.EmployeeDeductionId == employeeDeductionID
        //                                                                  select cDeduct).ToList();
        //                        if (additionalDeduct.Count() == 1)
        //                        {

        //                            additionalDeduct[0].Amount = amount;
        //                            additionalDeduct[0].Notes = "";
        //                            employeeDeductionsTotal = amount;

        //                        }
        //                        else if (additionalDeduct.Count() == 0)
        //                        {
        //                            CheckDeductions cdeduct = new CheckDeductions();
        //                            cdeduct.CheckId = checkId;
        //                            cdeduct.DeductionTypeId = deductionTypeID;
        //                            cdeduct.EmployeeDeductionId = employeeDeductionID;
        //                            cdeduct.Amount = amount;
        //                            cdeduct.Notes = "";
        //                            cdeduct.CreatedBy = Payroll.createdBy;
        //                            cdeduct.CreatedDate = DateTime.UtcNow;
        //                            cdeduct.IsActive = true;
        //                            cdeduct.IsDeleted = false;
        //                            _OrgdbContext.CheckDeductions.Add(cdeduct);
        //                            employeeDeductionsTotal = amount;

        //                        }
        //                        else
        //                        {
        //                            //  THROW ERROR 
        //                        }


        //                    }

        //                    //#region  Calculate State Witholding

        //                    decimal tempGross = gross - (taxMedicare + taxSocialSecurity);

        //                    taxState = _stateTaxCal.TaxPercentageMethod(tempGross, Convert.ToInt16(_empRow[0].Exemptions), Convert.ToBoolean(_empRow[0].HeadOfHousehold), Convert.ToChar(_empRow[0].Blind), Convert.ToBoolean(_empRow[0].Student));

        //                    List<CheckDeductions> stateTaxRow = (from deduct in _OrgdbContext.CheckDeductions.Where(r => r.IsActive == true && r.IsDeleted == false)
        //                                                         where deduct.CheckId == checkId && deduct.DeductionTypeId == 4
        //                                                         select deduct
        //                                             ).ToList();

        //                    if (stateTaxRow.Count() == 1)
        //                    {

        //                        stateTaxRow[0].Amount = taxState;
        //                        stateTaxRow[0].Notes = "MA";

        //                    }
        //                    else if (stateTaxRow.Count() == 0)
        //                    {
        //                        CheckDeductions obj = new CheckDeductions();
        //                        obj.CheckId = checkId;
        //                        obj.DeductionTypeId = 4;
        //                        obj.EmployeeDeductionId = 0;
        //                        obj.Amount = taxState;
        //                        obj.Notes = "MA";
        //                        obj.CreatedBy = Payroll.createdBy;
        //                        obj.CreatedDate = DateTime.UtcNow;
        //                        obj.IsActive = true;
        //                        obj.IsDeleted = false;
        //                        _OrgdbContext.CheckDeductions.Add(obj);

        //                    }
        //                    else
        //                    {

        //                    }


        //                    _checkRow[0].Amount = (gross - taxMedicare - taxSocialSecurity - taxFed - taxState - employeeDeductionsTotal);



        //                    _OrgdbContext.Checks.Update(_checkRow[0]);

        //                    //EmployeeTimePunchChange employeeTimePunchChange = new EmployeeTimePunchChange();






        //                } 
        //            }
        //            using (TransactionScope transaction = new TransactionScope())
        //            {
        //                _OrgdbContext.SaveChanges();
        //                transaction.Complete();
        //            }


        //        }
        //        return new JsonModel()
        //        {
        //            data = new object(),
        //            Message = StatusMessage.ProcessedSuccessMsg,
        //            StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new JsonModel()
        //        {
        //            data = new object(),
        //            Message = ex.Message,
        //            StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
        //        };
        //    }
        //}
        #endregion Old Code for Calculate Tax Amount


        public JsonModel GetIsChangesOnTimePuches()
        {


            SqlParameter[] parameters =
                {


                };
            var result = _OrgdbContext.ExecStoredProcedureWithOutput<ViewPayPeriods>("GetIsChangesOnTimePuches", parameters.Length, parameters).AsQueryable().FirstOrDefault();

            return new JsonModel()
            {
                data = result,
                Message = StatusMessage.ProcessedSuccessMsg,
                StatusCode = Convert.ToInt32(HttpStatusCodes.OK),
            };


        }



        public Task<string> PrintEmployeeChecksAsync(string FromEmployeeNO, string ToEmployeeNO,int PayPeriodId, int EmployeeId, int ApplicationId)
        {
            int _applicationID = Convert.ToInt32(CommonEnumField.ApplicationID);
            SqlParameter[] parameters =
             {
                    new SqlParameter("@PayPeriodId", PayPeriodId),
                    new SqlParameter("@EmployeeID", EmployeeId),
                    new SqlParameter("@FromEmployeeNO", FromEmployeeNO),
                    new SqlParameter("@ToEmployeeNO", ToEmployeeNO),
                    new SqlParameter("@ApplicationId", ApplicationId)

            };
            //var result = _OrgdbContext.ExecStoredProcedureWithOutput<PrintChecksDetails>("PrintCheckDetails", parameters.Length, parameters).AsQueryable().ToList().Take(2).ToList();
            var result = _OrgdbContext.ExecStoredProcedureWithOutput<ViewPrintChecksDetails>("PrintCheckDetails", parameters.Length, parameters).AsQueryable().ToList();



            foreach (var list in result)
            {
                SqlParameter[] parameters1 =
                 {
                   // new SqlParameter("@PageNumber", 1),
                   // new SqlParameter("@RowspPage", 100),
                    new SqlParameter("@EmployeeId", list.EmployeeId),
                    new SqlParameter("@PayPeriodId", PayPeriodId),
                    //new SqlParameter("@SearchText", "")
                };

                list.TimePunchList = _OrgdbContext.ExecStoredProcedureWithOutput<ViewTimePunchesHistoryModelOnChecks>("ViewTimePhunchesOnPrintChecks", parameters1.Length, parameters1).AsQueryable().ToList();

            }

            var BindHtmlWithModel = RenderRazorViewToStringAsync(result, "Views/Template/pdf.cshtml");

            return BindHtmlWithModel;

            //return new JsonModel()
            //{
            //    data = BindHtmlWithModel.Result,
            //    Message = StatusMessage.ProcessedSuccessMsg,
            //    StatusCode = Convert.ToInt32(HttpStatusCodes.OK),
            //};
        }

        //public async Task<JsonModel> PrintEmployeeChecksAsync(int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0)
        //{
        //    int _applicationID = Convert.ToInt32(CommonEnumField.ApplicationID);
        //    SqlParameter[] parameters =
        //     {

        //            new SqlParameter("@PayPeriodId", PayPeriodId),
        //            new SqlParameter("@EmployeeID", EmployeeId),
        //            new SqlParameter("@ApplicationId", ApplicationId)

        //    };
        //    var result= _OrgdbContext.ExecStoredProcedureWithOutOutput<PrintChecksDetails>("PrintCheckDetails", parameters.Length, parameters).AsQueryable().FirstOrDefault();

        //    var t = await RenderRazorViewToStringAsync(result, "Pages/PdfTemplates/pdf.cshtml");

        //    return null;
        //}

        public async Task<string> RenderRazorViewToStringAsync(object model, string viewName, bool isLandscape = false)
        {
            try
            {
                // use for http routing
                var httpContext = new DefaultHttpContext { RequestServices = _serviceProvider };
                var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());

                var filename = Guid.NewGuid().ToString() + ".pdf";

                var saveGenratedPdfPath = _HostingEnvironment.WebRootPath + System.IO.Path.DirectorySeparatorChar.ToString() + "SavePdffiles";

                if(Directory.Exists(saveGenratedPdfPath))
                {
                    saveGenratedPdfPath= saveGenratedPdfPath+ System.IO.Path.DirectorySeparatorChar.ToString() + filename;
                }
                else
                {
                    DirectoryInfo di = Directory.CreateDirectory(saveGenratedPdfPath);
                    saveGenratedPdfPath = saveGenratedPdfPath + System.IO.Path.DirectorySeparatorChar.ToString() + filename;

                }


                // used to convert view to string

                using (var stringWriter = new StringWriter())
                {
                    // get view
                    var viewResult = _razorViewEngine.GetView("", "~/" + viewName, false);

                    //var viewResult = _razorViewEngine.FindView(actionContext, viewName, false);

                    if (!viewResult.Success)
                    {
                        throw new ArgumentNullException($"{viewName} does not match any available view");
                    }

                    // set the model value for view
                    var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                    {
                        Model = model
                    };

                    // initialize view
                    var viewContext = new ViewContext(
                        actionContext,
                        viewResult.View,
                        viewDictionary,
                        new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                        stringWriter,
                        new HtmlHelperOptions()
                    );

                    // render view 
                    await viewResult.View.RenderAsync(viewContext);

                    var _stream = new MemoryStream();

                    //// convert string to pdf in stream & then destroy the stream 
                    using (var pdfWriter = new PdfWriter(_stream))
                    {
                        pdfWriter.SetCloseStream(false);
                        ConverterProperties cp = new ConverterProperties();
                        PdfDocument doc = new PdfDocument(pdfWriter);
                        if (isLandscape)
                        {
                            doc.SetDefaultPageSize(iText.Kernel.Geom.PageSize.A4.Rotate());
                        }
                        // CAUTION : Don't remove using block from here, it used to destroy the stream once pdf is generated
                        using (var document = HtmlConverter.ConvertToDocument(stringWriter.ToString(), doc, cp)) { }


                    }

                    ProcessPayrollRepository.createPdf(stringWriter.ToString(), saveGenratedPdfPath);

                    _stream.Position = 0;



                    //return _stream.ToArray();
                    return saveGenratedPdfPath;
                }

                //return saveGenratedPdfPath;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public static void createPdf(String html, String dest)
        {
            HtmlConverter.ConvertToPdf(html, new FileStream(dest, FileMode.Create));
        }
        public JsonModel AddCheckIdInTaxDeduction(Processpayrollviewmodel Payroll)
        {
            try
            {
                SqlParameter[] parameters =
                    {

                    new SqlParameter("@payperiodId",  Payroll.payperiodId)


                };
                _OrgdbContext.ExecStoredProcedureWithOutOutput<Employees>("ChnageCheckIDAfterCheckDeduction", parameters.Length, parameters).AsQueryable();

                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.AddEmpSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }


        public JsonModel FromEmployeeList(string EmployeeNo,int EmployeeId = 0)
        {
            int _applicationID = Convert.ToInt32(CommonEnumField.ApplicationID);
            SqlParameter[] parameters =
             {

                    new SqlParameter("@SelectEmployeeId", EmployeeId),
                    new SqlParameter("@SelectEmployeeNumber", EmployeeNo),
                    new SqlParameter("@ApplicationId", _applicationID)

            };
            var result = _OrgdbContext.ExecStoredProcedureWithOutput<EmployeeNumberModel>("GetCurrentPayperiodActiveEmployeeList", parameters.Length, parameters).AsQueryable().ToList();

            return new JsonModel()
            {
                data = result,
                Message = StatusMessage.ProcessedSuccessMsg,
                StatusCode = Convert.ToInt32(HttpStatusCodes.OK),
            };
        }


        public JsonModel ToEmployeeList(string EmployeeNo,int EmployeeId = 0)
        {
            int _applicationID = Convert.ToInt32(CommonEnumField.ApplicationID);
            SqlParameter[] parameters =
             {

                    new SqlParameter("@SelectEmployeeId", EmployeeId),
                    new SqlParameter("@SelectEmployeeNumber",EmployeeNo),
                    new SqlParameter("@ApplicationId", _applicationID)

            };
            var result = _OrgdbContext.ExecStoredProcedureWithOutput<EmployeeNumberModel>("GetCurrentPayperiodActiveToEmployeeList", parameters.Length, parameters).AsQueryable().ToList(); 

            return new JsonModel()
            {
                data = result,
                Message = StatusMessage.ProcessedSuccessMsg,
                StatusCode = Convert.ToInt32(HttpStatusCodes.OK),
            };
        }

    }
}

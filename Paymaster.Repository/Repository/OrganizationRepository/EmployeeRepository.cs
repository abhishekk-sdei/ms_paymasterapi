﻿using Microsoft.Data.SqlClient;
using Paymaster.Common;
using Paymaster.Common.Enum;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using static Paymaster.Common.Enum.CommonEnums;

namespace Paymaster.Repository.Repository.OrganizationRepository
{
    public  class EmployeeRepository : IEmployeeRepository
    {
        private readonly PaymasterOrgDbContext _OrgdbContext;
        public EmployeeRepository(PaymasterOrgDbContext OrgdbContext)
        {
            _OrgdbContext = OrgdbContext;
        }

        public JsonModel SaveEmployeeBasicDetails(EmployeesDetails employees)
        {
            try
            {
                int _applicationID = Convert.ToInt32(CommonEnums.CommonEnumField.ApplicationID);
                SqlParameter[] parameters =
                    {
                    //new SqlParameter("@Number", employees.Number),
                    new SqlParameter("@FirstName",   employees.FirstName),
                    new SqlParameter("@MiddleName",  employees.MiddleName),
                    new SqlParameter("@LastName",  employees.LastName),
                    new SqlParameter("@SocSec",   employees.SocSec),
                    new SqlParameter("@MaritalStatus",   employees.MaritalStatus),
                    new SqlParameter("@Hiredate",  employees.Hiredate),
                    new SqlParameter("@BirthDate",  employees.BirthDate),
                    new SqlParameter("@Race",   employees.Race),
                    new SqlParameter("@Sex",   employees.Sex),
                    new SqlParameter("@CreatedBy",   employees.CreatedBy),
                    new SqlParameter("@CreatedDate",  DateTime.Now),
                    new SqlParameter("@_applicationId",   _applicationID),
                    new SqlParameter("@UserId",  employees.UserId)


                };
                //_OrgdbContext.ExecStoredProcedureWithOutOutput<Employees>("sp_SaveEmployeeDetail", parameters.Length, parameters).AsQueryable();
                var EmployeeID = _OrgdbContext.ExecStoredProcedureWithOutput<GetEmployeeIdModel>("SaveEmployeeBasicDetail", parameters.Length, parameters).AsQueryable().FirstOrDefault().EmployeeID;
                // _OrgdbContext.ExecStoredProcedureWithOutOutput<Employees>("sp_CreateTimePunchOnSave", parameters.Length, parameters).AsQueryable();


                return new JsonModel()
                {
                    data = EmployeeID,
                    Message = ConstantString.StatusMessage.AddEmpBasicDetailsSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        public JsonModel SaveEmployeeSalaryDetails(EmployeeSalaryDetailsModel employees)
        {
            try
            {
                var entity = _OrgdbContext.Employees.FirstOrDefault(item => item.EmployeeID == employees.EmployeeID && item.IsActive == true && item.IsDeleted == false);
                if (entity != null)
                {
                    
                    entity.PayFrequency = employees.PayFrequency;
                    entity.HourlyPayRate = employees.HourlyPayRate;
                    entity.Hours = employees.Hours;
                    //entity.DailyRate = employees.DailyRate;
                    entity.EarnedIncomeCredit = employees.EarnedIncomeCredit;
                    entity.EarnedIncomeCreditChildren = employees.EarnedIncomeCreditChildren;
                    entity.Allowances = employees.Allowances;
                    entity.Dependents = employees.Dependents;
                    entity.Exemptions = employees.Exemptions;
                    entity.FlatFit = employees.FlatFit;
                    entity.AdditionalWitholdingAmount = employees.AdditionalWitholdingAmount;
                    entity.PartTime = employees.PartTime;
                    _OrgdbContext.Employees.Update(entity);

                    // Save changes in database
                    _OrgdbContext.SaveChanges();

                    

                    SqlParameter[] parameters =
                   {
                    //new SqlParameter("@Number", employees.Number),
                    new SqlParameter("@EmployeeId",   employees.EmployeeID),
                    new SqlParameter("@_PayFrequencyId",  employees.PayFrequency),
                    //new SqlParameter("@CreatedDate",  DateTime.UtcNow),
                    new SqlParameter("@HourlyRate",   employees.HourlyPayRate),
                    new SqlParameter("@Hours",   employees.Hours),
                    new SqlParameter("@ReferenceDate",  entity.Hiredate),
                    new SqlParameter("@CreatedBy",  entity.CreatedBy)

                };
                    //_OrgdbContext.ExecStoredProcedureWithOutOutput<EmployeeSalaryDetailsModel>("CreateTimePunchOnSave", parameters.Length, parameters).AsQueryable().FirstOrDefault().EmployeeID;
                    _OrgdbContext.ExecStoredProcedureWithOutOutput<EmployeeSalaryDetailsModel>("CreateTimePunchOnSave", parameters.Length, parameters).AsQueryable();
                }

                return new JsonModel()
                {
                    data = new object(),
                    Message = ConstantString.StatusMessage.AddEmpSalaryDetailsSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        public JsonModel SaveEmployeePersonalDetails(EmployeePersonalDetailsModel employees)
        {
            try
            {
                var entity = _OrgdbContext.Employees.FirstOrDefault(item => item.EmployeeID == employees.EmployeeID && item.IsActive == true && item.IsDeleted == false);
                if (entity != null)
                {

                    
                    entity.CityId = employees.CityId;
                    entity.StateId = employees.StateId;
                    entity.CountryId = employees.CountryId;
                    entity.HeadOfHousehold = employees.HeadOfHousehold;
                    entity.Blind = employees.Blind;
                    entity.Student = employees.Student;                    
                    //entity.UsCitizen = employees.UsCitizen;

                    _OrgdbContext.Employees.Update(entity);

                    // Save changes in database
                    _OrgdbContext.SaveChanges();
                }

                return new JsonModel()
                {
                    data = new object(),
                    Message = ConstantString.StatusMessage.AddEmpPersonalDetailsSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        public JsonModel SaveEmployeePaymentDetails(EmployeesDetails employees)
        {
            try
            {
                var entity = _OrgdbContext.Employees.FirstOrDefault(item => item.EmployeeID == employees.EmployeeID && item.IsActive == true && item.IsDeleted == false);
                if (entity != null)
                {

                    _OrgdbContext.Employees.Update(entity);

                    // Save changes in database
                    _OrgdbContext.SaveChanges();
                }

                return new JsonModel()
                {
                    data = new object(),
                    Message = ConstantString.StatusMessage.AddEmpPaymentDetailsSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        public JsonModel SaveEmployee(EmployeesDetails employees) 
        {
            try 
            {
                int _applicationID = Convert.ToInt32(CommonEnums.CommonEnumField.ApplicationID);              
                SqlParameter[] parameters =
                    {
                    //new SqlParameter("@Number", employees.Number),
                    new SqlParameter("@FirstName",   employees.FirstName),
                    new SqlParameter("@MiddleName",  employees.MiddleName),
                    new SqlParameter("@LastName",  employees.LastName),
                    new SqlParameter("@SocSec",   employees.SocSec),
                    new SqlParameter("@PayFrequency",   employees.PayFrequency),

                    new SqlParameter("@HourlyPayRate", employees.HourlyPayRate),
                    new SqlParameter("@DailyRate",   employees.DailyRate),
                    //new SqlParameter("@Hiredate",  employees.Hiredate),
                    new SqlParameter("@Hiredate",  DateTime.UtcNow),
                    //new SqlParameter("@EarnedIncomeCredit",  employees.EarnedIncomeCredit),
                    new SqlParameter("@EarnedIncomeCredit",  1),
                    //new SqlParameter("@EarnedIncomeCreditChildren",   employees.EarnedIncomeCreditChildren),
                    new SqlParameter("@EarnedIncomeCreditChildren",   1),
                    new SqlParameter("@MaritalStatus",   employees.MaritalStatus),

                    new SqlParameter("@Allowances",1),
                    new SqlParameter("@Dependents",  1),
                    new SqlParameter("@Exemptions", 1),
                    new SqlParameter("@BirthDate",  employees.BirthDate),
                    new SqlParameter("@Race",   employees.Race),
                    new SqlParameter("@UsCitizen",   1),

                    new SqlParameter("@FilingStatus", 1),
                    new SqlParameter("@Sex",   employees.Sex),
                    new SqlParameter("@CityId",  employees.CityId),
                    new SqlParameter("@StateId",  employees.StateId),
                    new SqlParameter("@CountryId",   employees.CountryId),
                    new SqlParameter("@CreatedBy",   employees.CreatedBy),

                    new SqlParameter("@CreatedDate",  DateTime.Now),
                    new SqlParameter("@_applicationId",   _applicationID),
                    new SqlParameter("@Hours",  employees.Hours),
                    new SqlParameter("@UserId",  employees.UserId)


                };
                //_OrgdbContext.ExecStoredProcedureWithOutOutput<Employees>("sp_SaveEmployeeDetail", parameters.Length, parameters).AsQueryable();
                _OrgdbContext.ExecStoredProcedureWithOutOutput<Employees>("SaveEmployeeDetail", parameters.Length, parameters).AsQueryable();
                // _OrgdbContext.ExecStoredProcedureWithOutOutput<Employees>("sp_CreateTimePunchOnSave", parameters.Length, parameters).AsQueryable();


                return new JsonModel()
                {
                    data = new object(),
                    Message = ConstantString.StatusMessage.AddEmpSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch(Exception ex) 
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        public JsonModel UpdateEmployee(Employees employees)
        {
            try
            {
                var entity = _OrgdbContext.Employees.FirstOrDefault(item => item.EmployeeID == employees.EmployeeID && item.IsActive == true && item.IsDeleted == false);
                if (entity != null)
                {
                    // Answer for question #2

                    // Make changes on entity
                    entity.FirstName = employees.FirstName;
                    entity.MiddleName = employees.MiddleName;
                    entity.LastName = employees.LastName;
                    entity.SocSec = employees.SocSec;
                    entity.PayFrequency = employees.PayFrequency;
                    entity.HourlyPayRate = employees.HourlyPayRate;

                    entity.DailyRate = employees.DailyRate;
                    entity.Hiredate = employees.Hiredate;
                    entity.EarnedIncomeCredit = employees.EarnedIncomeCredit;
                    entity.EarnedIncomeCreditChildren = employees.EarnedIncomeCreditChildren;

                    entity.MaritalStatus = employees.MaritalStatus;
                    entity.Allowances = employees.Allowances;
                    entity.Dependents = employees.Dependents;
                    entity.Exemptions = employees.Exemptions;

                    entity.BirthDate = employees.BirthDate;
                    entity.Race = employees.Race;
                    entity.UsCitizen = employees.UsCitizen;
                    entity.FilingStatus = 1;
                    entity.Sex = employees.Sex;
                    entity.UsCitizen = employees.UsCitizen;
                    entity.Race = employees.Race;

                    entity.CityId = employees.CityId;
                    entity.StateId = employees.StateId;
                    entity.CountryId = employees.CountryId;

                    //entity.CreatedBy = employees.CreatedBy;
                    entity.UpdatedBy = Convert.ToInt32(CommonEnums.CommonEnumField.ApplicationID); ;
                    //entity.CreatedDate = employees.CreatedDate;
                    entity.UpdatedDate = DateTime.Now;
                    entity.IsDeleted = false;
                    entity.IsActive = true;
                    // Update entity in DbSet
                    _OrgdbContext.Employees.Update(entity);

                    // Save changes in database
                    _OrgdbContext.SaveChanges();
                }

                return new JsonModel()
                {
                    data = new object(),
                    Message = ConstantString.StatusMessage.UpdateEmpSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        public JsonModel UpdateEmployeeBasicDetails(Employees employees)
        {
            try
            {
                var entity = _OrgdbContext.Employees.FirstOrDefault(item => item.EmployeeID == employees.EmployeeID && item.IsActive == true && item.IsDeleted == false);
                if (entity != null)
                {
                    // Answer for question #2

                    // Make changes on entity
                    entity.FirstName = employees.FirstName;
                    entity.MiddleName = employees.MiddleName;
                    entity.LastName = employees.LastName;
                    entity.SocSec = employees.SocSec;
                    entity.Hiredate = employees.Hiredate;
                    entity.MaritalStatus = employees.MaritalStatus;
                    entity.BirthDate = employees.BirthDate;
                    entity.Race = employees.Race;
                    entity.Sex = employees.Sex;

                    entity.UpdatedBy = Convert.ToInt32(CommonEnums.CommonEnumField.ApplicationID); ;
                    entity.UpdatedDate = DateTime.Now;
                    entity.IsDeleted = false;
                    entity.IsActive = true;
                    // Update entity in DbSet
                    _OrgdbContext.Employees.Update(entity);

                    // Save changes in database
                    _OrgdbContext.SaveChanges();
                }

                return new JsonModel()
                {
                    data = new object(),
                    Message = ConstantString.StatusMessage.UpdateEmpSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        public JsonModel UpdateEmployeeSalaryDetails(Employees employees)
        {
            try
            {
                var entity = _OrgdbContext.Employees.FirstOrDefault(item => item.EmployeeID == employees.EmployeeID && item.IsActive == true && item.IsDeleted == false);
                if (entity != null)
                {
                    // Answer for question #2

                    // Make changes on entity
                    entity.PayFrequency = employees.PayFrequency;
                    entity.HourlyPayRate = employees.HourlyPayRate;
                    entity.Hours = employees.Hours;
                    //entity.DailyRate = employees.DailyRate;
                    entity.EarnedIncomeCredit = employees.EarnedIncomeCredit;
                    entity.EarnedIncomeCreditChildren = employees.EarnedIncomeCreditChildren;
                    entity.Allowances = employees.Allowances;
                    entity.Dependents = employees.Dependents;
                    entity.Exemptions = employees.Exemptions;
                    entity.FlatFit = employees.FlatFit;
                    entity.AdditionalWitholdingAmount = employees.AdditionalWitholdingAmount;
                    entity.PartTime = employees.PartTime;
                    entity.UpdatedBy = Convert.ToInt32(CommonEnums.CommonEnumField.ApplicationID); ;
                    //entity.CreatedDate = employees.CreatedDate;
                    entity.UpdatedDate = DateTime.Now;
                    entity.IsDeleted = false;
                    entity.IsActive = true;
                    // Update entity in DbSet
                    _OrgdbContext.Employees.Update(entity);

                    // Save changes in database
                    _OrgdbContext.SaveChanges();
                }

                return new JsonModel()
                {
                    data = new object(),
                    Message = ConstantString.StatusMessage.UpdateEmpSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        public JsonModel UpdateEmployeePersonalDetails(Employees employees)
        {
            try
            {
                var entity = _OrgdbContext.Employees.FirstOrDefault(item => item.EmployeeID == employees.EmployeeID && item.IsActive == true && item.IsDeleted == false);
                if (entity != null)
                {
                    // Answer for question #2

                    // Make changes on entity
                    entity.CityId = employees.CityId;
                    entity.StateId = employees.StateId;
                    entity.CountryId = employees.CountryId;
                    entity.HeadOfHousehold = employees.HeadOfHousehold;
                    entity.Blind = employees.Blind;
                    entity.Student = employees.Student;

                    entity.UpdatedBy = Convert.ToInt32(CommonEnums.CommonEnumField.ApplicationID); ;
                    //entity.CreatedDate = employees.CreatedDate;
                    entity.UpdatedDate = DateTime.Now;
                    entity.IsDeleted = false;
                    entity.IsActive = true;
                    // Update entity in DbSet
                    _OrgdbContext.Employees.Update(entity);

                    // Save changes in database
                    _OrgdbContext.SaveChanges();
                }

                return new JsonModel()
                {
                    data = new object(),
                    Message = ConstantString.StatusMessage.UpdateEmpSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

    }
}

﻿using Microsoft.Data.SqlClient;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Paymaster.Common.ConstantString;
using static Paymaster.Common.Enum.CommonEnums;

namespace Paymaster.Repository.Repository.OrganizationRepository
{
    public class ChecksRepository: IChecksRepository
    {
        private readonly PaymasterOrgDbContext _OrgdbContext;
        public ChecksRepository(PaymasterOrgDbContext OrgdbContext)
        {
            _OrgdbContext = OrgdbContext;
        }

        public JsonModel UpdateCheckDetail(Checks checks)
        {
            try
            {
                var entity = _OrgdbContext.Checks.FirstOrDefault(item => item.CheckId == checks.CheckId && item.IsActive == true && item.IsDeleted == false);
                //sa Abhishek         

                if (entity != null)
                {
                    // Answer for question #2

                    // Make changes on entity
                    entity.Gross =checks.Gross;
                    entity.Amount =checks.Amount;
                    entity.UpdatedDate = DateTime.UtcNow;
                    _OrgdbContext.Checks.Update(entity);


                    // Save changes in database
                    _OrgdbContext.SaveChanges();
                }

               


                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.UpdateCheckMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        
        public IQueryable<T> GetCheksList<T>(int page = 0, int pageSize = 10) where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                  {
                    new SqlParameter("@PageNumber", page),
                    new SqlParameter("@RowspPage", pageSize)
                };
                // return _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_GetCheckslist", parameters.Length, parameters).AsQueryable();
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_GetActiveCheckslist", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> GetDropdownBindCheckslist<T>(int Emp_ID, int PayPeriodId = 0) where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@Emp_ID", Emp_ID),
                    new SqlParameter("@PayPeriod_ID", PayPeriodId)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_GetDropdownCheckslist", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> ViewChecksDetails<T>(int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0) where T : class, new()
        {
            try
            {
                ApplicationId = ApplicationId == 0 ? 3 : ApplicationId; // for Debug
                SqlParameter[] parameters =
                {
                    new SqlParameter("@PayPeriodId", PayPeriodId),
                    new SqlParameter("@EmployeeID", EmployeeId),
                    new SqlParameter("@ApplicationId", ApplicationId)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("sp_ViewCheckDetails", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

    }
    
}

﻿using Microsoft.Data.SqlClient;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Paymaster.Common.ConstantString;
using static Paymaster.Common.Enum.CommonEnums;

namespace Paymaster.Repository.Repository.OrganizationRepository
{
    public class TimePunchRepository : ITimePunchRepository
    {
        private readonly PaymasterOrgDbContext _OrgdbContext;
        public TimePunchRepository(PaymasterOrgDbContext OrgdbContext)
        {
            _OrgdbContext = OrgdbContext;
        }

        public IQueryable<T> GetTimePunchList<T>(int EmployeeID) where T : class, new()
        {
            try
            {
                //List<EmployeesDetails> EmployeesList = new List<EmployeesDetails>();
                //  return EmployeesList = _OrgdbContext.Employees.Where(r => r.IsActive == true && r.IsDeleted == false).OrderByDescending(r => r.EmployeeID).ToList();
                SqlParameter[] parameters =
                  {
                    new SqlParameter("@EmployeeID", EmployeeID)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetTimePuncheslist", parameters.Length, parameters).AsQueryable();
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetReadOnlyPayPeriodEmployees", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> GetTimePunchHistoryList<T>(int page = 0, int pageSize = 10, int Emp_ID = 0, int PayPeriodId = 0, string SearchText = "") where T : class, new()
        {
            try
            {
                //List<EmployeesDetails> EmployeesList = new List<EmployeesDetails>();
                //  return EmployeesList = _OrgdbContext.Employees.Where(r => r.IsActive == true && r.IsDeleted == false).OrderByDescending(r => r.EmployeeID).ToList();
                SqlParameter[] parameters =
                  {
                    new SqlParameter("@PageNumber", page),
                    new SqlParameter("@RowspPage", pageSize),
                    new SqlParameter("@EmployeeId", Emp_ID),
                    new SqlParameter("@PayPeriodId", PayPeriodId),
                    new SqlParameter("@SearchText", SearchText)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("ViewFilteredEmployeesTimePunches", parameters.Length, parameters).AsQueryable();
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetReadOnlyPayPeriodEmployees", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public IQueryable<T> GetEmployeecheckListHistory<T>(int page = 0, int pageSize = 10, int Emp_ID = 0, int PayPeriodId = 0, string SearchText = "") where T : class, new()
        {
            try
            {
                //List<EmployeesDetails> EmployeesList = new List<EmployeesDetails>();
                //  return EmployeesList = _OrgdbContext.Employees.Where(r => r.IsActive == true && r.IsDeleted == false).OrderByDescending(r => r.EmployeeID).ToList();
                SqlParameter[] parameters =
                  {
                    new SqlParameter("@PageNumber", page),
                    new SqlParameter("@RowspPage", pageSize),
                    new SqlParameter("@EmployeeId", Emp_ID),
                    new SqlParameter("@PayPeriodId", PayPeriodId),
                    new SqlParameter("@SearchText", SearchText)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetChecksHistoryList", parameters.Length, parameters).AsQueryable();
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetReadOnlyPayPeriodEmployees", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        public IQueryable<T> ViewTimePunchList<T>(int EmployeeID) where T : class, new()
        {
            try
            {
                //List<EmployeesDetails> EmployeesList = new List<EmployeesDetails>();
                //  return EmployeesList = _OrgdbContext.Employees.Where(r => r.IsActive == true && r.IsDeleted == false).OrderByDescending(r => r.EmployeeID).ToList();
                SqlParameter[] parameters =
                  {
                    new SqlParameter("@EmployeeID", EmployeeID)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("ViewTimePuncheslist", parameters.Length, parameters).AsQueryable();
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetReadOnlyPayPeriodEmployees", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public TimePunhesDetailModel TimePunchList(int page, int pageSize)
        {
            try
            {
                 SqlParameter[] parameters =
                  {
                    new SqlParameter("@PageNumber", page),
                    new SqlParameter("@RowspPage", pageSize),
                };
               
                //return _OrgdbContext.ExecStoredProcedureWithTimepunchesDetailOutput("GetReadOnlyPayPeriodEmployees", parameters.Length, parameters);
                return _OrgdbContext.ExecStoredProcedureWithTimepunchesDetailOutput("GetReadOnlyPayPeriodEmployeesActive", parameters.Length, parameters);

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public TimePunhesDetailModel ViewEmployeeTimelist(int page, int pageSize)
        {
            try
            {
                SqlParameter[] parameters =
                 {
                    new SqlParameter("@PageNumber", page),
                    new SqlParameter("@RowspPage", pageSize),
                };

                //return _OrgdbContext.ExecStoredProcedureWithTimepunchesDetailOutput("GetReadOnlyPayPeriodEmployees", parameters.Length, parameters);
                return _OrgdbContext.ExecStoredProcedureWithTimepunchesDetailOutput("ViewReadOnlyPayPeriodEmployeesActive", parameters.Length, parameters);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IQueryable<T> GetEmployeeCheckDeductionList<T>(int CheckId) where T : class, new()
        {
            try
            {
                SqlParameter[] parameters =
                  {
                    new SqlParameter("@CheckId", CheckId)
                };
                return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetEmployeeTaxDeductionList", parameters.Length, parameters).AsQueryable();
                //return _OrgdbContext.ExecStoredProcedureWithOutput<T>("GetReadOnlyPayPeriodEmployees", parameters.Length, parameters).AsQueryable();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public TimePunhesDetailModel ViewEmployeeFilteredTimelist(int page, int pageSize, int EmployeeId = 0, int PayPeriodId = 0)
        {
            try
            {
                SqlParameter[] parameters =
                 {
                    new SqlParameter("@PageNumber", page),
                    new SqlParameter("@RowspPage", pageSize),
                    new SqlParameter("@EmployeeId", EmployeeId),
                    new SqlParameter("@PayPeriodId", PayPeriodId),
                };

                //return _OrgdbContext.ExecStoredProcedureWithTimepunchesDetailOutput("GetReadOnlyPayPeriodEmployees", parameters.Length, parameters);
                return _OrgdbContext.ExecStoredProcedureWithTimepunchesDetailOutput("ViewFilteredEmployees", parameters.Length, parameters);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public JsonModel AddTimePunch(TimePunches timePunches) 
        {
            try
            {
                //sa Abhishek             

                SqlParameter[] parameters =
                   {
                    new SqlParameter("@PayperiodId", timePunches.PayPeriodId),
                    new SqlParameter("@EmployeeId",   timePunches.EmployeeId),
                    new SqlParameter("@InDateTime",  timePunches.InDateTime),
                    new SqlParameter("@OutDateTime",  timePunches.OutDateTime),
                    new SqlParameter("@Type",   timePunches.Type),
                    new SqlParameter("@ReferenceDate",   timePunches.ReferenceDate),
                    new SqlParameter("@Hours", timePunches.Hours),
                    new SqlParameter("@HourlyPayRate",  timePunches.HourlyPayRate),
                    new SqlParameter("@OvertimeMultiplier",  timePunches.OvertimeMultiplier),
                    new SqlParameter("@Amount",   timePunches.Amount),
                    new SqlParameter("@CreatedDate",   timePunches.CreatedDate),
                    new SqlParameter("@CreatedBy",   timePunches.CreatedBy)
                };
                //_OrgdbContext.TimePunches.Add(timePunches);
                // _OrgdbContext.SaveChanges();
                _OrgdbContext.ExecStoredProcedureWithOutOutput<Employees>("SaveTimePunchDetail", parameters.Length, parameters).AsQueryable();

                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.TimePunchesSaveSuccessMsg,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.OK)
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = Convert.ToInt32(HttpStatusCodes.BadRequest)
                };
            }
        }

        public JsonModel UpdateTimePunch(TimePunches timePunches)
        {
            try
            {
                string _changeValue = "", _changingValue = "",_fieldName = "";
                var entity = _OrgdbContext.TimePunches.FirstOrDefault(item => item.TimePunchId ==timePunches.TimePunchId && item.IsActive == true && item.IsDeleted == false);
                //sa Abhishek         

                if (entity != null)
                {

                    if (entity.Hours != timePunches.Hours)
                    {
                        _changeValue = entity.Hours.ToString();
                        _changingValue = timePunches.Hours.ToString();
                        _fieldName = "Hours";
                    }
                    else if (entity.HourlyPayRate != timePunches.HourlyPayRate)
                    {
                        _changeValue = entity.HourlyPayRate.ToString();
                        _changingValue = timePunches.HourlyPayRate.ToString();
                        _fieldName = "HourlyPayRate";
                    }
                    else if (entity.OvertimeMultiplier != timePunches.OvertimeMultiplier)
                    {
                        _changeValue = entity.OvertimeMultiplier.ToString();
                        _changingValue = timePunches.OvertimeMultiplier.ToString();
                        _fieldName = "OvertimeMultiplier";
                    }

                    // Make changes on entity
                    entity.InDateTime = timePunches.InDateTime;
                    entity.OutDateTime = timePunches.OutDateTime;
                    entity.HoursClock = timePunches.HoursClock;
                    entity.Hours = timePunches.Hours;
                    entity.HourlyPayRate = timePunches.HourlyPayRate;
                    entity.OvertimeMultiplier = timePunches.OvertimeMultiplier;
                    entity.Amount = timePunches.Amount;
                    entity.UpdatedDate = DateTime.Now;
                    entity.UpdatedBy = timePunches.UpdatedBy;
                    entity.PayPeriodId = timePunches.PayPeriodId;
                    entity.timepuncheschangeChk = timePunches.timepuncheschangeChk;
                    _OrgdbContext.TimePunches.Update(entity);

                    TimePunchChanges timePunchChanges = new TimePunchChanges();

                    

                   

                    if(_changingValue.Length > 0) 
                    {
                        timePunchChanges.TimePunchID = timePunches.TimePunchId;
                        timePunchChanges.TimePunchChangeFrom = _changeValue;
                        timePunchChanges.TimePunchChangeTo = _changingValue;
                        timePunchChanges.FieldName = _fieldName;
                        timePunchChanges.CreatedDate = DateTime.UtcNow;
                        timePunchChanges.CreatedBy = timePunches.UpdatedBy;
                        timePunchChanges.IsActive = true;
                        timePunchChanges.IsDeleted = false;
                        _OrgdbContext.TimePunchChanges.Add(timePunchChanges);
                    }


                    EmployeeTimePunchChange employeeTimePunchChange = new EmployeeTimePunchChange();
                    var ChkEmployee = _OrgdbContext.EmployeeTimePunchChange.FirstOrDefault(r => r.EmployeeId == entity.EmployeeId);
                    if (ChkEmployee == null)
                    {
                        employeeTimePunchChange.EmployeeId = entity.EmployeeId;
                        employeeTimePunchChange.CreatedDate = DateTime.UtcNow;
                        employeeTimePunchChange.CreatedBy = timePunches.UpdatedBy;
                        employeeTimePunchChange.PayPeriodId = (int)timePunches.PayPeriodId;
                        employeeTimePunchChange.IsActive = true;
                        employeeTimePunchChange.IsDeleted = false;
                        _OrgdbContext.EmployeeTimePunchChange.Add(employeeTimePunchChange);
                    }
                    else
                    {
                        ChkEmployee.IsActive = true;
                        ChkEmployee.IsDeleted = false;
                        _OrgdbContext.EmployeeTimePunchChange.Update(ChkEmployee);
                    }
                   
                        //

                        // Save changes in database
                        _OrgdbContext.SaveChanges();
                }


                return new JsonModel()
                {
                    data = new object(),
                    Message = StatusMessage.TimePunchesUpdateSuccessMsg,
                    StatusCode = (int)HttpStatusCodes.OK
                };
            }
            catch (Exception ex)
            {
                return new JsonModel()
                {
                    data = new object(),
                    Message = ex.Message,
                    StatusCode = (int)HttpStatusCodes.BadRequest
                };
            }
        }

    }
}

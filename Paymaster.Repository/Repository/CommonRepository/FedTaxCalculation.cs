﻿using Paymaster.DataContract.Organization.ViewModel;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.ICommonRepository;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Paymaster.Common.TaxCalculate
{
    public class FedTaxCalculation : IFedTaxCalculation
    {
        private readonly PaymasterOrgDbContext _OrgdbContext;
        public FedTaxCalculation(PaymasterOrgDbContext OrgdbContext)
        {
            _OrgdbContext = OrgdbContext;
        }
        decimal withholdingAllowanceAmount = 0;
        List<PercentageTableRow> percentageTable = new List<PercentageTableRow>();
        List<WageBracketTableRow> wageBracketTable = new List<WageBracketTableRow>();
        public decimal FedTaxCalculate(string createDate, int payFrequency)
        {
            try
            {
                int year = DateTime.Parse(createDate).Year;
                var res1 = (from percent in _OrgdbContext.FedPercentageMethodTables
                            where percent.Year == year && percent.PayFrequency == payFrequency
                            orderby percent.MaritalStatus, percent.FedPercentageMeathodOver ascending
                            select new PercentageTableRow
                            {
                                over = percent.FedPercentageMeathodOver,
                                Id = percent.FedPercentageMethodId,
                                maritalStatus = percent.MaritalStatus,
                                withold = percent.Withhold,
                                plusPercentage = percent.PlusPercentage
                            }).ToList();


                foreach (var item in res1)
                {
                    PercentageTableRow newRow = new PercentageTableRow();
                    newRow.maritalStatus = item.maritalStatus;
                    newRow.over = item.over;
                    newRow.withold = item.withold;
                    newRow.plusPercentage = item.plusPercentage;
                    percentageTable.Add(newRow);
                }
                var res2 = (from allowance in _OrgdbContext.FedAllowances
                            where allowance.Year == year && allowance.PayFrequency == payFrequency
                            select new
                            {
                                allowance.EachAllowanceAmount
                            }).ToList();

                withholdingAllowanceAmount = Convert.ToDecimal(res2[0].EachAllowanceAmount);

                var wageRes = (from wage in _OrgdbContext.FedWageBracketMethodTables
                               where wage.Year == year && wage.PayFrequency == payFrequency
                               select new WageBracketTableRow
                               {
                                   Id = wage.FedWageBracketMethodId,
                                   maritalStatus = wage.MaritalStatus,
                                   allowances = wage.Allowances,
                                   over = wage.FedWageBracketMethodOver,
                                   subtract = wage.Subtract,
                                   percentage = wage.Percentage
                               }).ToList();
                foreach (var item in wageRes)
                {
                    WageBracketTableRow newRow = new WageBracketTableRow();
                    newRow.maritalStatus = item.maritalStatus;
                    newRow.allowances = item.allowances;
                    newRow.over = item.over;
                    newRow.subtract = item.subtract;
                    newRow.percentage = item.percentage;
                    wageBracketTable.Add(newRow);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return withholdingAllowanceAmount;
        }

        public decimal TaxPercentage(decimal? gross, int martialStatus, int allowances)
        {
            decimal withHold = 0, plusPercent = 0, over = 0;
            int found = 0, goOn = 1;
            gross -= (allowances * withholdingAllowanceAmount);
            for(int i=0; goOn == 1 && i<percentageTable.Count; i++)
            {
                if (percentageTable[i].maritalStatus == martialStatus)
                {
                    found = 1;
                    if (gross > percentageTable[i].over)
                    {
                        withHold = percentageTable[i].withold;
                        plusPercent = percentageTable[i].plusPercentage;
                        over = percentageTable[i].over;
                    }
                    else
                    {
                        goOn = 0;
                    }
                }
                else if (found != 0)
                {
                    goOn = 0;
                }
            }

            Decimal rv = (withHold + ((gross - over) * plusPercent * .01M)) ?? 0;
            rv = Decimal.Round(rv, 2);
            return (rv);
        }

        public decimal TaxBracket(decimal? gross, int martialStatus, int allowances)
        {
            decimal subtract = 0, plusPercent = 0;
            int found = 0, goOn = 0;
            for (int i = 0; goOn == 1 && i < wageBracketTable.Count; i++)
            {
                if (wageBracketTable[i].maritalStatus == martialStatus && wageBracketTable[i].allowances == allowances)
                {
                    found = 1;
                    if (gross > wageBracketTable[i].over)
                    {
                        subtract = wageBracketTable[i].subtract;
                        plusPercent = wageBracketTable[i].percentage;
                    }
                    else
                    {
                        goOn = 0;
                    }
                }
                else if (found != 0)
                {
                    goOn = 0;
                }
            }
            Decimal rv = ((gross - subtract) * plusPercent * .01M) ?? 0;
            rv = Decimal.Round(rv, 2);
            return (rv);
        }
    }
}

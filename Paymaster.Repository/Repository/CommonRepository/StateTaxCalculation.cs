﻿using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.ICommonRepository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Paymaster.DataContract.Organization.Employees;

namespace Paymaster.Common.TaxCalculate
{
    public class StateTaxCalculation : IStateTaxCalculation
    {
        private readonly PaymasterOrgDbContext _OrgdbContext;
        public StateTaxCalculation(PaymasterOrgDbContext OrgdbContext)
        {
            _OrgdbContext = OrgdbContext;
        }
        StateExemptions model = new StateExemptions();
        public StateExemptions StateTaxCalulate(string year, Int32 payFrequency)
        {
            int Year = DateTime.Parse(year).Year;
            try
            {
                var stateTax = (from exemptions in _OrgdbContext.StateExemptions
                                where exemptions.Year == Year && exemptions.PayFrequency == payFrequency
                                select new StateExemptions
                                {
                                    PerExemptionAmount = exemptions.PerExemptionAmount,
                                    Plus = exemptions.Plus,
                                    HeadOfHousehold = exemptions.HeadOfHousehold,
                                    Blind = exemptions.Blind,
                                    StudentLimit = exemptions.StudentLimit
                                }).ToList();
                foreach (var item in stateTax)
                {
                    model.PerExemptionAmount = item.PerExemptionAmount;
                    model.Plus = item.Plus;
                    model.HeadOfHousehold = item.HeadOfHousehold;
                    model.Blind = item.Blind;
                    model.StudentLimit = item.StudentLimit;
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Decimal TaxPercentageMethod(Decimal? gross, Int16 exemptions, bool headOfHousehold, char blind, bool student)
        {
            decimal rv = 0M;
            decimal g = gross ?? 0;

            if (student == true && exemptions > 0 && g < model.StudentLimit)
            {
                rv = 0M;
            }
            else
            {
                if (exemptions > 0)
                    g -= ((exemptions * model.PerExemptionAmount) + model.Plus);

                g -= (headOfHousehold == true ? model.HeadOfHousehold : 0);

                switch (blind)
                {
                    case 'I':
                    case 'S':
                        g -= model.Blind;
                        break;
                    case 'B':
                        g -= (model.Blind * 2);
                        break;
                    default:
                        break;
                }
                rv = g * .0505M;
                rv = Decimal.Round(rv, 2);

                if (rv < 0)
                    rv = 0;
            }
            return (rv);
        }
    }
}

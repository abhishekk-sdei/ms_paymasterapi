﻿using Paymaster.DataContract.Common;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository.ICommonRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paymaster.Repository.Repository.CommonRepository
{
    public class CommonRepository : ICommonRepository
    {

        private readonly PaymasterDBContext _dbcontext;
        private readonly PaymasterOrgDbContext _OrgdbContext;

        public CommonRepository(PaymasterDBContext dbcontext)
        {
            _dbcontext = dbcontext;
        }

        public List<CountryMaster> GetCountryList() 
        {
            List<CountryMaster> CountryList = new List<CountryMaster>();
            CountryList = _dbcontext.CountryMaster.Where(r => r.IsActive == true && r.IsDeleted == false).ToList();
            return CountryList;
        }

        public List<StateMaster> GetStateList(int CountryId)
        {
            List<StateMaster> StateList = new List<StateMaster>();
            StateList = _dbcontext.StateMaster.Where(r => r.CountryID == CountryId && r.IsActive == true && r.IsDeleted == false).ToList();
            return StateList;
        }

        public List<CityMaster> GetCityList(int CountryId, int StateId)
        {
            List<CityMaster> CityList = new List<CityMaster>();
            CityList = _dbcontext.CityMaster.Where(r => r.CountryID == CountryId && r.StateId == StateId && r.IsActive == true && r.IsDeleted == false).ToList();
            return CityList;
        }

        public List<GenderMaster> GetGenderList()
        {
            List<GenderMaster> GenderList = new List<GenderMaster>();
            GenderList = _dbcontext.GenderMaster.Where(r => r.IsActive == true && r.IsDeleted == false).ToList();
            return GenderList;
        }

        public List<MaritalStatusMaster> GetMaritalStatusList()
        {
            List<MaritalStatusMaster> MaritalStatusList = new List<MaritalStatusMaster>();
            MaritalStatusList = _dbcontext.MaritalStatusMaster.Where(r => r.IsActive == true && r.IsDeleted == false).ToList();
            return MaritalStatusList;
        }
        public List<RaceMaster> GetRaceList()
        {
            List<RaceMaster> RaceList = new List<RaceMaster>();
            RaceList = _dbcontext.RaceMaster.Where(r => r.IsActive == true && r.IsDeleted == false).ToList();
            return RaceList;
        }

        public List<PeriodMaster> GetPayPeriodList()
        {
            List<PeriodMaster> RaceList = new List<PeriodMaster>();
            RaceList = _dbcontext.PeriodMaster.Where(r => r.IsActive == true && r.IsDeleted == false).ToList();
            return RaceList;
        }
        public List<TypeMaster> GetTypeList()
        {
            List<TypeMaster> TypeMasterList = new List<TypeMaster>();
            TypeMasterList = _dbcontext.TypeMaster.Where(r => r.IsActive == true && r.IsDeleted == false).ToList();
            return TypeMasterList;
            
        }

        public void ErrorHandling(Exception ex)
        {
            ErrorLog objErrorHandler = new ErrorLog();
            objErrorHandler.ErrorNumber = 1;
            objErrorHandler.ErrorType = ex.GetType().ToString();
            objErrorHandler.ErrorLine = 1;
            objErrorHandler.ErrorTime = DateTime.UtcNow;
            objErrorHandler.ErrorState = ex.StackTrace;
            objErrorHandler.ErrorSeverity = ex.Data.Count;
            objErrorHandler.ErrorMessage = ex.Message + " Inner exception " + ex.InnerException.Message;
            objErrorHandler.ErrorProcedure = "";
           _OrgdbContext.ErrorHandlerModel.Add(objErrorHandler);
        }
    }
}

﻿using Paymaster.DataContract.User;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Paymaster.DataContract.Authentication;

namespace Paymaster.Repository.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly PaymasterOrgDbContext _dbOrgContext;

        private readonly PaymasterDBContext _dbContext;

        public UserRepository(PaymasterOrgDbContext dbOrgContext, PaymasterDBContext dbContext)
        {
            _dbOrgContext = dbOrgContext;
            _dbContext = dbContext;
        }
        public TokenModel Authenticate(string email, string password)
        {
            TokenModel obj = new TokenModel();
            var user = _dbOrgContext.Users.FirstOrDefault(s => s.Email == email && s.Password == password && s.IsActive == true && s.IsDeleted == false);
            obj.UserName = user.UserName;
            obj.Email = user.Email;
            obj.RoleID = user.RoleType;
            obj.UserID = Convert.ToInt32(user.UserId);
            return obj;
        }

        public TokenModel AdminAuthenticate(string email, string password)
        {
            TokenModel obj = new TokenModel();
            var user = _dbContext.AdminUsers.FirstOrDefault(s => s.Email == email && s.Password == password && s.IsActive == true && s.IsDeleted == false);
            obj.UserName = user.UserName;
            obj.Email = user.Email;
            obj.RoleID = user.RoleType;
            return obj;
        }

    }
}

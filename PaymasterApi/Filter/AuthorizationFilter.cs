﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymasterApi.Filter
{
    public class AuthorizationFilter : IAuthorizationRequirement
    {
        public string AccessTokenValue { get; private set; }
        public AuthorizationFilter(string accessToken)
        {
            AccessTokenValue = accessToken;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Paymaster.Common.Options;
using Paymaster.Common.TaxCalculate;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository;
using Paymaster.Repository.InterfaceRepository.Admin.ILoginRepository;
using Paymaster.Repository.InterfaceRepository.ICommonRepository;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using Paymaster.Repository.Repository;
using Paymaster.Repository.Repository.Admin.LoginRepository;
using Paymaster.Repository.Repository.CommonRepository;
using Paymaster.Repository.Repository.OrganizationRepository;
using Paymaster.Services.Iservices;
using Paymaster.Services.Iservices.Admin;
using Paymaster.Services.Iservices.CommonInterface;
using Paymaster.Services.Iservices.OrganizationInterface;
using Paymaster.Services.Iservices.Token;
using Paymaster.Services.Services;
using Paymaster.Services.Services.Admin;
using Paymaster.Services.Services.CommonService;
using Paymaster.Services.Services.OrganizationServices;
using PaymasterApi.Filter;

namespace PaymasterApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private SymmetricSecurityKey _signingKey;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            try
            {
                services.AddControllers();
                services.AddCors();
                services.AddMvc();
                services.AddMvc().AddRazorPagesOptions(options =>
                {
                    options.RootDirectory = "/views";
                });

                var JwtIssuerOptions = Configuration.GetSection("JwtIssuerOptions");
                services.Configure<JwtIssuerOptions>(JwtIssuerOptions);

                // configure jwt authentication
                var JwtIssuerSetting = JwtIssuerOptions.Get<JwtIssuerOptions>();

                var key = Encoding.ASCII.GetBytes(JwtIssuerSetting.JwtKey);

                //services.AddCors(c =>
                //{
                //    c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
                //});


                services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    //x.("AuthorizedUser",
                    //          policy =>
                    //            policy.Requirements.Add(new AuthorizationFilter("Paymaster")));
                }).AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false

                        //ValidateIssuerSigningKey = true,
                        //ValidateIssuer = true,
                        //ValidateAudience = true,
                        //ValidIssuer = JwtIssuerOptions[nameof(JwtIssuerSetting.Issuer)],
                        //IssuerSigningKey = new SymmetricSecurityKey(key),
                        //ValidAudience = JwtIssuerOptions[nameof(JwtIssuerSetting.JwtAudience)],
                        //RequireExpirationTime = true,
                        //ValidateLifetime = true,
                        //ClockSkew = TimeSpan.Zero
                    };
                });

                //services.AddAuthorization(options =>
                //{
                //    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                //    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                //    //options.AddPolicy("AuthorizedUser",
                //    //      policy =>
                //    //        policy.Requirements.Add(new AuthorizationFilter("Paymaster")));


                //});
                //var jwtAppSettingOptions = Configuration.GetSection("JwtIssuerOptions");
                _signingKey = new SymmetricSecurityKey(key);

                //services.Configure<JwtIssuerOptions>(options =>
                //{
                //    options.Issuer = JwtIssuerOptions[nameof(JwtIssuerSetting.Issuer)];
                //    options.JwtAudience = JwtIssuerOptions[nameof(JwtIssuerSetting.JwtAudience)];
                //    options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
                //});

                //var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));


                //var tokenValidationParameters = new TokenValidationParameters
                //{
                //    ValidateIssuer = true,
                //    ValidIssuer = JwtIssuerOptions[nameof(JwtIssuerSetting.Issuer)],

                //    ValidateAudience = true,
                //    ValidAudience = JwtIssuerOptions[nameof(JwtIssuerSetting.JwtAudience)],

                //    ValidateIssuerSigningKey = true,

                //    IssuerSigningKey = _signingKey,

                //    RequireExpirationTime = true,
                //    ValidateLifetime = true,

                //    ClockSkew = TimeSpan.Zero
                //};



                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "paymasterAPI", Version = "v1" });
                   
                });

                #region DI

                #region AppServiceDI
                services.AddScoped<IUserService, UserService>();
                services.AddScoped<ITokenService, TokenService>();
                services.AddScoped<IOrganizationServices, OrganizationServices>();
                services.AddScoped<IEmployeeService, EmployeeService>();
                services.AddScoped<ITimePunchService, TimePunchService>();
                services.AddScoped<ICheckeServices, CheckServices>();
                services.AddScoped<ICommonService, CommonService>();
                services.AddScoped<ILoginService, LoginService>();
                services.AddScoped<IProcessPayrollServices, ProcessPayrollService>();
                
                #endregion

                #region AppRepositoryDI
                services.AddScoped<IUserRepository, UserRepository>();
                services.AddScoped<ITokenRepository, TokenRepository>();
                services.AddScoped<IOrganizationRepository, OrganizationRepository>();
                services.AddScoped<IEmployeeRepository, EmployeeRepository>();
                services.AddScoped<ITimePunchRepository, TimePunchRepository>();
                services.AddScoped<IChecksRepository, ChecksRepository>();
                services.AddScoped<ICommonRepository, CommonRepository>();
                services.AddScoped<ILoginRepository, LoginRepository>();
                services.AddScoped<IProcessPayrollRepository, ProcessPayrollRepository>();
                services.AddScoped<IFedTaxCalculation, FedTaxCalculation>();
                services.AddScoped<IStateTaxCalculation, StateTaxCalculation>();
            
                //services.AddScoped<ITokenRepository, TokenRepository>();
                #endregion

                #endregion

                services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
                //Master Context from App Settings
                services.AddDbContext<PaymasterDBContext>(option => { option.UseSqlServer(Configuration.GetConnectionString("PMMasterDB"), b => b.MigrationsAssembly("PaymasterApi")); });
                services.AddDbContext<PaymasterOrgDbContext>(option => { option.UseSqlServer(Configuration.GetConnectionString("PMORGMasterDB"), b => b.MigrationsAssembly("PaymasterApi")); });
                //services.AddDbContext<PaymasterOrgDbContext>();

            }
            catch (Exception ex) { }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            try
            {
                if (env.IsDevelopment())
                {
                    app.UseDeveloperExceptionPage();
                }

                app.UseCors(x => x
                   .AllowAnyOrigin()
                   .AllowAnyMethod()
                   .AllowAnyHeader());

                app.UseHttpsRedirection();

                app.UseAuthentication();


                app.UseRouting();

                app.UseAuthorization();

                app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });

                app.UseStaticFiles();

                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "PayMaster");
                });
                app.Run(async (context) =>
                {
                    await context.Response.WriteAsync("<h1>Welcome to Paymaster API</h1>");

                });
            }
            catch (Exception ex) { }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.DataContract.User;
using Paymaster.Common;
using Paymaster.Services.Iservices;
using Paymaster.Services.Iservices.OrganizationInterface;
using Paymaster.Services.Services.OrganizationServices;
using Paymaster.DataContract.Organization.ViewModal.LoginModal;
using Paymaster.DataContract.Common;
using static Paymaster.Common.ConstantString;
using static Paymaster.Common.Enum.CommonEnums;
using Paymaster.Common.Options;
using Microsoft.Extensions.Options;

namespace PaymasterApi.Controllers.Organization
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]

    public class OrganizationController : ControllerBase
    {
        private readonly IOrganizationServices _organizationServices;
        private readonly IUserService _UserService;
        private readonly JwtIssuerOptions _jwtOptions;
        public OrganizationController(IOrganizationServices organizationServices, IOptions<JwtIssuerOptions> jwtOptions, IUserService UserService)
        {
            _jwtOptions = jwtOptions.Value;
            _organizationServices = organizationServices;
            _UserService = UserService;
        }


        /// <summary>        
        /// Description  : Login for user
        /// </summary>
        /// /// <param name="loginViewModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        public IActionResult Login([FromBody]LoginViewModel loginViewModel)
        {

            Users users = new Users();
            Employees employees = new Employees();

            users.Email = loginViewModel.Username;
            users.Password = loginViewModel.Password;

            Users _users = _organizationServices.CheckCredentials(users);
            //int _id = Convert.ToInt32(_users.UserId);
            IActionResult response = Unauthorized();
            if (_users != null)
            {
                var user = _UserService.Authenticate(_users);
                response = Ok(new { token = user.AccessToken, UserId = _users.UserId, StatusCode = 200, Message = "Valid Credentials" });

            }   
            else
            {
                response = Ok(new { token = "", UserId = 0, StatusCode = 400, Message = "Invalid Credentials" });
            }
            return Ok(response);
        }

        /// <summary>        
        /// Description  : Get employee list
        /// </summary>
        /// /// <param name="page"></param>
        /// /// <param name="pageSize"></param>
        /// /// <param name="TimePunchCheck"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetEmployeeList")]
        public JsonModel GetEmployeeList(int page = 0, int pageSize = 10, int TimePunchCheck = 0)
        {
            int Totalrecords = 0;
            Employees employees = new Employees();
            List<EmployeeViewModel> EmployeeList = _organizationServices.GetEmployeeList(TimePunchCheck, page, pageSize);
            if (EmployeeList.Count > 0)
            {
                Totalrecords = EmployeeList[0].TotalRecords;
                foreach (var lst in EmployeeList)
                {
                    lst.EnctypedEmployeeID = CommonMethod.Encrypt(lst.EmployeeID.ToString());
                }
            }
            JsonModel obj = new JsonModel();
            obj.data = EmployeeList;


            obj.Message = StatusMessage.FetchMessage;

            obj.StatusCode = (int)HttpStatusCodes.OK;//Success
            obj.meta = new Meta()
            {
                TotalRecords = EmployeeList != null && Totalrecords > 0 ? Totalrecords : 0,
                CurrentPage = page,
                PageSize = pageSize,
                DefaultPageSize = pageSize,
                TotalPages = Math.Ceiling(Convert.ToDecimal((EmployeeList != null && EmployeeList.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
            };
            return obj;
        }

        /// <summary>        
        /// Description  : Get employee by employee id
        /// </summary>
        /// /// <param name="EmpId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetEmployeeById")]
        public JsonModel GetEmployeeListById(string EmpId)
        {
            //var EmployeeId  = Convert.ToInt32(CommonMethod.Decrypt(EmpId));
            var EmployeeId  = Convert.ToInt32(EmpId);
            EmployeesDetails employees = new EmployeesDetails();
            EmployeesDetails EmployeeList = _organizationServices.GetEmployeeListById(EmployeeId).FirstOrDefault();
            if (EmployeeList != null)
            {
                /*EmployeeList.EmployeeID*/
                EmployeeList.EnyEmployeeID = CommonMethod.Encrypt(EmployeeList.EmployeeID.ToString()); 

                return new JsonModel()
                {
                    data = EmployeeList,
                    Message = StatusMessage.FetchMessage,
                    StatusCode = (int)HttpStatusCodes.OK,
                };
            }
            else
            {
                return new JsonModel()
                {
                    data = EmployeeList,
                    Message = StatusMessage.NoFetchMessage,
                    StatusCode = (int)HttpStatusCodes.NotFound,
                };
            }
        }

        /// <summary>        
        /// Description  : Get active employees
        /// </summary>
        /// /// <param name="EmpId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetActiveEmployee")]
        public IActionResult GetActiveEmployee()
        {
            //var EmployeeId  = Convert.ToInt32(CommonMethod.Decrypt(EmpId));
      
            List<ActiveEmployees> EmployeeList = _organizationServices.GetActiveEmployee();
            return Ok(EmployeeList);
        }

        /// <summary>        
        /// Description  : Get previous employees
        /// </summary>
        /// /// <param name="EmpId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetPreviousEmployee")]
        public IActionResult GetPreviousEmployee()
        {
            List<ActiveEmployees> EmployeeList = _organizationServices.GetPreviousEmployee();
            return Ok(EmployeeList);

        }

        /// <summary>        
        /// Description  : Get employee list for searching
        /// </summary>
        /// /// <param name="Seaching"></param>
        /// /// <param name="page"></param>
        /// /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetEmployeeListByName")]
        public JsonModel GetEmployeeListByName(string Seaching = "", int page = 0, int pageSize = 10)
        {
            int Totalrecords = 0;
            List<EmployeeViewModel> EmployeeList = _organizationServices.GetEmployeeListByName(Seaching, page, pageSize);
            //int Totalrecords = EmployeeList.Count();
            // var reponse = EmployeeList.Skip(pageSize * (page - 1)).Take(pageSize).ToList();

            // return CommonMethod.Pagination<EmployeeViewModel>(reponse, page, pageSize, Totalrecords);
            if (EmployeeList.Count > 0)
            {
                Totalrecords = EmployeeList[0].TotalRecords;
                foreach (var lst in EmployeeList)
                {
                    lst.EnctypedEmployeeID = CommonMethod.Encrypt(lst.EmployeeID.ToString());
                }
            }
            JsonModel obj = new JsonModel();
            obj.data = EmployeeList;


            obj.Message = StatusMessage.FetchMessage;

            obj.StatusCode = (int)HttpStatusCodes.OK;//Success
            obj.meta = new Meta()
            {
                TotalRecords = EmployeeList != null && Totalrecords > 0 ? Totalrecords : 0,
                CurrentPage = page,
                PageSize = pageSize,
                DefaultPageSize = pageSize,
                TotalPages = Math.Ceiling(Convert.ToDecimal((EmployeeList != null && EmployeeList.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
            };

            return obj;
        }

        //sa Abhishek 
        /// <summary>        
        /// Description  : Delete employee
        /// </summary>
        /// /// <param name="EmployeeId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("DeleteEmployee")]
        public JsonModel DeleteEmployee(int EmployeeId)
        {
            return _organizationServices.DeleteEmployee(EmployeeId);

        }
        //ea Abhishek

        //sa Abhishek 
        /// <summary>        
        /// Description  : Check Socail Security Number
        /// </summary>
        /// /// <param name="SocSecNo"></param>
        /// /// <param name="empid"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("CheckSocSecNo")]
        [HttpGet]
        public JsonModel CheckSocSecNo(string SocSecNo, string empid)
        {
            int CHK = 0;
            var EmpID = Convert.ToInt32(empid);
            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            List<EmployeeSocSecModel> EmployeeList = _organizationServices.GetAllEmployeeList();
                EmployeeList = EmployeeList.Where(r => r.SocSec == SocSecNo).ToList();
                CHK = EmployeeList.Count>0 == true ? 0 : 1;


            return new JsonModel()
            {
                data = new object(),
                Message = CHK == 0 ? "Soc Sec already exists." : "Soc Sec not exists.",
                StatusCode = CHK == 0 ? 409 : 200
            };

        }

    }
}
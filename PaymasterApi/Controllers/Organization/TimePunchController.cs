﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Paymaster.Common;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Services.Iservices.CommonInterface;
using Paymaster.Services.Iservices.OrganizationInterface;
using static Paymaster.Common.ConstantString;
using static Paymaster.Common.Enum.CommonEnums;

namespace PaymasterApi.Controllers.Organization
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TimePunchController : ControllerBase
    {
        private readonly ITimePunchService _timePunchServices;
        private readonly ICheckeServices _checkeServices;
        private readonly ICommonService _commonService;

        //public TimePunchController(ITimePunchService timePunchServices,)
        public TimePunchController(ITimePunchService timePunchServices, ICheckeServices checkeServices, ICommonService commonService)
        {
            _timePunchServices = timePunchServices;
            _checkeServices = checkeServices;
            _commonService = commonService;
        }

        /// <summary>
        /// Description : Get time punches list 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetEmployeeTimelist")]
        public JsonModel GetEmployeeTimeList(int page = 0, int pageSize = 10)
        {
            var respone = _timePunchServices.TimePunchList(page, pageSize);
            int Totalrecords = respone.TimePunhesViewModel[0].TotalRecords;

            foreach (var lst in respone.TimePunhesViewModel)
            {
                lst.EnctypedEmployeeID = CommonMethod.Encrypt(lst.EmployeeId.ToString());
            }
            
            JsonModel obj = new JsonModel();
            obj.data = respone;

            
            obj.Message = StatusMessage.FetchMessage;

            obj.StatusCode = (int)HttpStatusCodes.OK;//Success
            obj.meta = new Meta()
            {
                TotalRecords = respone.TimePunhesViewModel != null && respone.TimePunhesViewModel[0].TotalRecords > 0 ? Totalrecords : 0,
                CurrentPage = page,
                PageSize = pageSize,
                DefaultPageSize = pageSize, 
                TotalPages = Math.Ceiling(Convert.ToDecimal((respone != null && respone.TimePunhesViewModel.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
            };

            return obj;
            //return Ok("test");
        }

        /// <summary>
        /// Description : View time punches list with filteration
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("ViewEmployeeTimelist")]
        public JsonModel ViewEmployeeTimelist(int page = 0, int pageSize = 10)
        {
            var respone = _timePunchServices.ViewEmployeeTimelist(page, pageSize);
            int Totalrecords = respone.TimePunhesViewModel[0].TotalRecords;

            foreach (var lst in respone.TimePunhesViewModel)
            {
                lst.EnctypedEmployeeID = CommonMethod.Encrypt(lst.EmployeeId.ToString());
            }

            JsonModel obj = new JsonModel();
            obj.data = respone;


            obj.Message = StatusMessage.FetchMessage;

            obj.StatusCode = (int)HttpStatusCodes.OK;//Success
            obj.meta = new Meta()
            {
                TotalRecords = respone.TimePunhesViewModel != null && respone.TimePunhesViewModel[0].TotalRecords > 0 ? Totalrecords : 0,
                CurrentPage = page,
                PageSize = pageSize,
                DefaultPageSize = pageSize,
                TotalPages = Math.Ceiling(Convert.ToDecimal((respone != null && respone.TimePunhesViewModel.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
            };

            return obj;
            //return Ok("test");
        }

        /// <summary>
        /// Description : Get employee tax deduction list
        /// </summary>
        /// <param name="CheckId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetEmployeeCheckDeductionList")]
        public IActionResult GetEmployeeCheckDeductionList(int CheckId)
        {
            List<ViewCheckDeduction> ViewCheckDeductionList = new List<ViewCheckDeduction>();
            ViewCheckDeductionList = _timePunchServices.GetEmployeeCheckDeductionList(CheckId);
            
            return Ok(ViewCheckDeductionList);
        }

        /// <summary>
        /// Description : View time punches list with filteration
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("ViewEmployeeFilteredTimelist")]
        public JsonModel ViewEmployeeFilteredTimelist(int page = 0, int pageSize = 10,int EmployeeId = 0,int PayPeriodId = 0)
        {
            int Totalrecords = 0;
            TimePunhesDetailModel respone = _timePunchServices.ViewEmployeeFilteredTimelist(page, pageSize, EmployeeId, PayPeriodId);
            if(respone.TimePunhesViewModel.Count > 0) 
            {
                Totalrecords = respone.TimePunhesViewModel[0].TotalRecords;
                foreach (var lst in respone.TimePunhesViewModel)
                {
                    lst.EnctypedEmployeeID = CommonMethod.Encrypt(lst.EmployeeId.ToString());
                }
            }
            

           

            JsonModel obj = new JsonModel();
            obj.data = respone;


            obj.Message = StatusMessage.FetchMessage;

            obj.StatusCode = (int)HttpStatusCodes.OK;//Success
            obj.meta = new Meta()
            {
                TotalRecords = respone.TimePunhesViewModel != null && Totalrecords > 0 ? Totalrecords : 0,
                CurrentPage = page,
                PageSize = pageSize,
                DefaultPageSize = pageSize,
                TotalPages = Math.Ceiling(Convert.ToDecimal((respone != null && respone.TimePunhesViewModel.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
            };

            return obj;
            //return Ok("test");
        }

        /// <summary>
        /// Description : Get time punches list for employee id
        /// </summary>
        /// <param name="EmpoyeeID"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetTimePunchList")]
        public IActionResult GetTimepunchList(string EmpoyeeID )
        {
            var empID = Convert.ToInt32(EmpoyeeID);
            List<TimePunches> TimePunchesList = _timePunchServices.GetTimePunchList(empID).ToList();
            return Ok(TimePunchesList);
        }

        /// <summary>
        /// Description : Get time punches list for employee id
        /// </summary>
        /// <param name="EmpoyeeID"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetTimePunchHistoryList")]
        public JsonModel GetTimePunchHistoryList(int page = 0, int pageSize = 10,int Emp_ID= 0,int PayPeriodId = 0,string SearchText = "")
        {
            // var empID = Convert.ToInt32(Emp_ID);
            List<TimePunchesHistoryModel> TimePunchesList = _timePunchServices.GetTimePunchHistoryList(page ,  pageSize, Emp_ID, PayPeriodId, SearchText).ToList();
            int Totalrecords = 0;
            if (TimePunchesList.Count > 0) {
                 Totalrecords = TimePunchesList[0].TotalRecords;
            }
            // return Ok(EmployeeList.Skip(pageSize * (page - 1)).Take(pageSize).ToList());
            JsonModel obj = new JsonModel();
            obj.data = TimePunchesList;


            obj.Message = StatusMessage.FetchMessage;

            obj.StatusCode = (int)HttpStatusCodes.OK;//Success
            obj.meta = new Meta()
            {
                TotalRecords = TimePunchesList != null && Totalrecords > 0 ? Totalrecords : 0,
                CurrentPage = page,
                PageSize = pageSize,
                DefaultPageSize = pageSize,
                TotalPages = Math.Ceiling(Convert.ToDecimal((TimePunchesList != null && TimePunchesList.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
            };

            return obj;
        }

        /// <summary>
        /// Description : Get check list for employee id
        /// </summary>
        ///  <param name="PayPeriodId"></param>
        /// <param name="SearchText"></param>
        /// <param name="Emp_ID"></param>
        ///  <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetEmployeecheckListHistory")]
        public JsonModel GetEmployeecheckListHistory(int page = 0, int pageSize = 10, int Emp_ID = 0, int PayPeriodId = 0, string SearchText = "")
        {
            // var empID = Convert.ToInt32(Emp_ID);
            List<ChecksHistoryModel> TimePunchesList = _timePunchServices.GetEmployeecheckListHistory(page, pageSize, Emp_ID, PayPeriodId, SearchText).ToList();
            int Totalrecords = 0;
            if (TimePunchesList.Count > 0)
            {
                Totalrecords = TimePunchesList[0].TotalRecords;
            }
            // return Ok(EmployeeList.Skip(pageSize * (page - 1)).Take(pageSize).ToList());
            JsonModel obj = new JsonModel();
            obj.data = TimePunchesList;


            obj.Message = StatusMessage.FetchMessage;

            obj.StatusCode = (int)HttpStatusCodes.OK;//Success
            obj.meta = new Meta()
            {
                TotalRecords = TimePunchesList != null && Totalrecords > 0 ? Totalrecords : 0,
                CurrentPage = page,
                PageSize = pageSize,
                DefaultPageSize = pageSize,
                TotalPages = Math.Ceiling(Convert.ToDecimal((TimePunchesList != null && TimePunchesList.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
            };

            return obj;
        }

        /// <summary>
        /// Description : View time punches list for employee id
        /// </summary>
        /// <param name="EmpoyeeID"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("ViewTimePunchList")]
        public IActionResult ViewTimePunchList(string EmpoyeeID)
        {
            var empID = Convert.ToInt32(EmpoyeeID);
            List<TimePunches> TimePunchesList = _timePunchServices.ViewTimePunchList(empID).ToList();
            return Ok(TimePunchesList);
        }

        /// <summary>
        /// Description : Add time punches
        /// </summary>
        /// <param name="timePunches"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("AddTimePunch")]
        public JsonModel AddTimePunch([FromBody] TimePunhesViewModel timePunches)
        {
            TimePunches punches = new TimePunches();
            punches.EmployeeId = timePunches.EmployeeId;
            punches.PayPeriodId = timePunches.CureentpayPeriodId;
            //punches.InDateTime = Convert.ToDateTime(timePunches.ReferenceDate.ToString().Split(' ')[0] + ' ' + timePunches.InDateTime.Split(' ')[0]);
            //punches.OutDateTime = Convert.ToDateTime(timePunches.ReferenceDate.ToString().Split(' ')[0] + ' ' + timePunches.OutDateTime.Split(' ')[0]); ;
            punches.Type = timePunches.Type.ToString();
            punches.ReferenceDate = timePunches.ReferenceDate;
            punches.Hours = timePunches.Hours;
            punches.HourlyPayRate = timePunches.HourlyPayRate;
            punches.OvertimeMultiplier = timePunches.OvertimeMultiplier;
            punches.Amount = timePunches.Amount;
            punches.CreatedDate = DateTime.UtcNow;
            punches.CreatedBy = timePunches.CreatedBy;

            JsonModel response = new JsonModel();
            // TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _timePunchServices.AddTimePunch(punches);
            return response;
        }

        /// <summary>
        /// Description : Update time punches
        /// </summary>
        /// <param name="timePunches"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("UpdateTimePunch")]
        public JsonModel UpdateTimePunch([FromBody] TimePunhesViewModel timePunches)
        {
            JsonModel response = new JsonModel();

            TimePunches punches = new TimePunches();
            Checks checks = new Checks();
            punches.PayPeriodId = timePunches.CureentpayPeriodId;
            punches.InDateTime = Convert.ToDateTime(timePunches.ReferenceDate.ToString().Split(' ')[0] + ' ' + timePunches.InDateTime.Split(' ')[0]);
            punches.OutDateTime = Convert.ToDateTime(timePunches.ReferenceDate.ToString().Split(' ')[0] + ' ' + timePunches.OutDateTime.Split(' ')[0]);
            punches.HoursClock = Convert.ToDecimal(timePunches.HoursClock);
            punches.Hours = timePunches.Hours;
            punches.HourlyPayRate = timePunches.HourlyPayRate;
            punches.OvertimeMultiplier = timePunches.OvertimeMultiplier;
            punches.Amount = timePunches.Amount;
            punches.TimePunchId = timePunches.TimePunchId;
            punches.UpdatedBy = timePunches.UpdatedBy;
            punches.timepuncheschangeChk = true;
            response = _timePunchServices.UpdateTimePunch(punches);
            //if (response.StatusCode == 200)
            //{
            //    checks.CheckId = timePunches.CheckId;
            //    checks.EmployeeId = timePunches.EmployeeId;
            //    //checks.EmployeeId = timePunches.EmployeeId;
            //    checks.Amount = timePunches.TotalAmount;
            //    checks.Gross = timePunches.TotalHrs  * timePunches.EmpHourlyPayRate;
            //    _checkeServices.UpdateCheckDetail(checks);
            //}
            return response;
        }


        /// <summary>
        /// Description : Get Checks list of employees 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize]
        [Route("GetEmployeesCheckslist")]
        [HttpGet]
        public JsonModel EmployeeChecklist(int page = 0, int pageSize = 10)
        {
            List<ViewChecksModel> lstViewChecksModel = new List<ViewChecksModel>();
            JsonModel respone = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            lstViewChecksModel = _checkeServices.GetChecksList(page, pageSize);
           

            // return respone;
            int Totalrecords = 0;
            if (lstViewChecksModel.Count > 0)
            {
                Totalrecords = lstViewChecksModel[0].TotalRecords;
            }
            // return Ok(EmployeeList.Skip(pageSize * (page - 1)).Take(pageSize).ToList());
            JsonModel obj = new JsonModel();
            obj.data = lstViewChecksModel;


            obj.Message = StatusMessage.FetchMessage;

            obj.StatusCode = (int)HttpStatusCodes.OK;//Success
            obj.meta = new Meta()
            {
                TotalRecords = lstViewChecksModel != null && Totalrecords > 0 ? Totalrecords : 0,
                CurrentPage = page,
                PageSize = pageSize,
                DefaultPageSize = pageSize,
                TotalPages = Math.Ceiling(Convert.ToDecimal((lstViewChecksModel != null && lstViewChecksModel.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
            };

            return obj;
        }

        /// <summary>
        /// Description : Get Checks list for binding dropdown for an employee
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize]
        [Route("GetDropdownBindCheckslist")]
        [HttpGet]
        public List<DropdownBindCheckList> GetDropdownBindCheckslist(int Emp_ID = 0,int PayPeriodId = 0)
        {
            List<DropdownBindCheckList> respone = new List<DropdownBindCheckList>();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            respone = _checkeServices.GetDropdownBindCheckslist(Emp_ID, PayPeriodId);
            return respone;
        }

        /// <summary>
        /// Description : View Checks Details
        /// </summary>
        /// <param name="PayPeriodId"></param>
        /// <param name="EmployeeId"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        [Authorize]
        [Route("ViewChecksDetails")]
        [HttpGet]
        public ViewChecksDetails ViewChecksDetails(int PayPeriodId = 0, int EmployeeId = 0,int ApplicationId = 0)
        {
            ViewChecksDetails respone = new ViewChecksDetails();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            respone = _checkeServices.ViewChecksDetails(PayPeriodId, EmployeeId, ApplicationId);
            return respone;
        }
    }
}
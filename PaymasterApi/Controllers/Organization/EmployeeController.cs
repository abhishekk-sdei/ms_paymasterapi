﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Paymaster.Common;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Services.Iservices.OrganizationInterface;
using Paymaster.Services.Services.OrganizationServices;

namespace PaymasterApi.Controllers.Organization
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
       
        public EmployeeController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;


        }


        
        [Authorize]
        [Route("SaveEmployee")]
        [HttpPost]
        /// <summary>        
        /// Description  : Save Employee
        /// </summary>
        /// /// <param name="employees"></param>
        /// <returns></returns>
        public JsonModel SaveEmployee([FromBody] EmployeesDetails employees) 
        {
             JsonModel response = new  JsonModel();
        TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response =  _employeeService.SaveEmployee(employees);
            return response;
        }

        [Authorize]
        [Route("SaveEmployeeBasicDetails")]
        [HttpPost]
        /// <summary>        
        /// Description  : Save Employee Basic Details
        /// </summary>
        /// /// <param name="employees"></param>
        /// <returns></returns>
        public JsonModel SaveEmployeeBasicDetails([FromBody] EmployeesDetails employees)
        {

            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _employeeService.SaveEmployeeBasicDetails(employees);
            return response;
        }

        [Authorize]
        [Route("SaveEmployeeSalaryDetails")]
        [HttpPost]
        /// <summary>        
        /// Description  : Save Employee Salary Details
        /// </summary>
        /// /// <param name="employees"></param>
        /// <returns></returns>
        public JsonModel SaveEmployeeSalaryDetails([FromBody] EmployeeSalaryDetailsModel employees)
        {

            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _employeeService.SaveEmployeeSalaryDetails(employees);
            return response;
        }

        [Authorize]
        [Route("SaveEmployeePersonalDetails")]
        [HttpPost]
        /// <summary>        
        /// Description  : Save Employee Personal Details
        /// </summary>
        /// /// <param name="employees"></param>
        /// <returns></returns>
        public JsonModel SaveEmployeePersonalDetails([FromBody] EmployeePersonalDetailsModel employees)
        {

            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _employeeService.SaveEmployeePersonalDetails(employees);
            return response;
        }

        [Authorize]
        [Route("SaveEmployeePaymentDetails")]
        [HttpPost]
        /// <summary>        
        /// Description  : Save Employee Payment Details
        /// </summary>
        /// /// <param name="employees"></param>
        /// <returns></returns>
        public JsonModel SaveEmployeePaymentDetails([FromBody] EmployeesDetails employees)
        {

            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _employeeService.SaveEmployeePaymentDetails(employees);
            return response;
        }


        [Authorize]
        [Route("UpdateEmployee")]
        [HttpPost]
        /// <summary>        
        /// Description  : Update Employee
        /// </summary>
        /// /// <param name="employeeDetail"></param>
        /// <returns></returns>
        public JsonModel UpdateEmployee([FromBody]EmployeesDetails employeeDetail)
        {

            
            Employees employees = new Employees();

            employees.EmployeeID = Convert.ToInt32(CommonMethod.Decrypt(employeeDetail.EnyEmployeeID).ToString());
            employees.SocSec = employeeDetail.SocSec;
            employees.FirstName = employeeDetail.FirstName;
            employees.MiddleName = employeeDetail.MiddleName;
            employees.LastName = employeeDetail.LastName;
            employees.Sex = employeeDetail.Sex;
            employees.Race = employeeDetail.Race;
            employees.MaritalStatus = employeeDetail.MaritalStatus;
            employees.BirthDate = employeeDetail.BirthDate;
            employees.CityId = employeeDetail.CityId;
            employees.StateId = employeeDetail.StateId;
            employees.CountryId = employeeDetail.CountryId;
            employees.HourlyPayRate = employeeDetail.HourlyPayRate;


            //employees.EmployeeID = employees.EmployeeID == 0 ? Convert.ToInt32(CommonMethod.Decrypt(employees.EnyEmployeeID).ToString()):0;
            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _employeeService.UpdateEmployee(employees);
            return response;
        }

        [Authorize]
        [Route("UpdateEmployeeBasicDetails")]
        [HttpPost]
        /// <summary>        
        /// Description  : Update Employee Basic Details
        /// </summary>
        /// /// <param name="employeeDetail"></param>
        /// <returns></returns>
        public JsonModel UpdateEmployeeBasicDetails([FromBody]EmployeesDetails employeeDetail)
        {


            Employees employees = new Employees();

            employees.EmployeeID = Convert.ToInt32(CommonMethod.Decrypt(employeeDetail.EnyEmployeeID).ToString());
            employees.SocSec = employeeDetail.SocSec;
            employees.FirstName = employeeDetail.FirstName;
            employees.MiddleName = employeeDetail.MiddleName;
            employees.LastName = employeeDetail.LastName;
            employees.Sex = employeeDetail.Sex;
            employees.Race = employeeDetail.Race;
            employees.MaritalStatus = employeeDetail.MaritalStatus;
            employees.BirthDate = employeeDetail.BirthDate;
            employees.Hiredate = employeeDetail.Hiredate;


            //employees.EmployeeID = employees.EmployeeID == 0 ? Convert.ToInt32(CommonMethod.Decrypt(employees.EnyEmployeeID).ToString()):0;
            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _employeeService.UpdateEmployeeBasicDetails(employees);
            return response;
        }

        [Authorize]
        [Route("UpdateEmployeeSalaryDetails")]
        [HttpPost]
        /// <summary>        
        /// Description  : Update Employee Salary Details
        /// </summary>
        /// /// <param name="employeeDetail"></param>
        /// <returns></returns>
        public JsonModel UpdateEmployeeSalaryDetails([FromBody]EmployeeSalaryDetailsModel employeeDetail)
        {


            Employees employees = new Employees();

            employees.EmployeeID = Convert.ToInt32(CommonMethod.Decrypt(employeeDetail.EnyEmployeeID).ToString());

            employees.PayFrequency = employeeDetail.PayFrequency;
            employees.HourlyPayRate = employeeDetail.HourlyPayRate;
            employees.Hours = employeeDetail.Hours;
            employees.EarnedIncomeCredit = employeeDetail.EarnedIncomeCredit;
            employees.EarnedIncomeCreditChildren = employeeDetail.EarnedIncomeCreditChildren;
            employees.Allowances = employeeDetail.Allowances;
            employees.Dependents = employeeDetail.Dependents;
            employees.Exemptions = employeeDetail.Exemptions;
            employees.FlatFit = employeeDetail.FlatFit;
            employees.AdditionalWitholdingAmount = employeeDetail.AdditionalWitholdingAmount;
            employees.PartTime = employeeDetail.PartTime;


            //employees.EmployeeID = employees.EmployeeID == 0 ? Convert.ToInt32(CommonMethod.Decrypt(employees.EnyEmployeeID).ToString()):0;
            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _employeeService.UpdateEmployeeSalaryDetails(employees);
            return response;
        }

        [Authorize]
        [Route("UpdateEmployeePersonalDetails")]
        [HttpPost]
        /// <summary>        
        /// Description  : Update Employee Personal Details
        /// </summary>
        /// /// <param name="employeeDetail"></param>
        /// <returns></returns>
        public JsonModel UpdateEmployeePersonalDetails([FromBody]EmployeePersonalDetailsModel employeeDetail)
        {


            Employees employees = new Employees();

            employees.EmployeeID = Convert.ToInt32(CommonMethod.Decrypt(employeeDetail.EnyEmployeeID).ToString());
            employees.CityId = employeeDetail.CityId;
            employees.StateId = employeeDetail.StateId;
            employees.CountryId = employeeDetail.CountryId;
            employees.HeadOfHousehold = employeeDetail.HeadOfHousehold;
            employees.Blind = employeeDetail.Blind;
            employees.Student = employeeDetail.Student;


            //employees.EmployeeID = employees.EmployeeID == 0 ? Convert.ToInt32(CommonMethod.Decrypt(employees.EnyEmployeeID).ToString()):0;
            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _employeeService.UpdateEmployeePersonalDetails(employees);
            return response;
        }

        [Authorize]
        [Route("UpdateEmployeePaymentDetails")]
        [HttpPost]
        /// <summary>        
        /// Description  : Update Employee Payment Details
        /// </summary>
        /// /// <param name="employeeDetail"></param>
        /// <returns></returns>
        public JsonModel UpdateEmployeePaymentDetails([FromBody]EmployeesDetails employeeDetail)
        {


            Employees employees = new Employees();

            employees.EmployeeID = Convert.ToInt32(CommonMethod.Decrypt(employeeDetail.EnyEmployeeID).ToString());        


            //employees.EmployeeID = employees.EmployeeID == 0 ? Convert.ToInt32(CommonMethod.Decrypt(employees.EnyEmployeeID).ToString()):0;
            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _employeeService.UpdateEmployee(employees);
            return response;
        }

    }
}
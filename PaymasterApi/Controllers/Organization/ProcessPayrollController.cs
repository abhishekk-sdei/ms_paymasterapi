﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Paymaster.Common;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Services.Iservices.OrganizationInterface;

namespace PaymasterApi.Controllers.Organization
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessPayrollController : ControllerBase
    {
        private readonly IProcessPayrollServices _processPayrollServices;
        private string fileName { get; set; }

        public ProcessPayrollController(IProcessPayrollServices processPayrollServices)
        {
            _processPayrollServices = processPayrollServices;
        }

        [Authorize]
        [HttpGet]
        [Route("GetPayPeriodList")]
        //sa Old code for get pay period list
        //public JsonModel GetPayPeriodList(int page = 0, int pageSize = 10)
        //{
        //    // Employees employees = new Employees();
        //    List<PayPeriods> PeriodMasterList = _processPayrollServices.GetPayPeriodList();
        //    int Totalrecords = PeriodMasterList.Count();
        //    // return Ok(EmployeeList.Skip(pageSize * (page - 1)).Take(pageSize).ToList());
        //    var reponse = PeriodMasterList.Skip(pageSize * (page - 1)).Take(pageSize).ToList();

        //    return CommonMethod.Pagination<PayPeriods>(reponse, page, pageSize, Totalrecords);
        //}
        //ea Old code for get pay period list

        /// <summary>
        /// Description : Get Pay Period List
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public JsonModel GetPayPeriodList(int page = 0, int pageSize = 10)
        {
            JsonModel respone = new JsonModel();
            return respone = _processPayrollServices.GetPayPeriodList(page, pageSize);
        }

        /// <summary>
        /// Description : Get All Pay Period List to bind in dropdwon
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetAllPayPeriodList")]
        public IActionResult GetAllPayPeriodList()
        {

            List<BindPayPeriodList> ViewPayPeriodsList = _processPayrollServices.GetAllPayPeriodList();
            return Ok(ViewPayPeriodsList);
        }
        //sa Old code for Process Payroll
        [Authorize]
        [HttpPost]
        [Route("closepayrollprocess")]
        public IActionResult closepayrollprocess([FromBody] Processpayrollviewmodel Payroll)
        {
            ViewPayPeriods payPeriods = new ViewPayPeriods();
            payPeriods.PayPeriodId = Payroll.payperiodId;
            payPeriods.UserId = Payroll.userId;
            var result= _processPayrollServices.payrollProcess(payPeriods);
           
            return Ok(result);
        }
        //ea Old code for Process Payroll


        /// <summary>
        /// Description : Process payroll for partiular pay period
        /// </summary>
        /// <param name="Payroll"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("PayProllProcess")]
        public IActionResult payrollprocess([FromBody] Processpayrollviewmodel Payroll)
        {
            //ViewPayPeriods payPeriods = new ViewPayPeriods();
            //payPeriods.PayPeriodId = Payroll.payperiodId;
            //payPeriods.UserId = Payroll.userId;
            var result = _processPayrollServices.CalculateAmount(Payroll);
            return Ok(result);
        }


        /// <summary>
        /// Description : Process payroll for partiular pay period
        /// </summary>
        /// <param name="Payroll"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("AddCheckIdInTaxDeduction")]
        public IActionResult AddCheckIdInTaxDeduction([FromBody] Processpayrollviewmodel Payroll)
        {
            var result = _processPayrollServices.AddCheckIdInTaxDeduction(Payroll);
            return Ok(result);
        }

        /// <summary>
        /// Description : Get Pay Period List for Searching 
        /// </summary>
        /// <param name="Seaching"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetPayPeriodListByName")]
        public JsonModel GetPayPeriodListByName(string Seaching, int page = 0, int pageSize = 10)
        {
            JsonModel respone = new JsonModel();
            return respone = _processPayrollServices.GetPayPeriodList(page, pageSize, Seaching);
        }

        /// <summary>
        /// Description : Get ANy changes on Time punches 
        /// </summary>

        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("GetIsChangesOnTimePuches")]

        public IActionResult GetIsChangesOnTimePuches()
        {

            var result = _processPayrollServices.GetIsChangesOnTimePuches();
            return Ok(result);
        }

        /// <summary>
        /// Description : Print the employee checks
        /// </summary>

        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("PrintEmployeeChecks")]
        //[Produces(contentType: "application/pdf")]
        public IActionResult PrintEmployeeChecks(string FromEmployeeNO, string ToEmployeeNO,int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0)
        {
            

            var result = _processPayrollServices.PrintEmployeeChecks(FromEmployeeNO, ToEmployeeNO,PayPeriodId, EmployeeId, ApplicationId);
            return downloadFile(result.Result);
            //return Ok(result.Result);
        }
         


        private FileResult downloadFile(string filePath)
        {
            //IFileProvider provider = new PhysicalFileProvider(filePath);
            //IFileProvider provider = new PhysicalFileProvider(Path.GetFullPath(filePath));

            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            fileName = Path.GetFileName(filePath);
            //IFileProvider provider = new PhysicalFileProvider("3fbd29c2-447c-4117-a900-a682a3c6cfe4.pdf");
            //IFileInfo fileInfo = provider.GetFileInfo(fileName);
            //var readStream = fileInfo.CreateReadStream();
            var mimeType = "application/pdf";
            return File(fileBytes, mimeType, fileName);
        }
        
        [Authorize]
        [HttpGet]
        [Route("FromEmployeeList")]
        public IActionResult FromEmployeeList(string EmployeeNo,int EmployeeID)
        {
            var result = _processPayrollServices.FromEmployeeList(EmployeeNo,EmployeeID);
            return Ok(result);
        }

        [Authorize]
        [HttpGet]
        [Route("ToEmployeeList")]
        public IActionResult ToEmployeeList(string EmployeeNo,int EmployeeID)
        {
            var result = _processPayrollServices.ToEmployeeList(EmployeeNo,EmployeeID);
            return Ok(result);
        }
    }
}
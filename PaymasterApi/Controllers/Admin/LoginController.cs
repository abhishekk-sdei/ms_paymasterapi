﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Paymaster.Common;
using Paymaster.DataContract.Admin.Login;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.ViewModal.LoginModal;
using Paymaster.DataContract.User;
using Paymaster.Services.Iservices;
using Paymaster.Services.Iservices.Admin;
using Paymaster.Services.Iservices.CommonInterface;
using static Paymaster.Common.ConstantString;
using static Paymaster.Common.Enum.CommonEnums;

namespace PaymasterApi.Controllers.Admin
{

    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUserService _UserService;
        private readonly ILoginService _loginService;
        private readonly ICommonService _ICommonService;

        public LoginController(IUserService UserService, ILoginService loginService, ICommonService ICommonService)
        {
            _UserService = UserService;
            _loginService = loginService;
            _ICommonService = ICommonService;
        }

        /// <summary>        
        /// Description  : Login for Admin
        /// </summary>
        /// <param name="loginViewModel"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("AdminLogin")]
        public IActionResult Login([FromBody]LoginViewModel loginViewModel)
        {
            AdminUsers users = new AdminUsers();
            users.Email = loginViewModel.Username;
            users.Password = loginViewModel.Password;

            users = _loginService.CheckAdminCredentials(users);
            IActionResult response = Unauthorized();
            if (users != null)
            {
                var tempuserid = users.UserId;
                var user = _UserService.AdminAuthenticate(users);
                response = Ok(new { token = user.AccessToken , UserId = tempuserid });
            }
            return Ok(response);
        }

        /// <summary>        
        /// Description  : Get Organization List
        /// </summary>
        /// <param name="page"></param>
        /// /// <param name="pageSize"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetOrganizationList")]
        public JsonModel GetOrganizationList(int page = 0, int pageSize = 10)
        {
            List<OrganizationDetails> OrganizationDetailsList = _loginService.GetOrganizationList();
            int Totalrecords = OrganizationDetailsList.Count();
            var reponse = OrganizationDetailsList.Skip(pageSize * (page - 1)).Take(pageSize).ToList();
            return new JsonModel()
            {
                data = reponse,
                Message = StatusMessage.FetchMessage,

                StatusCode = (int)HttpStatusCodes.OK,//Success
                meta = new Meta()
                {
                    TotalRecords = reponse != null && reponse.Count > 0 ? Totalrecords : 0,
                    CurrentPage = page,
                    PageSize = pageSize,
                    DefaultPageSize = pageSize,
                    TotalPages = Math.Ceiling(Convert.ToDecimal((reponse != null && reponse.Count > 0 ? (decimal)Totalrecords : 0) / (decimal)pageSize))
                }
            };
        }

        /// <summary>        
        /// Description  : Get Organization list by organization id
        /// </summary>
        /// <param name="OrganizationId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetOrganizationListById")]
        public IActionResult GetOrganizationListById(int OrganizationId)
        {
            OrganizationDetails response = new OrganizationDetails();
            response = _loginService.GetOrganizationListById(OrganizationId);
            return Ok(response);
        }


        /// <summary>        
        /// Description  : Set Up Organization
        /// </summary>
        /// <param name="ApplicationDetails"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("SetUpOrganization")]
        [HttpPost]
        public JsonModel SetUpOrganization([FromBody] Organization_Details ApplicationDetails)
        {
            JsonModel response = new JsonModel();
            response = _loginService.SetUpOrganization(ApplicationDetails);
            //if(response.StatusCode == 400) 
            //{
            //    Exception ex = new Exception();               
            //    _ICommonService.ErrorHandling(response.data);
            //}
            return response;

        }

        /// <summary>        
        /// Description  : Save Organization
        /// </summary>
        /// <param name="ApplicationDetails"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("SaveOrganization")]
        [HttpPost]
        public IActionResult SaveOrganization([FromBody] ApplicationDetails ApplicationDetails)
        {
            
            JsonModel response = new JsonModel();
            List<OrganizationDetails> lst = new List<OrganizationDetails>();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            var res = _loginService.SaveOrganization(ApplicationDetails);
            if (res.StatusCode == 200)
            {
                lst = _loginService.CreateDynamicDB(ApplicationDetails).ToList();
            }

            return Ok(res);
        }

        /// <summary>        
        /// Description  : Update Organization
        /// </summary>
        /// <param name="organizationDetails"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("UpdateOrganization")]
        [HttpPost]
        public JsonModel UpdateOrganization([FromBody]OrganizationDetails organizationDetails)
        {
            JsonModel response = new JsonModel();
            TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _loginService.UpdateOrganization(organizationDetails);
            return response;
        }

        /// <summary>        
        /// Description  : Check Domain Availability
        /// </summary>
        /// <param name="DomainName"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("CheckDomainName")]
        [HttpGet]
        public JsonModel CheckDomainName(string DomainName)
        {
            
               JsonModel response = new JsonModel();
            //TokenModel token = CommonMethod.GetTokenDataModel(HttpContext);
            response = _loginService.CheckDomainName(DomainName);
            return response;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Paymaster.DataContract;
using Paymaster.DataContract.Authentication;
using Paymaster.Services.Iservices;

using Paymaster.Services.Iservices.Token;

namespace PaymasterApi.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private IUserService _userService;
        private readonly ITokenService _tokenService;

        public HomeController(IUserService userService,ITokenService tokenService)
        {
            _userService = userService;
            _tokenService = tokenService;
        }

        /// <summary>
        /// Description : Verify Domain
        /// </summary>
        /// <param name="DomainName"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("VerifyDomainName")]
        public IActionResult VerifyDomainName(string DomainName = "")
        {
            OrganizationDetails domainToken = new OrganizationDetails();
            TokenModel token = new TokenModel();
            token.Request = HttpContext;
            domainToken.DomainName = DomainName;
            TokenModel tokenData = _tokenService.GetDomain(domainToken);


            //if (tokenData != null)
            //{
            //    tokenData.Organization = _tokenService.GetOrganizationById(domainToken.OrganizationId, token);
            //    Response.StatusCode = (int)HttpStatusCodes.OK;
            //    return Json(new
            //    {
            //        data = tokenData,
            //        Message = StatusMessage.VerifiedBusinessName,
            //        StatusCode = (int)HttpStatusCodes.OK
            //    });
            //}
            //else
            //{
            //    Response.StatusCode = (int)HttpStatusCodes.Unauthorized;
            //    return Json(new
            //    {
            //        data = new object(),
            //        Message = StatusMessage.UnVerifiedBusinessName,
            //        StatusCode = (int)HttpStatusCodes.Unauthorized
            //    });
            //}

            return Ok(tokenData);
        }


    }
}
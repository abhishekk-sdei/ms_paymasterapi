﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Paymaster.DataContract.Common;
using Paymaster.Services.Iservices.CommonInterface;

namespace PaymasterApi.Controllers.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly ICommonService _commonService;

        public CommonController(ICommonService commonService)
        {
            _commonService = commonService;
        }

        /// <summary>        
        /// Description  : Get Country List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetCountryList")]
        public IActionResult GetCountryList()
        {
            List<CountryMaster> CountryList = _commonService.GetCountryList();
            return Ok(CountryList);
        }

        /// <summary>        
        /// Description  : Get State List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetStateList")]
        public IActionResult GetStateList(int CountryId)
        {
            List<StateMaster> StateList = _commonService.GetStateList(CountryId);
            return Ok(StateList);
        }

        /// <summary>        
        /// Description  : Get City List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetCityList")]
        public IActionResult GetCityList(int CountryId,int StateId)
        {
            List<CityMaster> CityList = _commonService.GetCityList(CountryId, StateId);
            return Ok(CityList);
        }

        /// <summary>        
        /// Description  : Get Gender List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetGenderList")]
        public IActionResult GetGenderList()
        {
            List<GenderMaster> GenderList = _commonService.GetGenderList();
            return Ok(GenderList);
        }

        /// <summary>        
        /// Description  : Get Marital Status List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetMaritalStatusList")]
        public IActionResult GetMaritalStatusList()
        {
            List<MaritalStatusMaster> MaritalStatusList = _commonService.GetMaritalStatusList();
            return Ok(MaritalStatusList);
        }

        /// <summary>        
        /// Description  : Get Race List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetRaceList")]
        public IActionResult GetRaceList()
        {
            List<RaceMaster> RaceList = _commonService.GetRaceList();
            return Ok(RaceList);
        }

        /// <summary>        
        /// Description  : Get Pay period List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetPayPeriodList")]
        public IActionResult GetPayPeriodList()
        {
            List<PeriodMaster> PayPeriodList = _commonService.GetPayPeriodList();
            return Ok(PayPeriodList);
            }

        /// <summary>        
        /// Description  : Get Type List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetTypeList")]
        public IActionResult GetTypeList()
        {
            List<TypeMaster> TypeMasterList = _commonService.GetTypeList();
            return Ok(TypeMasterList);
        }

    }
}
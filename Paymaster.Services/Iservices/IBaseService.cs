﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Iservices
{
    public interface IBaseService
    {
        T ExecuteFunctions<T>(Func<T> method);
    }
}

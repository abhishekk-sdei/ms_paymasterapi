﻿using System;
using System.Collections.Generic;
using System.Text;
using Paymaster.DataContract.Admin.Login;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.User;

namespace Paymaster.Services.Iservices
{
    public interface IUserService
    {
        TokenModel Authenticate(Users _users);
        TokenModel AdminAuthenticate(AdminUsers _users);
        //IEnumerable<User> GetAll();

    }
}


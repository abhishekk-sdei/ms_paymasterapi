﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Iservices.OrganizationInterface
{
    public interface ITimePunchService
    {
        public List<TimePunches> GetTimePunchList(int EmpoyeeID);
        public List<TimePunchesHistoryModel> GetTimePunchHistoryList(int page = 0, int pageSize = 10, int Emp_ID = 0, int PayPeriodId = 0, string SearchText = "");

        public List<ChecksHistoryModel> GetEmployeecheckListHistory(int page = 0, int pageSize = 10, int Emp_ID = 0, int PayPeriodId = 0, string SearchText = "");

        public List<TimePunches> ViewTimePunchList(int EmpoyeeID);
        public TimePunhesDetailModel TimePunchList(int page, int pageSize );
        public TimePunhesDetailModel ViewEmployeeTimelist(int page, int pageSize);

        public List<ViewCheckDeduction> GetEmployeeCheckDeductionList(int CheckId);

        public TimePunhesDetailModel ViewEmployeeFilteredTimelist(int page, int pageSize, int EmployeeId = 0, int PayPeriodId = 0);
        public JsonModel AddTimePunch(TimePunches timePunches);

        public JsonModel UpdateTimePunch(TimePunches timePunches);

    }
}

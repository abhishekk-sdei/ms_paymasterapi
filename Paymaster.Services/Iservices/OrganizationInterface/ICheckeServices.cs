﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Iservices.OrganizationInterface
{
    public interface ICheckeServices
    {
        //public List<Checks> GetTimePunchList(int EmpoyeeID);
        //public JsonModel AddTimePunch(TimePunches timePunches);

        public JsonModel UpdateCheckDetail(Checks checks);
        //public JsonModel GetChecksList(int page , int pageSize );
        public List<ViewChecksModel> GetChecksList(int page, int pageSize);
        public List<DropdownBindCheckList> GetDropdownBindCheckslist(int Emp_ID, int PayPeriodId = 0);
        public ViewChecksDetails ViewChecksDetails(int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0);
    }
        
}

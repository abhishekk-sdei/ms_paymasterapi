﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Paymaster.Services.Iservices.OrganizationInterface
{
    public interface IProcessPayrollServices
    {
        //public List<PayPeriods> GetPayPeriodList();
        public JsonModel GetPayPeriodList(int page, int pageSize, string Seaching = "");
        public List<BindPayPeriodList> GetAllPayPeriodList();
        public JsonModel payrollProcess(ViewPayPeriods payPeriods);
        public JsonModel CalculateAmount(Processpayrollviewmodel Payroll);
        public JsonModel GetIsChangesOnTimePuches();
        //public Task<MemoryStream> PrintEmployeeChecks(int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0);
        public Task<string> PrintEmployeeChecks(string FromEmployeeNO, string ToEmployeeNO,int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0);

        public JsonModel AddCheckIdInTaxDeduction(Processpayrollviewmodel Payroll);
        public JsonModel FromEmployeeList(string EmployeeNo,int EmployeeId = 0);
        public JsonModel ToEmployeeList(string EmployeeNo,int EmployeeId = 0);
    }
}

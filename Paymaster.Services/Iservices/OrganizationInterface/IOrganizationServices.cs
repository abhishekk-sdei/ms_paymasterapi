﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.DataContract.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Iservices.OrganizationInterface
{
    //public interface IOrganizationServices:IBaseService\
    public interface IOrganizationServices 
    {
        public List<EmployeeViewModel> GetEmployeeList(int TimePunchCheck = 0, int page = 0, int pageSize = 10);
        public List<EmployeeSocSecModel> GetAllEmployeeList();
        //public EmployeeDetailModel GetEmployeeList(int TimePunchCheck = 0);

        public Users CheckCredentials(Users users);

        //public List<Employees> GetEmployeeListByName(string Seaching, int page = 0, int pageSize = 10);
        public List<EmployeeViewModel> GetEmployeeListByName(string Seaching, int page = 0, int pageSize = 10);
        public JsonModel DeleteEmployee(int EmployeeId);

        List<EmployeesDetails> GetEmployeeListById(int EmployeeId);
        List<ActiveEmployees> GetActiveEmployee();
        List<ActiveEmployees> GetPreviousEmployee();
    }
}

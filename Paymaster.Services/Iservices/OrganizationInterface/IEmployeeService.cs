﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Iservices.OrganizationInterface
{
    public interface IEmployeeService
    {
        public JsonModel SaveEmployee(EmployeesDetails employees);

        public JsonModel SaveEmployeeBasicDetails(EmployeesDetails employees);

        public JsonModel SaveEmployeeSalaryDetails(EmployeeSalaryDetailsModel employees);

        public JsonModel SaveEmployeePersonalDetails(EmployeePersonalDetailsModel employees);
        public JsonModel SaveEmployeePaymentDetails(EmployeesDetails employees);

        public JsonModel UpdateEmployee(Employees employees);
        public JsonModel UpdateEmployeeBasicDetails(Employees employees);
        public JsonModel UpdateEmployeeSalaryDetails(Employees employees);
        public JsonModel UpdateEmployeePersonalDetails(Employees employees);

       
    }
}

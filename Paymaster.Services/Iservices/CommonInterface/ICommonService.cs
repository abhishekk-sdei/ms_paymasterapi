﻿using Paymaster.DataContract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Iservices.CommonInterface
{
    public interface ICommonService
    {
        public List<CountryMaster> GetCountryList();
        public List<StateMaster> GetStateList(int CountryId);

        public List<CityMaster> GetCityList(int CountryId, int StateId);

        public List<GenderMaster> GetGenderList();

        public List<MaritalStatusMaster> GetMaritalStatusList();

        public List<RaceMaster> GetRaceList();

        public List<PeriodMaster> GetPayPeriodList();

        public List<TypeMaster> GetTypeList();
        public void ErrorHandling(Exception ex);
    }
}



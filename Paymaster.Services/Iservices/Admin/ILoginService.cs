﻿using Paymaster.DataContract.Admin.Login;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Iservices.Admin
{
    public interface ILoginService
    {
        public AdminUsers CheckAdminCredentials(AdminUsers users);

        public List<OrganizationDetails> GetOrganizationList();
        public OrganizationDetails GetOrganizationListById(int OrganizationId);

        public JsonModel SaveOrganization(ApplicationDetails ApplicationDetails);
        public JsonModel SetUpOrganization(Organization_Details ApplicationDetails);

        public JsonModel UpdateOrganization(OrganizationDetails organizationDetails);

        public JsonModel CheckDomainName(string DomainName);

        List<OrganizationDetails> CreateDynamicDB(ApplicationDetails ApplicationDetails);
    }
}

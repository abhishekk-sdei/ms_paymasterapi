﻿using Paymaster.DataContract.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Iservices.Token
{
    public interface ITokenService :IBaseService
    {
        TokenModel GetDomain(OrganizationDetails domainToken);
    }
}

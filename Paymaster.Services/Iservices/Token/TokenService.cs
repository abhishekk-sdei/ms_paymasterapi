﻿using Paymaster.DataContract.Authentication;
using Paymaster.Repository.InterfaceRepository;
using Paymaster.Services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Iservices.Token
{
    public class TokenService: BaseService,ITokenService
    {
        private readonly ITokenRepository _tokenRepository;
        public TokenService(ITokenRepository tokenRepository)
        {
            _tokenRepository = tokenRepository;
        }
        public TokenModel GetDomain(OrganizationDetails domainToken)
        {
            return _tokenRepository.GetDomain(domainToken);
        }
    }
}

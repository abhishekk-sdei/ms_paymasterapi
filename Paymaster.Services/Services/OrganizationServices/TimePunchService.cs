﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using Paymaster.Services.Iservices.OrganizationInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paymaster.Services.Services.OrganizationServices
{
    public class TimePunchService : ITimePunchService
    {
        private readonly ITimePunchRepository  _timePunchRepository;

        public TimePunchService(ITimePunchRepository timePunchRepository)
        {
            _timePunchRepository = timePunchRepository;
        }


        public List<TimePunches> GetTimePunchList(int EmployeeID) 
        {
            return _timePunchRepository.GetTimePunchList<TimePunches>(EmployeeID).ToList();
        }

        public List<TimePunchesHistoryModel> GetTimePunchHistoryList(int page = 0, int pageSize = 10, int Emp_ID = 0, int PayPeriodId = 0, string SearchText = "")
        {
            return _timePunchRepository.GetTimePunchHistoryList<TimePunchesHistoryModel>(page, pageSize, Emp_ID, PayPeriodId, SearchText).ToList();
        }

        public List<ChecksHistoryModel> GetEmployeecheckListHistory(int page = 0, int pageSize = 10, int Emp_ID = 0, int PayPeriodId = 0, string SearchText = "")
        {
            return _timePunchRepository.GetEmployeecheckListHistory<ChecksHistoryModel>(page, pageSize, Emp_ID, PayPeriodId, SearchText).ToList();
        }

        public List<TimePunches> ViewTimePunchList(int EmployeeID)
        {
            return _timePunchRepository.ViewTimePunchList<TimePunches>(EmployeeID).ToList();
        }

        public TimePunhesDetailModel TimePunchList(int page , int pageSize )
        {
            return _timePunchRepository.TimePunchList(page, pageSize);
        }
        public TimePunhesDetailModel ViewEmployeeTimelist(int page, int pageSize)
        {
            return _timePunchRepository.ViewEmployeeTimelist(page, pageSize);
        }

        public List<ViewCheckDeduction> GetEmployeeCheckDeductionList(int CheckId)
        {
            return _timePunchRepository.GetEmployeeCheckDeductionList<ViewCheckDeduction>(CheckId).ToList();
        }

        public TimePunhesDetailModel ViewEmployeeFilteredTimelist(int page, int pageSize, int EmployeeId = 0, int PayPeriodId = 0)
        {
            return _timePunchRepository.ViewEmployeeFilteredTimelist(page, pageSize,  EmployeeId ,PayPeriodId);
        }
        public JsonModel AddTimePunch(TimePunches timePunches)
        {
            return _timePunchRepository.AddTimePunch(timePunches);
        }

        public JsonModel UpdateTimePunch(TimePunches timePunches)
        {
            return _timePunchRepository.UpdateTimePunch(timePunches);
        }
    }
}

﻿using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using Paymaster.Services.Iservices.OrganizationInterface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Services.OrganizationServices
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

       
        public JsonModel SaveEmployee(EmployeesDetails employees) 
        {
            return _employeeRepository.SaveEmployee(employees) ;

        }
        public JsonModel SaveEmployeeBasicDetails(EmployeesDetails employees)
        {
            return _employeeRepository.SaveEmployeeBasicDetails(employees);

        }
        public JsonModel SaveEmployeeSalaryDetails(EmployeeSalaryDetailsModel employees)
        {
            return _employeeRepository.SaveEmployeeSalaryDetails(employees);

        }
        public JsonModel SaveEmployeePersonalDetails(EmployeePersonalDetailsModel employees)
        {
            return _employeeRepository.SaveEmployeePersonalDetails(employees);

        }

        public JsonModel SaveEmployeePaymentDetails(EmployeesDetails employees)
        {
            return _employeeRepository.SaveEmployeePaymentDetails(employees);

        }

        public JsonModel UpdateEmployee(Employees employees)
        {
            return _employeeRepository.UpdateEmployee(employees);

        }

        public JsonModel UpdateEmployeeBasicDetails(Employees employees)
        {
            return _employeeRepository.UpdateEmployeeBasicDetails(employees);

        }

        public JsonModel UpdateEmployeeSalaryDetails(Employees employees)
        {
            return _employeeRepository.UpdateEmployeeSalaryDetails(employees);

        }
        public JsonModel UpdateEmployeePersonalDetails(Employees employees)
        {
            return _employeeRepository.UpdateEmployeePersonalDetails(employees);

        }
    }
}

﻿using Microsoft.Extensions.Options;
using Paymaster.Common.Options;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.DataContract.User;
using Paymaster.Repository.Context;
using Paymaster.Repository.InterfaceRepository;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using Paymaster.Services.Iservices.OrganizationInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paymaster.Services.Services.OrganizationServices
{
    //public class OrganizationServices : BaseService,IOrganizationServices
    public class OrganizationServices : IOrganizationServices
    {
        private readonly IOrganizationRepository _organizationRepository;

        private readonly JwtIssuerOptions _appSettings;
        public OrganizationServices(IOptions<JwtIssuerOptions> appSettings ,IOrganizationRepository organizationRepository)
        {
            _appSettings = appSettings.Value;
            _organizationRepository = organizationRepository;
        }

        public  List<EmployeeViewModel> GetEmployeeList(int TimePunchCheck = 0, int page = 0, int pageSize = 10)
        {
            //return _organizationRepository.GetEmployeeList();
            return _organizationRepository.GetEmployeeList<EmployeeViewModel>(TimePunchCheck ,page, pageSize).ToList();

            //return _organizationRepository.GetEmployeeList(TimePunchCheck);
        }

        public List<EmployeeSocSecModel> GetAllEmployeeList()
        {
            return _organizationRepository.GetAllEmployeeList<EmployeeSocSecModel>().ToList();
        }

        public List<EmployeesDetails> GetEmployeeListById(int EmployeeId)
        {
            return _organizationRepository.GetEmployeeListById<EmployeesDetails>(EmployeeId).ToList();
        }

        public List<ActiveEmployees> GetActiveEmployee()
        {
            return _organizationRepository.GetActiveEmployee<ActiveEmployees>().ToList();
        }

        public List<ActiveEmployees> GetPreviousEmployee()
        {
            return _organizationRepository.GetPreviousEmployee<ActiveEmployees>().ToList();
        }

        public Users CheckCredentials(Users users)
        {
            return _organizationRepository.CheckCredentials(users);
        }

        public List<EmployeeViewModel> GetEmployeeListByName(string Seaching, int page = 0, int pageSize = 10)
        {
            return _organizationRepository.GetEmployeeListByName<EmployeeViewModel>(Seaching, page, pageSize).ToList();
           // return _organizationRepository.GetEmployeeListByName(Seaching);
        }

        public JsonModel DeleteEmployee(int EmployeeId)
        {
            return _organizationRepository.DeleteEmployee(EmployeeId);
        }
    }
}

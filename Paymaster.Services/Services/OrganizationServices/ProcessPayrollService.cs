﻿using Paymaster.Common;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using Paymaster.Services.Iservices.OrganizationInterface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paymaster.Services.Services.OrganizationServices
{
    public class ProcessPayrollService : IProcessPayrollServices
    {
        private readonly IProcessPayrollRepository _processPayrollRepository;
        public ProcessPayrollService(IProcessPayrollRepository processPayrollRepository)
        {
            _processPayrollRepository = processPayrollRepository;
        }
        //public List<PayPeriods> GetPayPeriodList()
        //{
        //    return _processPayrollRepository.GetPayPeriodList();
        //}
        public JsonModel GetPayPeriodList(int page, int pageSize,string Seaching = "")
        {
           // return _processPayrollRepository.GetPayPeriodList();

            var payperiodlist = _processPayrollRepository.GetPayPeriodList<ViewPayPeriods>(Seaching).ToList();
            int Totalrecords = payperiodlist.Count();
            var payperiodlistpageingres = payperiodlist.Skip(pageSize * (page - 1)).Take(pageSize).ToList();
            var result = CommonMethod.Pagination(payperiodlistpageingres, page, pageSize, Totalrecords);
            return result;
        }

        public List<BindPayPeriodList> GetAllPayPeriodList()
        {
            // return _processPayrollRepository.GetPayPeriodList();

            var payperiodlist = _processPayrollRepository.GetAllPayPeriodList<BindPayPeriodList>().ToList();
          
            return payperiodlist;
        }

        public JsonModel payrollProcess(ViewPayPeriods payPeriods)
        { 
            return _processPayrollRepository.payrollProcess(payPeriods);
            //return null;
        }

        public JsonModel CalculateAmount(Processpayrollviewmodel Payroll)
        {
            return _processPayrollRepository.CalculateAmount(Payroll);
        }
        public JsonModel GetIsChangesOnTimePuches()
        {
            return _processPayrollRepository.GetIsChangesOnTimePuches();
        }
        //public Task<MemoryStream> PrintEmployeeChecks(int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0)
        public Task<string> PrintEmployeeChecks( string FromEmployeeNO, string ToEmployeeNO,int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0)
        {
            return _processPayrollRepository.PrintEmployeeChecksAsync( FromEmployeeNO, ToEmployeeNO,PayPeriodId, EmployeeId, ApplicationId);
        }

        public JsonModel AddCheckIdInTaxDeduction(Processpayrollviewmodel Payroll)
        {
            return _processPayrollRepository.AddCheckIdInTaxDeduction(Payroll);
        }

        public JsonModel FromEmployeeList(string EmployeeNo,int EmployeeId = 0)
        {
            return _processPayrollRepository.FromEmployeeList(EmployeeNo,EmployeeId);
        }

        public JsonModel ToEmployeeList(string EmployeeNo,int EmployeeId = 0)
        {
            return _processPayrollRepository.ToEmployeeList(EmployeeNo,EmployeeId);
        }
    }
}

﻿using Paymaster.Common;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.Organization.Employees;
using Paymaster.Repository.InterfaceRepository.IOrganizationRepository;
using Paymaster.Services.Iservices.OrganizationInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Paymaster.Services.Services.OrganizationServices
{
   
    public class CheckServices:ICheckeServices
    {
        private readonly IChecksRepository _checksRepository;
        public CheckServices(IChecksRepository checksRepository)
        {
            _checksRepository = checksRepository;
        }
        public JsonModel UpdateCheckDetail(Checks checks)
        {
            return _checksRepository.UpdateCheckDetail(checks);
        }
        //public JsonModel GetChecksList(int page, int pageSize)
        public List<ViewChecksModel> GetChecksList(int page, int pageSize)
        {

            

            var checklist= _checksRepository.GetCheksList<ViewChecksModel>(page, pageSize).ToList();
            return checklist;
            //int Totalrecords = checklist.Count();
            //var checklistpageingres= checklist.Skip(pageSize * (page - 1)).Take(pageSize).ToList();
            //var result= CommonMethod.Pagination(checklistpageingres, page, pageSize, Totalrecords);
            //return result;
        }

        public List<DropdownBindCheckList> GetDropdownBindCheckslist(int Emp_ID, int PayPeriodId = 0)
        {

            List<DropdownBindCheckList> checklist = _checksRepository.GetDropdownBindCheckslist<DropdownBindCheckList>(Emp_ID, PayPeriodId).ToList();
            return checklist;
        }

        public ViewChecksDetails ViewChecksDetails(int PayPeriodId = 0, int EmployeeId = 0, int ApplicationId = 0)
        {
            ViewChecksDetails result = new ViewChecksDetails();
            var checklist = _checksRepository.ViewChecksDetails<ViewChecksDetails>(PayPeriodId, EmployeeId, ApplicationId).FirstOrDefault();
            result = checklist;
            return result;
        }
    }
}

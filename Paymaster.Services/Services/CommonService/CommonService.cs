﻿using Paymaster.DataContract.Common;
using Paymaster.Repository.InterfaceRepository.ICommonRepository;
using Paymaster.Services.Iservices.CommonInterface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Paymaster.Services.Services.CommonService
{
    public class CommonService : ICommonService
    {
        private readonly ICommonRepository _commonRepository;

        public CommonService(ICommonRepository commonRepository)
        {
            _commonRepository = commonRepository;
        }

        public List<CountryMaster> GetCountryList() 
        {
            return _commonRepository.GetCountryList();
        }

        public List<StateMaster> GetStateList(int CountryId)
        {
            return _commonRepository.GetStateList(CountryId);
        }

        public List<CityMaster> GetCityList(int CountryId, int StateId)
        {
            return _commonRepository.GetCityList(CountryId, StateId);
        }

        public List<GenderMaster> GetGenderList()
        {
            return _commonRepository.GetGenderList();
        }

        public List<MaritalStatusMaster> GetMaritalStatusList()
        {
            return _commonRepository.GetMaritalStatusList();
        }

        public List<RaceMaster> GetRaceList()
        {
            return _commonRepository.GetRaceList();
        }

        public List<PeriodMaster> GetPayPeriodList()
        {
            return _commonRepository.GetPayPeriodList();
        }

        public List<TypeMaster> GetTypeList()
        {
            return _commonRepository.GetTypeList();
        }

        public void ErrorHandling(Exception ex)
        {
           _commonRepository.ErrorHandling( ex);
        }
    }
}

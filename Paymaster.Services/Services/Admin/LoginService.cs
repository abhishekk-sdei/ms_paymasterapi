﻿using Paymaster.DataContract.Admin.Login;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Common;
using Paymaster.DataContract.User;
using Paymaster.Repository.InterfaceRepository.Admin.ILoginRepository;
using Paymaster.Services.Iservices.Admin;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Paymaster.Services.Services.Admin
{
    public class LoginService : ILoginService
    {
        private readonly ILoginRepository _loginRepository;

        public LoginService(ILoginRepository loginRepository)
        {
            _loginRepository = loginRepository;
        }

        public AdminUsers CheckAdminCredentials(AdminUsers users)
        {
            return _loginRepository.CheckAdminCredentials(users);
        }

        public List<OrganizationDetails> GetOrganizationList()
        {
            return _loginRepository.GetOrganizationList();
        }

        public OrganizationDetails GetOrganizationListById(int OrganizationId)
        {
            return _loginRepository.GetOrganizationListById(OrganizationId);
        }
        public JsonModel SaveOrganization(ApplicationDetails ApplicationDetails)
        {
            return _loginRepository.SaveOrganization(ApplicationDetails);

        }
        public JsonModel SetUpOrganization(Organization_Details ApplicationDetails)
        {           
            return _loginRepository.SetUpOrganization(ApplicationDetails);

        }

        public JsonModel UpdateOrganization(OrganizationDetails organizationDetails)
        {
            return _loginRepository.UpdateOrganization(organizationDetails);
        }


        public JsonModel CheckDomainName(string DomainName)
        {
            return _loginRepository.CheckDomainName(DomainName);
        }

        public List<OrganizationDetails> CreateDynamicDB(ApplicationDetails ApplicationDetails)
        {
            return _loginRepository.CreateDynamicDB<OrganizationDetails>(ApplicationDetails).ToList();
        }
    }
}

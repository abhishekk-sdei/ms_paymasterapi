﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Paymaster.Common.Options;
using Paymaster.DataContract.Admin.Login;
using Paymaster.DataContract.Authentication;
using Paymaster.DataContract.Helper;
using Paymaster.DataContract.User;
using Paymaster.Repository.InterfaceRepository;
using Paymaster.Services.Iservices;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Paymaster.Services.Services
{
    public class UserService : IUserService
    {

        private readonly IUserRepository _userRepository;
        private readonly JwtIssuerOptions _appSettings;
        private IConfiguration _configuration;
        public UserService(IUserRepository userRepository, IOptions<JwtIssuerOptions> appSettings, IConfiguration configuration)
        {
            _userRepository = userRepository;
            _appSettings = appSettings.Value;
            _configuration = configuration;
        }   
        public TokenModel Authenticate(Users user)
        {
            //

            var userData = _userRepository.Authenticate(user.Email, user.Password);

            if (userData == null)
                return null;

            //  var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtIssuerOptions:JwtKey"]));
            //  var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //  var claims = new[] {
            //new Claim(ClaimTypes.Name, userData.UserName.ToString()),
            //         new Claim(ClaimTypes.Role, userData.RoleID.ToString()),
            //    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            //  };

            //  var token = new JwtSecurityToken(_configuration["JwtIssuerOptions:Issuer"],
            //      _configuration["JwtIssuerOptions:Issuer"],
            //      claims,
            //      expires: DateTime.Now.AddMinutes(120),
            //      signingCredentials: credentials);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.JwtKey);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, userData.UserName.ToString()),
                      new Claim(ClaimTypes.Role, userData.RoleID.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            userData.AccessToken = new JwtSecurityTokenHandler().WriteToken(token);

            return userData;
            //return null;
        }

        public TokenModel AdminAuthenticate(AdminUsers user)
        {

            var userData = _userRepository.AdminAuthenticate(user.Email, user.Password);

            if (userData == null)
                return null;

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtIssuerOptions:JwtKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
          new Claim(ClaimTypes.Name, userData.UserName.ToString()),
                   new Claim(ClaimTypes.Role, userData.RoleID.ToString()),
              new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(_configuration["JwtIssuerOptions:Issuer"],
                _configuration["JwtIssuerOptions:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            userData.AccessToken = new JwtSecurityTokenHandler().WriteToken(token);

            return userData;
            //return null;
        }
    }
}
